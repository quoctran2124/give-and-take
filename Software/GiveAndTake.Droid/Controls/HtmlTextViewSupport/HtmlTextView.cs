﻿using Android.Content;
using Android.Text;
using Android.Text.Method;
using Android.Text.Style;
using Android.Util;
using Android.Views;
using GiveAndTake.Droid.Helpers;
using Java.Lang;
using System;
using System.Collections.Generic;
using System.Linq;
using Android.OS;
using Android.Widget;

namespace GiveAndTake.Droid.Controls.HtmlTextViewSupport
{
	public class HtmlTextView : TextView
	{
		private List<HyperLink> _hyperLinks = new List<HyperLink>();
		public string HtmlText
		{
			set
			{
				ISpanned html;
				if (Build.VERSION.SdkInt >= BuildVersionCodes.N)
				{
					html = Html.FromHtml(value, FromHtmlOptions.ModeLegacy);
				}
				else
				{
					html = Html.FromHtml(value);
				}

				var spannableString = new SpannableString(html);

				MakeLinkClickable(spannableString);
				SetText(spannableString, BufferType.Spannable);
			}
		}

		private void MakeLinkClickable(ISpannable spannable)
		{
			_hyperLinks = CollectLinks(spannable);
			foreach (var hyperLink in _hyperLinks)
			{
				spannable.SetSpan(hyperLink.InternalUrlSpan, hyperLink.Start, hyperLink.End, SpanTypes.ExclusiveExclusive);
				hyperLink.InternalUrlSpan.LinkClicked += OnAccessLink;
				spannable.RemoveSpan(hyperLink.UrlSpan);
			}
		}

		private static List<HyperLink> CollectLinks(ISpannable spannable)
		{
			var links = new List<HyperLink>();
			var spannableStringBuilder = new SpannableStringBuilder(spannable);
			var spans = spannableStringBuilder.GetSpans(0,
				spannable.Length(),
				Class.FromType(typeof(URLSpan))).Cast<URLSpan>();

			foreach (var span in spans)
			{
				if (span is URLSpan urlSpan)
				{
					var start = spannable.GetSpanStart(urlSpan);
					var end = spannable.GetSpanEnd(urlSpan);

					var hyperLink = new HyperLink
					{
						UrlSpan = urlSpan,
						Start = start,
						End = end,
						InternalUrlSpan = new InternalUrlSpan()
					};

					links.Add(hyperLink);
				}
			}

			return links;
		}

		private void OnAccessLink(View textView)
		{
			LinkClicked?.Invoke(this, null);
		}

		public event EventHandler LinkClicked;

		public HtmlTextView(Context context, IAttributeSet attrs) : base(context, attrs)
		{
			SetLinkTextColor(ColorHelper.LightBlue);
			MovementMethod = LinkMovementMethod.Instance;
		}

		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
			foreach (var hyperLink in _hyperLinks)
			{
				hyperLink.InternalUrlSpan.LinkClicked -= OnAccessLink;
			}
		}
	}
}
