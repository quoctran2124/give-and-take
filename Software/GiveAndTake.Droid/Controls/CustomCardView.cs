﻿using Android.Content;
using Android.Graphics;
using Android.Support.V7.Widget;
using Android.Util;
using GiveAndTake.Droid.Helpers;

namespace GiveAndTake.Droid.Controls
{
	public class CustomCardView : CardView
	{
		public CustomCardView(Context context, IAttributeSet attrs) : base(context, attrs)
		{
		}

		private string _cardBackgroundColorCustom;

		public string CardBackgroundColorCustom
		{
			get => _cardBackgroundColorCustom;
			set
			{
				_cardBackgroundColorCustom = value;

				SetCardBackgroundColor(!string.IsNullOrEmpty(value) ? ColorHelper.ToColor(value) : Color.BlueViolet);
			}
		}
	}
}