﻿using Android.Content;
using Android.Support.V4.View;
using Android.Views;
using Android.Widget;
using GiveAndTake.Core;
using GiveAndTake.Core.Models;
using System;
using System.Collections.Generic;

namespace GiveAndTake.Droid.Controls
{
	public class ImageSliderAdapter : PagerAdapter
	{
		public Action HandleItemSelected { get; set; }

		private readonly Context _context;
		private readonly List<Image> _imageData;

		public ImageSliderAdapter(Context context, List<Image> imageData)
		{
			_imageData = imageData;
			_context = context;
		}

		public override bool IsViewFromObject(View view, Java.Lang.Object @object)
		{
			return view == ((RelativeLayout)@object);
		}


		public override int Count => _imageData.Count == 0 ? 1 : _imageData.Count;

		public override void DestroyItem(ViewGroup container, int position, Java.Lang.Object objectValue)
		{
			container.RemoveView((RelativeLayout)objectValue);
		}

		public override Java.Lang.Object InstantiateItem(ViewGroup container, int position)
		{
			var inflater = _context.GetSystemService(Context.LayoutInflaterService) as LayoutInflater;
			var view = inflater?.Inflate(Resource.Layout.FragmentImage, null);
			var child = view?.FindViewById<CustomMvxCachedImageView>(Resource.Id.imgDisplay);
			if (child != null)
			{
				child.Click += (o, e) =>
				{
					HandleItemSelected?.Invoke();
				};
				child.ImageUrl = _imageData.Count == 0 ? AppConstants.DefaultPostImage : _imageData[position].OriginalImage;
			}

			container.AddView(view);
			return view;
		}
	}
}