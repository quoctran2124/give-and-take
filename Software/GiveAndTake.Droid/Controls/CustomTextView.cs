﻿using Android.Content;
using Android.Graphics;
using Android.Util;
using Android.Widget;
using GiveAndTake.Core;
using GiveAndTake.Core.Models;
using GiveAndTake.Droid.Helpers;
using I18NPortable;

namespace GiveAndTake.Droid.Controls
{
	public class CustomTextView : TextView
	{
		public CustomTextView(Context context) : base(context)
		{
		}

		public CustomTextView(Context context, IAttributeSet attrs) : base(context, attrs)
		{
		}

		public CustomTextView(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr)
		{
		}

		public CustomTextView(Context context, IAttributeSet attrs, int defStyleAttr, int defStyleRes) : base(context, attrs, defStyleAttr, defStyleRes)
		{
		}

		public bool IsSelected
		{
			set
			{
				SetTextColor(value ? Color.Black : ColorHelper.LightBlack);
				SetTypeface(null, value ? TypefaceStyle.Bold : TypefaceStyle.Normal);
			}
		}

		private bool _isEnable;
		public bool IsEnable
		{
			get => _isEnable;
			set
			{
				_isEnable = value;
				Activated = value;
				this.Alpha = (value) ? 1f : 0.5f;
			}
		}
	}
}