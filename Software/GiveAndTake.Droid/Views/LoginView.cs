using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Gms.Common;
using Android.Runtime;
using Android.Util;
using Android.Widget;
using Firebase.Iid;
using GiveAndTake.Core.Models;
using GiveAndTake.Core.ViewModels;
using GiveAndTake.Droid.Views.Base;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Commands;
using MvvmCross.Platforms.Android.Presenters.Attributes;
using System;
using Xamarin.Facebook;
using Xamarin.Facebook.Login;
using Object = Java.Lang.Object;
using Result = Android.App.Result;

namespace GiveAndTake.Droid.Views
{
	[MvxActivityPresentation]
	[Activity(Label = "GiveAndTake.Droid.Views", ScreenOrientation = ScreenOrientation.Portrait)]

	public class LoginView : BaseActivity
	{
		public IMvxCommand ShowConditionCommand { get; set; }
		public IMvxCommand<User> LoginCommand { get; set; }

		public IMvxAsyncCommand HandleLostConnectionCommand { get; set; }

		protected override int LayoutId => Resource.Layout.LoginView;

		private static AccessToken _accessToken;
		private ICallbackManager _callbackManager;
		private ImageButton _btnFacebookLogin;
		static readonly string TAG = "LoginView";
		
		public string FireBaseToken { get; set; }

		protected override void InitView()
		{
			_callbackManager = CallbackManagerFactory.Create();

			var loginCallback = new FacebookCallback<LoginResult>
			{
				HandleSuccess = OnLoginSuccess,
				HandleError = OnLoginFail
			};

			LoginManager.Instance.RegisterCallback(_callbackManager, loginCallback);

			_btnFacebookLogin = FindViewById<ImageButton>(Resource.Id.btnFb);
			_btnFacebookLogin.Click += OnLoginButtonClicked;
		}

		private void OnLoginFail(FacebookException obj)
		{
			HandleLostConnectionCommand.Execute();
		}

		private void ClickCommand()
		{
			ShowConditionCommand?.Execute();
		}
		protected override void CreateBinding()
		{
			base.CreateBinding();

			var bindingSet = this.CreateBindingSet<LoginView, LoginViewModel>();

			bindingSet.Bind(this)
				.For(v => v.LoginCommand)
				.To(vm => vm.LoginCommand);

			bindingSet.Bind(this)
				.For(v => v.HandleLostConnectionCommand)
				.To(vm => vm.HandleLostConnectionCommand);

			bindingSet.Apply();
		}

		protected override void OnViewModelSet()
		{
			base.OnViewModelSet();

			_accessToken = AccessToken.CurrentAccessToken;
			bool isLoggedIn = _accessToken != null && !_accessToken.IsExpired;
			if (isLoggedIn)
			{
				HandleSuccessfulLogin();
			}
		}

		protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
		{
			_callbackManager.OnActivityResult(requestCode, (int)resultCode, data);
			base.OnActivityResult(requestCode, resultCode, data);
		}

		protected override void Dispose(bool disposing)
		{
			_btnFacebookLogin.Click -= OnLoginButtonClicked;
			base.Dispose(disposing);
		}

		private void OnLoginButtonClicked(object sender, EventArgs e)
		{
			LoginManager.Instance.LogInWithReadPermissions(this, new[] {"public_profile", "email"});
		}

		private void OnLoginSuccess(LoginResult loginResult)
		{
			if (Profile.CurrentProfile == null)
			{
				new MyProfileTracker { HandleLogin = HandleSuccessfulLogin }.StartTracking();
			}
			else
			{
				HandleSuccessfulLogin();
			}
		}

		private void HandleSuccessfulLogin()
		{
			var result = IsPlayServicesAvailable();
			if (result)
			{
				FireBaseToken = FirebaseInstanceId.Instance.Token;
				Log.Debug(TAG, "InstanceID token: " + FirebaseInstanceId.Instance.Token);
			}
			LoginCommand.Execute(new User
			{
				FirstName = Profile.CurrentProfile.FirstName,
				LastName = Profile.CurrentProfile.LastName,
				DisplayName = Profile.CurrentProfile.Name,
				UserName = Profile.CurrentProfile.Id,
				AvatarUrl = GetProfilePicture(Profile.CurrentProfile.Id),
				SocialAccountId = Profile.CurrentProfile.Id
			});
			_btnFacebookLogin.Enabled = true;
		}

		private static string GetProfilePicture(string profileId) => $"https://graph.facebook.com/{profileId}/picture?type=large";

		public bool IsPlayServicesAvailable()
		{
			int resultCode = GoogleApiAvailability.Instance.IsGooglePlayServicesAvailable(this);
			return resultCode == ConnectionResult.Success;
		}
	}

	public class FacebookCallback<TResult> : Object, IFacebookCallback where TResult : Object
	{
		public Action HandleCancel { get; set; }
		public Action<FacebookException> HandleError { get; set; }
		public Action<TResult> HandleSuccess { get; set; }

		public void OnCancel() => HandleCancel?.Invoke();

		public void OnError(FacebookException error) => HandleError?.BeginInvoke(error, null, null);

	    public void OnSuccess(Object result) => HandleSuccess?.BeginInvoke(result.JavaCast<TResult>(), null, null);
    }

	public class MyProfileTracker : ProfileTracker
	{
		public Action HandleLogin { get; set; }
		protected override void OnCurrentProfileChanged(Profile oldProfile, Profile currentProfile)
		{
			StopTracking();
			HandleLogin?.Invoke();
		}
	}
}