﻿using Android.App;
using GiveAndTake.Droid.Views.Base;
using MvvmCross.Platforms.Android.Presenters.Attributes;

namespace GiveAndTake.Droid.Views
{
	[MvxActivityPresentation]
	[Activity]
	public class AvatarImageView : BaseActivity
	{
		protected override int LayoutId => Resource.Layout.AvatarImageView;

		protected override void InitView()
		{
		}
	}
}