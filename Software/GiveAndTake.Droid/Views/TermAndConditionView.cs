﻿using Android.App;
using GiveAndTake.Droid.Views.Base;
using MvvmCross.Platforms.Android.Presenters.Attributes;

namespace GiveAndTake.Droid.Views
{
	[MvxActivityPresentation]
	[Activity]
	public class TermAndConditionView : BaseActivity
	{
		protected override int LayoutId => Resource.Layout.TermAndConditionView;

		protected override void InitView()
		{
		}
	}
}