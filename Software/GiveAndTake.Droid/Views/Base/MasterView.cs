﻿using System;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using GiveAndTake.Core.ViewModels.Base;
using GiveAndTake.Droid.Views.TabNavigation;
using ME.Leolin.Shortcutbadger;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Commands;
using MvvmCross.Platforms.Android.Binding.BindingContext;
using MvvmCross.Platforms.Android.Presenters.Attributes;

namespace GiveAndTake.Droid.Views.Base
{
	[MvxActivityPresentation]
	[Activity(Label = "View for HomeViewModel", WindowSoftInputMode = SoftInput.AdjustPan, ScreenOrientation = ScreenOrientation.Portrait)]
	public class MasterView : BaseActivity
	{
		protected override int LayoutId => Resource.Layout.MasterView;
		public IMvxAsyncCommand ShowInitialViewModelsCommand { get; set; }
		public IMvxCommand BackPressedFromCreatePostCommand { get; set; }
		public IMvxCommand BackPressedFromHomeViewSearchedCommand { get; set; }
		public IMvxCommand BackPressedFromPostDetailCommand { get; set; }
		public IMvxCommand BackPressedFromRequestCommand { get; set; }
		public event EventHandler<int> BackToHomeViewTabFromAnotherTab;

		public static bool IsForeground;
		public bool IsHomeScreen = true;
		public int CurrentTabPosition;
		protected override void InitView()
		{		
		}

		protected override void OnResume()
		{
			base.OnResume();
			IsForeground = true;
			ShortcutBadger.RemoveCount(Android.App.Application.Context);
		}

		protected override void OnPause()
		{
			base.OnPause();
			IsForeground = false;
		}

		protected override void CreateBinding()
		{
			base.CreateBinding();
			var bindingSet = this.CreateBindingSet<MasterView, MasterViewModel>();

			bindingSet.Bind(this)
				.For(v => v.ShowInitialViewModelsCommand)
				.To(vm => vm.ShowInitialViewModelsCommand);

			bindingSet.Apply();
		}

		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);

			if (bundle == null)
			{
				ShowInitialViewModelsCommand.Execute();
			}
		}
		public override void OnBackPressed()
		{
			if (BackPressedFromCreatePostCommand != null)
			{
				BackPressedFromCreatePostCommand.Execute();				
			} else if (BackPressedFromHomeViewSearchedCommand != null)
			{
				BackPressedFromHomeViewSearchedCommand.Execute();
				BackPressedFromHomeViewSearchedCommand = null;
			}
			else if (BackPressedFromPostDetailCommand != null)
			{
				BackPressedFromPostDetailCommand.Execute();
				BackPressedFromPostDetailCommand = null;
			}
			else if (BackPressedFromRequestCommand != null)
			{
				BackPressedFromRequestCommand.Execute();
				BackPressedFromRequestCommand = null;
			}
			else
			{
				if (IsHomeScreen)
				{
					if (CurrentTabPosition == 1)
					{
						BackToHomeViewTabFromAnotherTab?.Invoke(this,1);
					}
					else if (CurrentTabPosition == 2)
					{
						BackToHomeViewTabFromAnotherTab?.Invoke(this, 2);
					}
					else if (CurrentTabPosition == 3)
					{
						BackToHomeViewTabFromAnotherTab?.Invoke(this, 3);
					}
					else
					{
						MoveTaskToBack(true);
					}
				}
				else
				{
					base.OnBackPressed();
				}				
		}
		}
	}
}