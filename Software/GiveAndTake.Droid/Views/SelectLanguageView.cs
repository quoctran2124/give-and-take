﻿using Android.App;
using Android.Support.V4.View;
using GiveAndTake.Core.Models;
using GiveAndTake.Core.ViewModels;
using GiveAndTake.Droid.Controls;
using GiveAndTake.Droid.Views.Base;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Commands;
using MvvmCross.Platforms.Android.Presenters.Attributes;
using System.Collections.Generic;

namespace GiveAndTake.Droid.Views
{
	[MvxActivityPresentation]
	[Activity]
	public class SelectLanguageView : BaseActivity
	{
		protected override int LayoutId => Resource.Layout.SelectLanguageView;

		protected override void InitView()
		{
		}
	}
}