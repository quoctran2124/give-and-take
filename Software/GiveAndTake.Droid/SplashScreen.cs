﻿using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using GiveAndTake.Core;
using GiveAndTake.Core.Helpers;
using GiveAndTake.Core.Models;
using ME.Leolin.Shortcutbadger;
using MvvmCross;
using MvvmCross.Droid.Support.V7.AppCompat;

namespace GiveAndTake.Droid
{
	[Activity( MainLauncher = true, 
		NoHistory = true,
		LaunchMode = LaunchMode.SingleTask,
		ScreenOrientation = ScreenOrientation.Portrait,
		Theme = "@style/SplashTheme")]
	[IntentFilter(new[] { Intent.ActionView },
		Categories = new[] { Intent.ActionView, Intent.CategoryBrowsable, Intent.CategoryDefault },
		DataScheme = "http",
		DataHost = "chovanhan.asia",
		AutoVerify = true)]
	public class SplashScreen : MvxSplashScreenAppCompatActivity<Setup, App>
	{
		private Core.Models.Notification _clickedNotification;
		private int _badgeValue;

		public SplashScreen()
			: base(Resource.Layout.SplashScreen)
		{

		}


		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			
		}

		protected override void OnResume()
		{
			base.OnResume();

			var notificationData = Intent?.Extras?.GetString("NotificationModelData");
			_badgeValue = (int) Intent?.Extras?.GetInt("BadgeData");
			bool isDataModelInitialized = Mvx.CanResolve<IDataModel>();

			if (notificationData != null)
			{

				var notification = JsonHelper.Deserialize<Core.Models.Notification>(notificationData);
				if (isDataModelInitialized)
				{
					var dataModel = Mvx.Resolve<IDataModel>();
					dataModel.Badge = _badgeValue;
					if (dataModel.IsLoggedIn)
					{
						_clickedNotification = notification;
						Finish();
					}

					Intent.Extras.Clear();
				}
				else
				{
					_clickedNotification = notification;
				}
			}
			else
			{
				if (isDataModelInitialized && Mvx.Resolve<IDataModel>().IsLoggedIn)
				{
					Finish();
				}
			}
		}

		protected override void OnDestroy()
		{
			base.OnDestroy();
			if (Mvx.CanResolve<IDataModel>())
			{				
				if (_clickedNotification != null)
				{
					var dataModel = Mvx.Resolve<IDataModel>();
					dataModel.SelectedNotification = _clickedNotification;
					dataModel.Badge = _badgeValue;
					dataModel.RaiseNotificationReceived(_clickedNotification);
					dataModel.RaiseBadgeUpdated(_badgeValue);
				}
			}
			ShortcutBadger.RemoveCount(Android.App.Application.Context);
			if (Intent != null)
			{
				if (Intent.Action == Intent.ActionView)
				{
					try
					{
						var dataModel = Mvx.Resolve<IDataModel>();
						dataModel.ApplinkUrl = Intent.DataString;
						dataModel.OnApplinkReceived();
					}
					catch
					{

					}

				}
			}
		}
	}
}