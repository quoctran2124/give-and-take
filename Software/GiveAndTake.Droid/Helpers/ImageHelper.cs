﻿using Android.Content;
using Android.Graphics;
using Android.Media;
using Android.Provider;
using MvvmCross;
using MvvmCross.Logging;
using System;
using System.IO;
using Android.Database;
using Uri = Android.Net.Uri;

namespace GiveAndTake.Droid.Helpers
{
	public class ImageHelper 
	{
		private static readonly IMvxLog Log = Mvx.Resolve<IMvxLogProvider>().GetLogFor<ImageHelper>();
		private const int QualityCompression = 100;
		public static string GetRealPathFromUri(ContentResolver contentResolver, Uri contentUri)
		{
			try
			{
				string fullPathToImage;

				var imageCursor = contentResolver.Query(contentUri, null, null, null, null);
				imageCursor.MoveToFirst();
				var idx = imageCursor.GetColumnIndex(MediaStore.Images.ImageColumns.Data);

				if (idx != -1)
				{
					fullPathToImage = imageCursor.GetString(idx);
				}
				else
				{
					var docId = DocumentsContract.GetDocumentId(contentUri);

					// The first element is a type and second element is a id
					var id = docId.Split(':')[1];
					const string whereSelect = MediaStore.Images.ImageColumns.Id + "=?";
					var projections = new[] { MediaStore.Images.ImageColumns.Data };

					var cursor = contentResolver.Query(MediaStore.Images.Media.InternalContentUri, projections, whereSelect, new[] { id }, null);
					if (cursor.Count == 0)
					{
						cursor = contentResolver.Query(MediaStore.Images.Media.ExternalContentUri, projections, whereSelect, new[] { id }, null);
					}

					var colData = cursor.GetColumnIndexOrThrow(MediaStore.Images.ImageColumns.Data);
					cursor.MoveToFirst();
					fullPathToImage = cursor.GetString(colData);
				}

				return fullPathToImage;
			}
			catch (Exception ex)
			{
				Log.Error("NOT get image path because" + ex.Message);
			}

			return null;
		}

		public static byte[] RotateImage(string path)
		{
			byte[] imageBytes;

			var originalImage = BitmapFactory.DecodeFile(path);
			var rotatedImage = originalImage;
			var rotation = GetRotation(path);

			if (rotation != 0)
			{
				var matrix = new Matrix();
				matrix.PostRotate(rotation);

				rotatedImage = Bitmap.CreateBitmap(originalImage, 0, 0, originalImage.Width, originalImage.Height,
					matrix, true);
			}

			using (var ms = new MemoryStream())
			{
				rotatedImage.Compress(Bitmap.CompressFormat.Jpeg, QualityCompression, ms);
				imageBytes = ms.ToArray();
			}

			originalImage.Recycle();
			rotatedImage.Recycle();
			originalImage.Dispose();
			rotatedImage.Dispose();

			return imageBytes;
		}

		private static int GetRotation(string filePath)
		{
			using (var ei = new ExifInterface(filePath))
			{
				var orientation = (Orientation)ei.GetAttributeInt(ExifInterface.TagOrientation, (int)Orientation.Normal);

				switch (orientation)
				{
					case Orientation.Rotate90:
						return 90;
					case Orientation.Rotate180:
						return 180;
					case Orientation.Rotate270:
						return 270;
					default:
						return 0;
				}
			}
		}
	}
}