﻿using Android.Runtime;
using GiveAndTake.Core;
using GiveAndTake.Core.Helpers.Interface;
using MvvmCross;
using MvvmCross.Platforms.Android;
using Xamarin.Facebook.Share.Model;
using Xamarin.Facebook.Share.Widget;

namespace GiveAndTake.Droid.Helpers
{
	public class FacebookHelper : Java.Lang.Object, IFacebookHelper
	{

		private ShareDialog _shareDialog;

		public FacebookHelper()
		{
			_shareDialog = new ShareDialog(Mvx.Resolve<IMvxAndroidCurrentTopActivity>().Activity);
		}

		public void ShareFacebookContent(string content, string contentUrl)
		{
			if (ShareDialog.CanShow(Java.Lang.Class.FromType(typeof(ShareLinkContent))))
			{
				var linkContent = new ShareLinkContent.Builder()
					.SetQuote(content)
					.SetContentUrl(Android.Net.Uri.Parse(contentUrl))
					.JavaCast<ShareLinkContent.Builder>()
					.Build();
				_shareDialog.Show(linkContent);
			}
		}
	}

}