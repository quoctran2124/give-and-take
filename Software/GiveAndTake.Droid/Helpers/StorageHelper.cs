﻿using Android.Content;
using Android.Preferences;
using GiveAndTake.Core;
using GiveAndTake.Core.Helpers.Interface;

namespace GiveAndTake.Droid.Helpers
{
	public class StorageHelper : IStorageHelper
	{
		public void SaveLanguage(LanguageType language)
		{
			ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(Android.App.Application.Context);
			ISharedPreferencesEditor editor = prefs.Edit();
			editor.PutString("chovanhan_language", language.ToString());
			editor.Apply();
		}	
	}
}