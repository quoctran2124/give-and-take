﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace GiveAndTake.Core.Models
{
	[DataContract]
	public class AppData
	{
		[DataMember(Name = "isFirstStartApp")]
		public bool IsNotFirstStartApp { get; set; } = false;

		[DataMember(Name = "language")]
		public LanguageType CurrentLanguage { get; set; }
	}
}
