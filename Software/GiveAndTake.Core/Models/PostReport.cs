﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace GiveAndTake.Core.Models
{
	[DataContract]
	public class PostReport
	{
		[DataMember(Name = "postId")]
		public string PostId { get; set; }

		[DataMember(Name = "message")]
		public string Message { get; set; }
	}
}
