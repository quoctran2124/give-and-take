﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace GiveAndTake.Core.Models
{
	[DataContract]
	public class ApiAppreciationResponse
	{
		[DataMember(Name = "statusCode")]
		public int StatusCode { get; set; }
		[DataMember(Name = "errorMessage")]
		public string ErrorMessage { get; set; }
		[DataMember(Name = "data")]
		public string Result { get; set; }
	}
}
