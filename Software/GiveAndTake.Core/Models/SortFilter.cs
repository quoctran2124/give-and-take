﻿namespace GiveAndTake.Core.Models
{
	public class SortFilter
	{
		public string FilterName { get; set; }

		public string FilterTag { get; set; }
	}
}