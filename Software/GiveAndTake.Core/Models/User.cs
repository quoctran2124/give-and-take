﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace GiveAndTake.Core.Models
{
    [DataContract]
    public class User
    {
	    [DataMember(Name = "id")]
	    public string Id { get; set; }

	    [DataMember(Name = "firstName")]
	    public string FirstName { get; set; }

	    [DataMember(Name = "lastName")]
	    public string LastName { get; set; }

	    [DataMember(Name = "displayName")]
	    public string DisplayName { get; set; }

	    [DataMember(Name = "username")]
	    public string UserName { get; set; }

	    [DataMember(Name = "socialAccountId")]
	    public string SocialAccountId { get; set; }

	    [DataMember(Name = "avatarUrl")]
	    public string AvatarUrl { get; set; }

		[DataMember(Name = "email")]
        public string Email { get; set; }

        [DataMember(Name = "birthdate")]
        public DateTimeOffset? BirthDate { get; set; }

        [DataMember(Name = "phoneNumber")]
        public string PhoneNumber { get; set; }

        [DataMember(Name = "address")]
        public string Address { get; set; }

        [DataMember(Name = "gender")]
        public string Gender { get; set; }

	    [DataMember(Name = "role")]
		public List<string> Role { get; set; }

	    [DataMember(Name = "status")]
		public string Status { get; set; }

		[DataMember(Name = "sentCount")]
		public int SentCount { get; set; }

	    [DataMember(Name = "appreciationNumber")]
	    public int AppreciationNumber { get; set; }

	    [DataMember(Name = "memberType")]
	    public string MemberType { get; set; }

	    [DataMember(Name = "avatarImageData")]
	    public string AvatarImageData { get; set; }
	}
}