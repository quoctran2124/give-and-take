﻿namespace GiveAndTake.Core.Models
{
	public class PopupMessageParam
	{
		public string Title { get; set; }
		public string Message { get; set; }
		public string ConfirmTitle { get; set; }
		public string CancelTitle { get; set; }
	}
}