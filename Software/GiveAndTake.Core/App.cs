﻿using System.Diagnostics;
using System.Globalization;
using System.Reflection;
using GiveAndTake.Core.Helpers;
using GiveAndTake.Core.Helpers.Interface;
using GiveAndTake.Core.Models;
using GiveAndTake.Core.Services;
using GiveAndTake.Core.ViewModels;
using I18NPortable;
using MvvmCross;
using MvvmCross.Base;
using MvvmCross.Plugin.File;
using MvvmCross.ViewModels;

namespace GiveAndTake.Core
{
	public class App : MvxApplication
	{
		public override void Initialize()
		{
			base.Initialize();

			RegisterAppDataLoader();
			RegisterHelpers();
			RegisterServices();
			InitLocales();

			var appDataLoader = Mvx.Resolve<IAppDataLoader>();
			var appData = appDataLoader.AppData;
			var dataModel = Mvx.Resolve<IDataModel>();

			if (!appData.IsNotFirstStartApp)
			{
				var currentCulture = CultureInfo.CurrentUICulture.Name.ToLower();
				var currentLanguage = LanguageType.vi;
				appData.CurrentLanguage = LanguageType.vi;
				dataModel.Language = LanguageType.vi;
				appDataLoader.SaveData(appData);
				I18N.Current.Locale = currentLanguage.ToString();
				RegisterAppStart<SelectLanguageViewModel>();
				return;
			}

			I18N.Current.Locale = appData.CurrentLanguage.ToString();
			dataModel.Language = appData.CurrentLanguage;
			RegisterAppStart<LoginViewModel>();
		}

		private void InitLocales()
		{
			I18N.Current
				.SetNotFoundSymbol("$") // Optional: when a key is not found, it will appear as $key$ (defaults to "$")
				.SetFallbackLocale(LanguageType.vi.ToString()) // Optional but recommended: locale to load in case the system locale is not supported
				.SetThrowWhenKeyNotFound(false) // Optional: Throw an exception when keys are not found (recommended only for debugging)
				.SetLogger(text => Debug.WriteLine(text)) // action to output traces
				.Init(GetType().GetTypeInfo().Assembly); // assembly where locales live
			I18N.Current.Locale = LanguageType.vi.ToString();
		}

		protected void RegisterHelpers()
		{
			Mvx.LazyConstructAndRegisterSingleton<IDataModel, DataModel>();
			var dataModel = new DataModel();
			Mvx.RegisterSingleton(dataModel);
			Mvx.RegisterSingleton<ILoadingOverlayService>(new LoadingOverlayService());
			Mvx.LazyConstructAndRegisterSingleton<IEmailHelper, EmailHelper>();
		}

		protected void RegisterAppDataLoader()
		{
			var fileStore = Mvx.Resolve<IMvxFileStore>();
			Mvx.RegisterSingleton(fileStore);

			var resourceLoader = Mvx.Resolve<IMvxResourceLoader>();
			Mvx.RegisterSingleton(resourceLoader);

			if (!Mvx.CanResolve<IAppDataLoader>())
			{
				var appDataLoader = new AppDataLoader(fileStore, resourceLoader);
				appDataLoader.Initialize();
				Mvx.RegisterSingleton<IAppDataLoader>(appDataLoader);
			}
		}

		protected void RegisterServices()
		{
			Mvx.LazyConstructAndRegisterSingleton<IManagementService, ManagementService>();
		}
	}
}
