﻿# key = value (the key will be the same across locales)

Giving = Đang cho
Gave = Đã cho
Pending = Đang đợi
Approved = Đã duyệt
Rejected = Đã từ chối
Received = Đã nhận
ApprovedRequest = Đã duyệt cho
ReceivedRequest = Đã nhận vật phẩm
PostIsDeleted = Bài đăng này đã bị xóa!

GivingStatus = Đang cho
GaveStatus = Đã cho
PendingStatus = Đang đợi
ApprovedStatus = Đã duyệt
ReceivedStatus = Đã nhận

Normal = Thành viên
Silver = Tấm lòng bạc
Gold = Tấm lòng vàng
Platinum = Tấm lòng bạch kim
Diamond = Tấm lòng kim cương

DefaultWarningMessage = Chức năng này chưa xong, \nXin vui lòng quay lại sau
ErrorMessage = Đã có lỗi xảy ra, \nXin vui lòng thử lại sau
ErrorConnectionMessage = Lỗi kết nối mạng, \nXin vui lòng thử lại
RequestRejectingMessage = Bạn có chắc chắn từ chối yêu cầu?
DeleteConfirmationMessage = Bạn có muốn xóa bài viết này?
SuccessfulAcceptanceMessage = Bạn đã chấp nhận thành công!
SuccessfulRejectionMessage = Bạn đã từ chối thành công!
CancelEditPostConfirmMessage = Bạn có muốn hủy chỉnh sửa?
CancelRequestConfirm = \nBạn có chắc chắn muốn bỏ yêu cầu ?\n
ReportPostNotAvailableMessage = Xin lỗi, Bạn đã báo cáo bài đăng này trước đó!
CancelEditProfileSettingConfirm = Thay đổi chưa được lưu, \nbạn có chắc chắn muốn hủy?
BlockUserConfirm = Bạn có chắc chắn muốn chặn \nngười dùng này?
BlockUserContent = Người dùng này sẽ không nhìn thấy bài viết cũng như yêu cầu nhận vật phẩm từ bạn cho đến khi bạn mở khóa

PopupChangeLanguageTitle = Chọn ngôn ngữ của bạn
SearchResultNullTitle = Không tìm thấy kết quả nào
SubmitTitle = Xác nhận
PopupCategoriesTitle = Phân loại
PopupSortFiltersTitle = Xếp theo
PopupLocationFiltersTitle = Lọc theo
PopupRequestDetailTitle = Chi tiết yêu cầu
PopupResponseTitle = Thông tin phản hồi
ButtonRejectTitle = Từ chối
ButtonApproveTitle = Chấp nhận
RequestsTitle = Danh sách yêu cầu
RequestApprovalTitle = Thông tin trao đổi
RankTitle = Xếp hạng
SentTitle = Đã cho
MyPostsTitle = Bài đăng của tôi
MyRequestsTitle = Yêu cầu đã gửi
CreatePostBtnSubmitTitle = Đăng
LoginTitle = Đăng nhập với tài khoản
LoadingDataOverlayTitle = Đang tải dữ liệu
UploadDataOverLayTitle = Đang đăng bài
UpdateOverLayTitle = Đang cập nhật
LoginProcessOverLayTitle = Tiến hành đăng nhập
ProcessingDataOverLayTitle = Đang xử lý
PopupReportTitle = Lý do báo cáo bài đăng:
BlockLabel = Khóa
SelectedImage = Đã chọn 0 hình
ImageProcessing = Đang tối ưu hóa hình ảnh..
PopupTitle = Thông tin trao đổi
SendTo = Gửi đến:
PopupInputInformationPlaceHolder = Thông tin trao đổi ...

InappropriateContentReason = Nội dung không phù hợp
InappropriatePictureReason = Chứa hình ảnh phản cảm
FakeContentReason = Nội dung không có thật
OtherReason = Lý do khác:

MarkGiven = Chuyển trạng thái sang "Đã cho"
MarkGiving = Chuyển trạng thái sang "Đang cho"
MarkReceived = Chuyển trạng thái sang "Đã nhận"

Modify = Chỉnh sửa
ViewPostRequests = Duyệt cho
Delete = Xóa
Report = Báo cáo
CancelRequest = Hủy yêu cầu
SaveAPost = Lưu
ConfirmChangeStatusOfPost = Bài đăng đã có yêu cầu nhận, \nbạn có chắc chắn muốn đóng yêu cầu?
ConfirmDeletePost = Bạn có chắc chắn muốn xóa bài đăng này?
EditProfile = Chỉnh sửa thông tin
SendFeedback = Gửi phản hồi
LogOut = Đăng xuất
BlockUser = Khóa người dùng
TakePhoto = Chụp ảnh
SelectPhoto = Chọn ảnh từ bộ sưu tập
CancelTitle = Hủy
Done =  Xong
NotFound = Không tìm thấy
SendTitleButton = Gửi
RequestReceiver = Gửi đến:
ChangeLanguage = Thay đổi ngôn ngữ
ConfirmChangeLanguage = Lưu thay đổi

About = Thông tin ứng dụng
AppInfo = Thông tin về ứng dụng Cho và nhận
Department = Sở Văn hóa - Thông tin
DaNangCity = Thành phố Đà Nẵng
MobileApp = Ứng dụng di động Cho và nhận
AppVersionLabel = Phiên bản ứng dụng:
ReleaseDateLabel = Ngày phát hành:
ReleaseDateValue = 14 tháng 02 năm 2019
SupportContactLabel = Liên hệ hỗ trợ:
DevelopedBy = Ứng dụng được phát triển bởi

Member = Thành viên"
Times = Lần
Time = Lần
UserNameLabel = Tên người dùng
TermAndCondition = Điều khoản và điều kiện sử dụng

CreatePostDescriptionPlaceHolder = Mô tả (Nhãn hiệu, kiểu dáng, màu sắc, ... )
CreatePostTitlePlaceHolder = Tiêu đề (Thương hiệu, thể loại, ...)
PopupReasonHint = Lý do tôi muốn báo cáo bài đăng này là...

Tất cả (mặc định) = Tất cả (mặc định)
Văn phòng phẩm = Văn phòng phẩm
Đồ dùng cá nhân = Đồ dùng cá nhân
Mẹ và bé = Mẹ và bé
Nội thất = Nột thất
Nội ngoại thất = Nội ngoại thất
Thú cưng = Thú cưng
Xe cộ = Xe cộ
Đồ điện tử = Đồ điện tử
Thức ăn = Thức ăn
Khác = Khác

DefaultCategoryCreatePostName = Văn phòng phẩm
DefaultLocationFilter = Đà Nẵng
Đà Nẵng = Đà Nẵng

SortByNewest = Mới nhất (Mặc định)
SortByOldest = Cũ nhất

FewSecondsAgo = Vài giây trước
MinutesAgo = phút trước
HoursAgo = giờ trước
Yesterday = Hôm qua
OneMinuteAgo = 1 phút trước
OneHourAgo = 1 giờ trước

Selected1Picture = Đã chọn 1 hình
SelectedPictures = Đã chọn {0} hình

ShareTitle =  Chia sẻ

ShareContentAddress = Khu vực
ShareContentCategory = Danh mục
ShareContentTitle = Vật phẩm
ShareContentCreatedDay = Ngày tạo
ShareContentDescription = Mô tả

AppUpdateTitle = Phiên bản mới đã sẵn sàng
AppUpdateMessage = Vui lòng cập nhật ứng dụng để tiếp tục sử dụng
AppUpdateUpdateButtonText = Cập nhật ngay