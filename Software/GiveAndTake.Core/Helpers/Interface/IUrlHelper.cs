﻿namespace GiveAndTake.Core.Helpers.Interface
{
	public interface IUrlHelper
	{
		void OpenUrl(string url);
		void OpenStorePage();
	}
}
