﻿namespace GiveAndTake.Core.Helpers.Interface
{
	public interface IStorageHelper
	{
		void SaveLanguage(LanguageType language);
	}
}
