﻿using System;
using System.Collections.Generic;
using GiveAndTake.Core.Models;

namespace GiveAndTake.Core.Helpers.Interface
{
	public interface IFacebookHelper
	{
		void ShareFacebookContent(string content, string contentUrl);
	}
}
