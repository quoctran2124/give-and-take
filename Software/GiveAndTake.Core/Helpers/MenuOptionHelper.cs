﻿using System.Collections.Generic;
using I18NPortable;

namespace GiveAndTake.Core.Helpers
{
	public static class MenuOptionHelper
	{
		private static II18N Strings => I18N.Current;
		public static List<string> GivingPostOptions => new List<string>
		{
			Strings["MarkGiven"],
			Strings["Modify"],
			Strings["ViewPostRequests"],
			Strings["Delete"]
		};

		public static List<string> GivenPostOptions => new List<string>
		{
			Strings["MarkGiving"],
			Strings["Modify"],
			Strings["ViewPostRequests"],
			Strings["Delete"]
		};

		public static List<string> PendingPostOptions => new List<string>
		{
			Strings["CancelRequest"],
			Strings["Report"]
		};

		public static List<string> ApprovedPostOptions => new List<string>
		{
			Strings["MarkReceived"],
			Strings["Report"]
		};

		public static List<string> ReceivedPostOptions => new List<string>
		{
			Strings["Report"]
		};

		public static List<string> OtherProfileMenuOption => new List<string>
		{
			Strings["BlockUser"]
		};

		public static List<string> GetMenuOptions(string status)
		{
			if (status == Strings["Giving"])
			{
				return GivingPostOptions;
			}
			else if (status == Strings["Gave"])
			{
				return GivenPostOptions;
			}
			else if (status == Strings["Pending"])
			{
				return PendingPostOptions;
			}
			else if (status == Strings["Approved"])
			{
				return ApprovedPostOptions;
			}
			else if (status == Strings["Received"])
			{
				return ReceivedPostOptions;
			}
			else
			{
				return ReceivedPostOptions;
			}
		}
	}
}