﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;

namespace GiveAndTake.Core.Helpers
{
	public static class PermissionHelper
	{
		public static void GrandPermission(List<Permission> permissions, Action action)
		{
			Task.Run(async () =>
			{
				foreach (var permission in permissions)
				{
					if (await GrandPermission(permission) != PermissionStatus.Granted)
					{
						return;
					}
				}

				action();
			});
		}

		private static async Task<PermissionStatus> GrandPermission(Permission permission)
		{
			var status = await CrossPermissions.Current.CheckPermissionStatusAsync(permission);
			if (status != PermissionStatus.Granted)
			{
				var results = await CrossPermissions.Current.RequestPermissionsAsync(permission);
				if (results.ContainsKey(permission))
				{
					status = results[permission];
				}
			}

			return status;
		}
	}
}