﻿using System.Collections.Generic;
using I18NPortable;
using MvvmCross.UI;

namespace GiveAndTake.Core.Helpers
{
	public class ColorHelper
	{
		public static MvxColor Red = MvxColor.ParseHexString("#C2272D");
		public static MvxColor Green = MvxColor.ParseHexString("#24B574");
		public static MvxColor Brown = MvxColor.ParseHexString("#666867");
		public static MvxColor Black = MvxColor.ParseHexString("#000000");

		private static II18N Strings => I18N.Current;

		public Dictionary<string, MvxColor> StatusColors = new Dictionary<string, MvxColor>()
		{
			{Strings["Pending"], Green },
			{Strings["Approved"], Red },
			{Strings["Received"], Brown },
			{Strings["Giving"], Green },
			{Strings["Gave"], Brown },
			{Strings["ApprovedRequest"], Green },
			{Strings["ReceivedRequest"], Brown }
		};

		public MvxColor GetStatusColor(string status)
		{
			return !string.IsNullOrEmpty(status) && StatusColors.ContainsKey(status) ? StatusColors[status] : Black;
		}
	}
}
