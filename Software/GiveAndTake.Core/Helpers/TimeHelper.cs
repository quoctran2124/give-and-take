﻿using System;
using I18NPortable;

namespace GiveAndTake.Core.Helpers
{
	public class TimeHelper
	{
		private static II18N Strings => I18N.Current;
		public static string ToTimeAgo(DateTime dateTime)
		{
			var timeSpan = DateTime.Now.Subtract(dateTime);
			if (timeSpan < TimeSpan.FromSeconds(60))
			{
				return Strings["FewSecondsAgo"];
			}
			if (timeSpan < TimeSpan.FromMinutes(60))
			{
				if (timeSpan.Minutes == 1)
				{
					return Strings["OneMinuteAgo"];
				}
				else
				{
					return $"{timeSpan.Minutes}" + " " + Strings["MinutesAgo"];
				}
			}
			if (timeSpan <= TimeSpan.FromHours(24))
			{
				if (timeSpan.Hours == 1)
				{
					return Strings["OneHourAgo"];
				}
				else if (timeSpan.Hours == 24)
				{
					return Strings["Yesterday"];
				}
				else
				{
					return $"{timeSpan.Hours}" + " " + Strings["HoursAgo"];
				}
			}			
			return dateTime.ToString(AppConstants.DateStringFormat);
		}
	}
}
