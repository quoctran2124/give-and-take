﻿using GiveAndTake.Core.Helpers;
using GiveAndTake.Core.Helpers.Interface;
using GiveAndTake.Core.ViewModels.Base;
using MvvmCross.Commands;

namespace GiveAndTake.Core.ViewModels
{
	public class AboutViewModel : BaseViewModel
	{
		public string AppVersionValue => _systemHelper.GetAppVersion();
		public string AppInfoLabel => Strings["AppInfo"];
		public string DepartmentLabel => Strings["Department"];
		public string DaNangCityLabel => Strings["DaNangCity"];
		public string MobileAppLabel => Strings["MobileApp"];
		public string AppVersionLabel => Strings["AppVersionLabel"];
		public string ReleaseDateLabel => Strings["ReleaseDateLabel"];
		public string ReleaseDateValue => Strings["ReleaseDateValue"];
		public string SupportContactLabel => Strings["SupportContactLabel"];
		public string SupportContactValue => AppConstants.SupportContactValue;
		public string DevelopedBy => Strings["DevelopedBy"];
		public IMvxCommand BackPressedCommand => _backPressedCommand ?? (_backPressedCommand = new MvxCommand(BackPressed));
		public IMvxCommand PhoneDialerCommand => _phoneDialerCommand ?? (_phoneDialerCommand = new MvxCommand(ShowPhoneDialer));

		private readonly ISystemHelper _systemHelper;
		private readonly IUrlHelper _urlHelper;
		private IMvxCommand _backPressedCommand;
		private IMvxCommand _phoneDialerCommand;

		public AboutViewModel(ISystemHelper iSystemHelper, IUrlHelper iUrlHelper)
		{
			_systemHelper = iSystemHelper;
			_urlHelper = iUrlHelper;
		}

		private async void BackPressed()
		{
			await NavigationService.Close(this);
		}

		private void ShowPhoneDialer()
		{
			_urlHelper.OpenUrl("tel:" + AppConstants.SupportContactPhone);
		}
	}
}
