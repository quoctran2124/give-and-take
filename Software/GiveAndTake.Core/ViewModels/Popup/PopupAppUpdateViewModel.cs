﻿using GiveAndTake.Core.Helpers.Interface;
using GiveAndTake.Core.ViewModels.Base;
using I18NPortable;
using MvvmCross.Commands;

namespace GiveAndTake.Core.ViewModels.Popup
{
	public class PopupAppUpdateViewModel : BaseViewModel
	{
		public IMvxCommand UpdateCommand => _submitCommand ?? (_submitCommand = new MvxCommand(OnUpdate));

		public IMvxCommand CloseCommand => _closeCommand ?? (_closeCommand = new MvxCommand(OnClose));

		public string Title => Strings["AppUpdateTitle"];

		public string Message => Strings["AppUpdateMessage"];

		public string UpdateButtonTitle => Strings["AppUpdateUpdateButtonText"];

		private IMvxCommand _submitCommand;
		private IMvxCommand _closeCommand;
		private readonly IUrlHelper _urlHelper;

		public PopupAppUpdateViewModel(IUrlHelper urlHelper)
		{
			_urlHelper = urlHelper;
		}

		private void OnUpdate()
		{
			_urlHelper.OpenStorePage();
		}

		private void OnClose()
		{
			NavigationService.Close(this);
		}
	}
}