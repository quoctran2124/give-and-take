﻿using FFImageLoading.Transformations;
using FFImageLoading.Work;
using GiveAndTake.Core.Models;
using GiveAndTake.Core.ViewModels.Base;
using MvvmCross.Commands;
using System.Collections.Generic;
using System.Threading.Tasks;
using GiveAndTake.Core.Exceptions;
using GiveAndTake.Core.Services;

namespace GiveAndTake.Core.ViewModels.Popup
{
	public class PopupResponseViewModel : BaseViewModel<Request, RequestStatus>
	{
		#region Properties
		public string PopupTitle => Strings["PopupTitle"];
		public string SendTo => Strings["SendTo"];
		public string PopupInputInformationPlaceHolder => Strings["PopupInputInformationPlaceHolder"];
		public string BtnSubmitTitle => Strings["SendTitleButton"];
		public string BtnCancelTitle => Strings["CancelTitle"];

		public IMvxCommand CloseCommand => _closeCommand ?? (_closeCommand = new MvxAsyncCommand(OnClose));

		public IMvxCommand SubmitCommand => _submitCommand ?? (_submitCommand = new MvxAsyncCommand(OnSubmit));

		public string AvatarUrl
		{
			get => _avatarUrl;
			set => SetProperty(ref _avatarUrl, value);
		}

		public string UserName
		{
			get => _userName;
			set => SetProperty(ref _userName, value);
		}

		public string RequestDescription
		{
			get => _requestDescription;
			set
			{
				SetProperty(ref _requestDescription, value);
				UpdateSubmitBtn();
			}
		}

		public bool IsSubmitBtnEnabled
		{
			get => _isSubmitBtnEnabled;
			set => SetProperty(ref _isSubmitBtnEnabled, value);
		}

		public List<ITransformation> AvatarTransformations => new List<ITransformation> { new CircleTransformation() };

		private Request _request;
		private readonly IDataModel _dataModel;
		private string _avatarUrl;
		private string _userName;
		private string _requestDescription;
		private bool _isSubmitBtnEnabled;
		private IMvxCommand _closeCommand;
		private IMvxCommand _submitCommand;
		private readonly ILoadingOverlayService _overlay;

		#endregion

		#region Constructor

		public PopupResponseViewModel(IDataModel dataModel, ILoadingOverlayService loadingOverlayService)
		{
			_dataModel = dataModel;
			_overlay = loadingOverlayService;
		}

		public override void Prepare(Request request)
		{
			_request = request;
			AvatarUrl = request.User.AvatarUrl;
			UserName = request.User.DisplayName ?? AppConstants.DefaultUserName;
		}

		public async Task OnSubmit()
		{
			bool success = false;
			try
			{
				var response = new RequestResponse
				{
					RequestId = _request.Id,
					ResponseMessage = RequestDescription
				};
				await _overlay.ShowOverlay(Strings["ProcessingDataOverLayTitle"]);
				await ManagementService.CreateResponse(response, _dataModel.LoginResponse.Token);
				success = true;				
			}
			catch (AppException.ApiException)
			{
				await NavigationService.Navigate<PopupWarningViewModel, string, bool>(Strings["ErrorConnectionMessage"]);
			}
			finally
			{
				await _overlay.CloseOverlay();
				await Task.Delay(777); // for iphone, dont delete. Solve issues such as: OnRequestAccepted(Request request) in NotificaitonViewModel
				if (success)
				{
					await NavigationService.Close(this, RequestStatus.Submitted);
				}
			}
		}

		public void UpdateSubmitBtn() => IsSubmitBtnEnabled = !string.IsNullOrEmpty(_requestDescription);

		private async Task OnClose() => await NavigationService.Close(this, RequestStatus.Cancelled);

		#endregion
	}
}
