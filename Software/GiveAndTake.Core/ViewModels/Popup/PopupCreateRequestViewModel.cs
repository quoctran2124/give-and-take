﻿using FFImageLoading.Transformations;
using FFImageLoading.Work;
using GiveAndTake.Core.Exceptions;
using GiveAndTake.Core.Models;
using GiveAndTake.Core.Services;
using GiveAndTake.Core.ViewModels.Base;
using MvvmCross.Commands;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;

namespace GiveAndTake.Core.ViewModels.Popup
{
	public class PopupCreateRequestViewModel : BaseViewModel<Post, RequestStatus>
	{
		#region Properties

		private IMvxCommand _closeCommand;
		private ICommand _submitCommand;

		private readonly IDataModel _dataModel;
		private string _avatarUrl;
		private string _userName;
		private string _requestDescription;
		private string _postId;
		private string _userId;
		private bool _isSubmitBtnEnabled;
		private readonly ILoadingOverlayService _overlay;

		public string PopupTitle => Strings["PopupTitle"];
		public string SendTo => Strings["SendTo"];
		public string PopupInputInformationPlaceHolder => Strings["PopupInputInformationPlaceHolder"];
		public string BtnSubmitTitle => Strings["SendTitleButton"];
		public string BtnCancelTitle => Strings["CancelTitle"];

		public string AvatarUrl
		{
			get => _avatarUrl;
			set => SetProperty(ref _avatarUrl, value);
		}

		public string UserName
		{
			get => _userName;
			set => SetProperty(ref _userName, value);
		}

		public string RequestDescription
		{
			get => _requestDescription;
			set
			{
				SetProperty(ref _requestDescription, value);
				UpdateSubmitBtn();
			} 
		}

		public bool IsSubmitBtnEnabled
		{
			get => _isSubmitBtnEnabled;
			set => SetProperty(ref _isSubmitBtnEnabled, value);
		}

		public List<ITransformation> AvatarTransformations => new List<ITransformation> { new CircleTransformation() };

		#endregion

		#region Constructor

		public PopupCreateRequestViewModel(IDataModel dataModel, ILoadingOverlayService loadingOverlayService)
		{
			_dataModel = dataModel;
			_overlay = loadingOverlayService;
		}

		public override void Prepare(Post post)
		{
			_postId = post.PostId;
			_userId = post.User.Id;
			AvatarUrl = post.User.AvatarUrl;
			UserName = post.User.DisplayName ?? AppConstants.DefaultUserName;
		}

		public IMvxCommand CloseCommand =>
			_closeCommand ?? (_closeCommand = new MvxAsyncCommand(() => NavigationService.Close(this, RequestStatus.Cancelled)));
		public ICommand SubmitCommand => _submitCommand ?? (_submitCommand = new MvxAsyncCommand(InitCreateNewRequest));


		public async Task InitCreateNewRequest()
		{
			bool success = false;
			try
			{
				await _overlay.ShowOverlay(Strings["ProcessingDataOverLayTitle"]);
				var request = new Request()
				{
					PostId = _postId,
					UserId = _userId,
					RequestMessage = RequestDescription,
				};

				var result = await ManagementService.CreateRequest(request, _dataModel.LoginResponse.Token);
				if (result)
				{
					await NavigationService.Navigate<PopupWarningViewModel, string>(Strings["ErrorMessage"]);
				}
				success = true;					
			}
			catch (AppException.ApiException)
			{
				await NavigationService.Navigate<PopupWarningViewModel, string, bool>(Strings["ErrorConnectionMessage"]);
			}
			finally
			{
				await _overlay.CloseOverlay(777);
				if (success)
				{
					await NavigationService.Close(this, RequestStatus.Submitted);
				}
			}
		}

		public void UpdateSubmitBtn() => IsSubmitBtnEnabled = !string.IsNullOrEmpty(_requestDescription);

		#endregion
	}
}
