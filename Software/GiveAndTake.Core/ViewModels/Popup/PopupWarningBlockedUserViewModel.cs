﻿using System;
using System.Collections.Generic;
using System.Text;
using GiveAndTake.Core.Helpers.Interface;
using GiveAndTake.Core.ViewModels.Base;
using MvvmCross.Commands;

namespace GiveAndTake.Core.ViewModels.Popup
{
	public class PopupWarningBlockedUserViewModel : BaseViewModelResult<bool>
	{
		public IMvxAsyncCommand CloseCommand { get; set; }
		private IMvxCommand _mailingCommand;
		public IMvxCommand MailingCommand =>
			_mailingCommand ?? (_mailingCommand = new MvxCommand(InitNewEmail));

		public string HtmlContent => DataModel.Language == LanguageType.vi ? AppConstants.WarningBlockedUserMessageVn : AppConstants.WarningBlockedUserMessageEn;

		private readonly IEmailHelper _emailHelper;

		public PopupWarningBlockedUserViewModel(IEmailHelper emailHelper)
		{
			_emailHelper = emailHelper;
			CloseCommand = new MvxAsyncCommand(() => NavigationService.Close(this, true));
		}

		private void InitNewEmail()
		{
			_emailHelper.OpenFeedbackEmail();
		}

		public string CloseButtonTitle => Strings["SubmitTitle"];
	}
}
