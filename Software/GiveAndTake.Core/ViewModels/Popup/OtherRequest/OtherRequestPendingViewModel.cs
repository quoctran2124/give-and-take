﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FFImageLoading.Transformations;
using FFImageLoading.Work;
using GiveAndTake.Core.Models;
using GiveAndTake.Core.ViewModels.Base;
using MvvmCross.Commands;

namespace GiveAndTake.Core.ViewModels.Popup.OtherRequest
{
	public class OtherRequestPendingViewModel : BaseViewModel<Request, PopupRequestStatusResult>
	{
		public string PopupTitle => Strings["PopupRequestDetailTitle"];

		public string BtnRejectTitle => Strings["ButtonRejectTitle"];

		public string BtnApproveTitle => Strings["ButtonApproveTitle"];

		public IMvxCommand RejectCommand => _rejectCommand ?? (_rejectCommand = new MvxCommand(HandleOnRejected));

		public IMvxCommand ApproveCommand => _acceptCommand ?? (_acceptCommand = new MvxCommand(HandleOnApproved));

		public IMvxCommand CloseCommand => _closeCommand ?? (_closeCommand = new MvxCommand(HandleOnClosed));

		public IMvxCommand ShowPostDetailCommand => _showPostDetailCommand ?? (_showPostDetailCommand = new MvxCommand(HandleOnShowPostDetail));
		public IMvxCommand ShowTakerProfileCommand => _showTakerProfileCommand ?? (_showTakerProfileCommand = new MvxCommand(HandleOnShowTakerProfileCommand));

		public List<ITransformation> PostTransformations => new List<ITransformation> { new CornersTransformation(15, CornerTransformType.AllRounded) };

		public List<ITransformation> AvatarTransformations => new List<ITransformation> { new CircleTransformation() };

		public string TakerFullName
		{
			get => _takerFullName;
			set => SetProperty(ref _takerFullName, value);
		}

		public string TakerAvatarUrl
		{
			get => _takerAvatarUrl;
			set => SetProperty(ref _takerAvatarUrl, value);
		}

		public string PostUrl
		{
			get => _postUrl;
			set => SetProperty(ref _postUrl, value);
		}

		public string RequestMessage
		{
			get => _requestMessage;
			set => SetProperty(ref _requestMessage, value);
		}

		public string RequestCreatedDate
		{
			get => _requestCreatedDate;
			set => SetProperty(ref _requestCreatedDate, value);
		}

		private Request _request;
		private IMvxCommand _rejectCommand;
		private IMvxCommand _acceptCommand;
		private IMvxCommand _closeCommand;
		private IMvxCommand _showPostDetailCommand;
		private IMvxCommand _showTakerProfileCommand;
		private string _takerFullName;
		private string _takerAvatarUrl;
		private string _postUrl;
		private string _requestCreatedDate;
		private string _requestMessage;

		public override void Prepare(Request request)
		{
			_request = request;
		}

		public override Task Initialize()
		{
			TakerAvatarUrl = _request.User.AvatarUrl;
			PostUrl = _request.Post.Images != null && _request.Post.Images.Any() ? _request.Post.Images.ElementAt(0).OriginalImage : "";
			TakerFullName = _request.User.DisplayName ?? AppConstants.DefaultUserName;
			RequestCreatedDate = _request.CreatedTime.ToString(AppConstants.DateStringFormat);
			RequestMessage = _request.RequestMessage;
			return base.Initialize();
		}

		private void HandleOnRejected() => NavigationService.Close(this, PopupRequestStatusResult.Rejected);

		private void HandleOnApproved() => NavigationService.Close(this, PopupRequestStatusResult.Approved);

		private void HandleOnClosed() => NavigationService.Close(this, PopupRequestStatusResult.Cancelled);

		private void HandleOnShowPostDetail() => NavigationService.Close(this, PopupRequestStatusResult.ShowPostDetail);
		private void HandleOnShowTakerProfileCommand() => NavigationService.Close(this, PopupRequestStatusResult.ShowTakerProfile);
	}
}
