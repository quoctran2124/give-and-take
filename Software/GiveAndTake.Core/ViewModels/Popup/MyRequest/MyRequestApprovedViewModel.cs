﻿using GiveAndTake.Core.ViewModels.Base;
using MvvmCross.Commands;

namespace GiveAndTake.Core.ViewModels.Popup.MyRequest
{
	public class MyRequestApprovedViewModel : BaseRequestApprovedViewModel
	{
		public string BtnReceivedTitle => Strings["ReceivedRequest"];
		public string BtnRemoveRequestTitle => Strings["CancelRequest"];
		public IMvxCommand RemoveRequestCommand => _removeRequestCommand ?? (_removeRequestCommand = new MvxCommand(HandleOnRemoved));
		public IMvxCommand ReceivedCommand => _receivedCommand ?? (_receivedCommand = new MvxCommand(HandleOnReceived));

		private IMvxCommand _removeRequestCommand;
		private IMvxCommand _receivedCommand;
		
		private void HandleOnRemoved() => NavigationService.Close(this, PopupRequestStatusResult.Removed);

		private void HandleOnReceived() => NavigationService.Close(this, PopupRequestStatusResult.Received);

	}
}
