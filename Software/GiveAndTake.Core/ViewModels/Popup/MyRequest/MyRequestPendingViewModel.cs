﻿using GiveAndTake.Core.Models;
using GiveAndTake.Core.ViewModels.Base;
using MvvmCross.Commands;
using System.Threading.Tasks;

namespace GiveAndTake.Core.ViewModels.Popup.MyRequest
{
	public class MyRequestPendingViewModel : BaseViewModel<Request, PopupRequestStatusResult>
	{
		public string MyRequestPopupTitle => Strings["MyRequestsTitle"];
		public string BtnRemoveRequestTitle => Strings["CancelRequest"];
		public string SentTo => Strings["RequestReceiver"];
		public IMvxCommand CloseCommand => _closeCommand ?? (_closeCommand = new MvxCommand(HandleOnClosed));
		public IMvxCommand RemoveRequestCommand => _removeRequestCommand ?? (_removeRequestCommand = new MvxCommand(HandleOnRemoved));
		public IMvxCommand ShowGiverProfileCommand => _showGiverProfileCommand ?? (_showGiverProfileCommand = new MvxCommand(HandleOnShowGiverProfileCommand));

		private string _lbGiverFullName;
		private string _lbRequestDate;
		private string _lbMyRequestMessage;

		private Request _request;

		private IMvxCommand _closeCommand;
		private IMvxCommand _removeRequestCommand;
		private IMvxCommand _showGiverProfileCommand;

		public string GiverFullName
		{
			get => _lbGiverFullName;
			set => SetProperty(ref _lbGiverFullName, value);
		}

		public string RequestDate
		{
			get => _lbRequestDate;
			set => SetProperty(ref _lbRequestDate, value);
		}

		public string MyRequestMessage
		{
			get => _lbMyRequestMessage;
			set => SetProperty(ref _lbMyRequestMessage, value);
		}

		public override void Prepare(Request request)
		{
			_request = request;
		}

		public override Task Initialize()
		{
			GiverFullName = _request.Post.User.DisplayName ?? AppConstants.DefaultUserName;
			RequestDate = _request.CreatedTime.ToString(AppConstants.DateStringFormat);
			MyRequestMessage = _request.RequestMessage;
			return base.Initialize();
		}

		private void HandleOnClosed() => NavigationService.Close(this, PopupRequestStatusResult.Cancelled);
		private void HandleOnRemoved() => NavigationService.Close(this, PopupRequestStatusResult.Removed);
		private void HandleOnShowGiverProfileCommand() => NavigationService.Close(this, PopupRequestStatusResult.ShowGiverProfile);
	}
}
