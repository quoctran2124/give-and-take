﻿using System.Threading.Tasks;
using GiveAndTake.Core.Exceptions;
using GiveAndTake.Core.Models;
using GiveAndTake.Core.Services;
using GiveAndTake.Core.ViewModels.Base;
using MvvmCross.Commands;

namespace GiveAndTake.Core.ViewModels.Popup
{
	public class PopupReportViewModel: BaseViewModel<Post, bool>
	{
		private IMvxCommand _closeCommand;
		private IMvxCommand _submitCommand;
		private IMvxCommand _inappropriateContentClickCommand;
		private IMvxCommand _inappropriatePictureClickCommand;
		private IMvxCommand _fakeContentClickCommand;
		private IMvxCommand _otherClickCommand;
		private bool _isInappropriateContentReasonChecked;
		private bool _isInappropriatePictureReasonChecked;
		private bool _isFakeContentReasonChecked;
		private bool _isOtherReasonChecked;

		private PopupReason _chosenPopupReason;
		private string _otherReasonContent;
		private string _postReportId;

		private readonly ILoadingOverlayService _overlay;
		public PopupReportViewModel(ILoadingOverlayService overlay)
		{
			IsInappropriateContentReasonChecked = true;
			_chosenPopupReason = PopupReason.InappropriateContentReason;
			_overlay = overlay;
		}

		public string PopupTitle => Strings["PopupReportTitle"];
		public string InappropriateContentReason => Strings["InappropriateContentReason"];
		public string InappropriatePictureReason => Strings["InappropriatePictureReason"];
		public string FakeContentReason => Strings["FakeContentReason"];
		public string OtherReason => Strings["OtherReason"];
		public string BtnCancelTitle => Strings["CancelTitle"];
		public string BtnSubmitTitle => Strings["SendTitleButton"];
		public string TextHint => Strings["PopupReasonHint"];
		
		public IMvxCommand CloseCommand => _closeCommand ?? (_closeCommand = new MvxCommand(() => NavigationService.Close(this, false)));
		public IMvxCommand SubmitCommand => _submitCommand ?? (_submitCommand = new MvxAsyncCommand(OnReportSubmitted));
		public IMvxCommand InappropriateContentClickCommand => _inappropriateContentClickCommand ?? (_inappropriateContentClickCommand = new MvxCommand<PopupReason>(OnReasonClicked));
		public IMvxCommand InappropriatePictureClickCommand => _inappropriatePictureClickCommand ?? (_inappropriatePictureClickCommand = new MvxCommand<PopupReason>(OnReasonClicked));
		public IMvxCommand FakeContentClickCommand => _fakeContentClickCommand ?? (_fakeContentClickCommand = new MvxCommand<PopupReason>(OnReasonClicked));
		public IMvxCommand OtherClickCommand => _otherClickCommand ?? (_otherClickCommand = new MvxCommand<PopupReason>(OnReasonClicked));

		public bool IsInappropriateContentReasonChecked
		{
			get => _isInappropriateContentReasonChecked;
			set
			{
				SetProperty(ref _isInappropriateContentReasonChecked, value);
				if (value) _chosenPopupReason = PopupReason.InappropriateContentReason;
			}
		}

		public bool IsInappropriatePictureReasonChecked
		{
			get => _isInappropriatePictureReasonChecked;
			set
			{
				SetProperty(ref _isInappropriatePictureReasonChecked, value);
				if (value) _chosenPopupReason = PopupReason.InappropriatePictureReason;
			}
		}
		public bool IsFakeContentReasonChecked
		{
			get => _isFakeContentReasonChecked;
			set
			{
				SetProperty(ref _isFakeContentReasonChecked, value);
				if (value) _chosenPopupReason = PopupReason.FakeContentReason;
			}
		}

		public bool IsOtherReasonChecked
		{
			get => _isOtherReasonChecked;
			set
			{
				SetProperty(ref _isOtherReasonChecked, value);
				if (value) _chosenPopupReason = PopupReason.OtherReason;
			}
		}

		public string OtherReasonContent
		{
			get => _otherReasonContent;
			set => SetProperty(ref _otherReasonContent, value);
		}

		public override void Prepare(Post post)
		{
			_postReportId = post.PostId;
		}

		private async Task OnReportSubmitted()
		{
			var reasonMessage = GetReasonMessage();
			bool isSubmitted = false;
			try
			{
				var postReport = new PostReport()
				{
					PostId = _postReportId,
					Message = reasonMessage
				};
				await _overlay.ShowOverlay(Strings["UpdateOverLayTitle"]);
				await ManagementService.CreatePostReport(postReport, DataModel.LoginResponse.Token);
				isSubmitted = true;
			}
			catch (AppException.ApiException)
			{
				await NavigationService.Navigate<PopupWarningViewModel, string, bool>(Strings["ErrorConnectionMessage"]);
			}
			finally
			{
				await _overlay.CloseOverlay();
				if (isSubmitted)
				{
					await NavigationService.Close(this, true);
				}
			}
		}

		private void OnReasonClicked(PopupReason reason)
		{
			SetAllReasonToBeFalse();
			switch (reason)
			{
				case PopupReason.InappropriateContentReason:
					IsInappropriateContentReasonChecked = true;
					break;
				case PopupReason.InappropriatePictureReason:
					IsInappropriatePictureReasonChecked = true;
					break;
				case PopupReason.FakeContentReason:
					IsFakeContentReasonChecked = true;
					break;
				case PopupReason.OtherReason:
					IsOtherReasonChecked = true;
					break;
			}
			_chosenPopupReason = reason;
		}

		private void SetAllReasonToBeFalse()
		{
			IsInappropriateContentReasonChecked = false;
			IsInappropriatePictureReasonChecked = false;
			IsFakeContentReasonChecked = false;
			IsOtherReasonChecked = false;
		}

		private string GetReasonMessage()
		{
			var reasonMessage = "";
			switch (_chosenPopupReason)
			{
				case PopupReason.InappropriateContentReason:
					reasonMessage = Strings["InappropriateContentReason"];
					break;
				case PopupReason.InappropriatePictureReason:
					reasonMessage = Strings["InappropriatePictureReason"];
					break;
				case PopupReason.FakeContentReason:
					reasonMessage = Strings["FakeContentReason"];
					break;
				case PopupReason.OtherReason:
					reasonMessage = OtherReasonContent;
					break;
			}
			return reasonMessage;
		}
	}
}

