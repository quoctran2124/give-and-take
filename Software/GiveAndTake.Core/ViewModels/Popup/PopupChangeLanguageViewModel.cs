﻿using GiveAndTake.Core.Models;
using GiveAndTake.Core.ViewModels.Base;
using MvvmCross.Commands;
using System.Windows.Input;
using I18NPortable;
using MvvmCross;

namespace GiveAndTake.Core.ViewModels.Popup
{
	public class PopupChangeLanguageViewModel : BaseViewModelResult<bool>
	{
		private IMvxCommand _closeCommand;
		private IMvxCommand _submitCommand;
		private IMvxCommand _isVietnameseClickCommand;
		private IMvxCommand _isEnglishClickCommand;
		private LanguageType _chosenLanguage;
		private bool _isSubmitBtnEnabled;
		private LanguageType _newLanguage;
		private bool _isVietnameseChecked;
		private bool _isEnglishChecked;

		public string PopupTitle => Strings["PopupChangeLanguageTitle"];
		public string VnTextTitle => AppConstants.VnTextTitle;
		public string EnTextTitle => AppConstants.EnTextTitle;
		public string BtnSubmitTitle => Strings["ConfirmChangeLanguage"];
		public string BtnCancelTitle => Strings["CancelTitle"];

		public bool IsSubmitBtnEnabled
		{
			get => _isSubmitBtnEnabled;
			set => SetProperty(ref _isSubmitBtnEnabled, value);
		}

		public bool IsVietnameseChecked
		{
			get => _isVietnameseChecked;
			set
			{
				SetProperty(ref _isVietnameseChecked, value);
				if (value)
				{
					_chosenLanguage = LanguageType.vi;
					_newLanguage = _chosenLanguage;
					UpdateSubmitBtn();
				}
			}
		}

		public bool IsEnglishChecked
		{
			get => _isEnglishChecked;
			set
			{
				SetProperty(ref _isEnglishChecked, value);
				if (value)
				{
					_chosenLanguage = LanguageType.en;
					_newLanguage = _chosenLanguage;
					UpdateSubmitBtn();
				}
			}
		}

		public PopupChangeLanguageViewModel(IDataModel dataModel)
		{
			if (dataModel.Language == LanguageType.vi)
			{
				_newLanguage = LanguageType.vi;
				IsVietnameseChecked = true;
			}
			else
			{
				_newLanguage = LanguageType.en;
				IsEnglishChecked = true;
			}
		}


		public IMvxCommand CloseCommand =>
			_closeCommand ?? (_closeCommand = new MvxAsyncCommand(() => NavigationService.Close(this, false)));
		public ICommand SubmitCommand => _submitCommand ?? (_submitCommand = new MvxCommand(InitChangeLanguage));
		public IMvxCommand IsVietnameseClickCommand => _isVietnameseClickCommand ?? (_isVietnameseClickCommand = new MvxCommand<LanguageType>(OnLanguageClicked));
		public IMvxCommand IsEnglishClickCommand => _isEnglishClickCommand ?? (_isEnglishClickCommand = new MvxCommand<LanguageType>(OnLanguageClicked));

		private void InitChangeLanguage()
		{
			_newLanguage = GetNewLanguage();
			ChangeLanguage(_newLanguage);
		}

		private LanguageType GetNewLanguage()
		{
			switch (_chosenLanguage)
			{
				case LanguageType.vi:
					_newLanguage = LanguageType.vi;
					break;
				case LanguageType.en:
					_newLanguage = LanguageType.en;
					break;
			}
			return _newLanguage;
		}

		private void ChangeLanguage(LanguageType language)
		{
			var appDataLoader = Mvx.Resolve<IAppDataLoader>();
			var appData = appDataLoader.AppData;
			appData.IsNotFirstStartApp = true;
			appDataLoader.SaveData(appData);

			I18N.Current.Locale = language.ToString();
			DataModel.Language = language;
			AppDataLoader.UpdateLanguage(language);
			DataModel.RaiseLanguageUpdated();

			NavigationService.Close(this,true);
		}

		private void OnLanguageClicked(LanguageType language)
		{
			IsVietnameseChecked = false;
			IsEnglishChecked = false;
			switch (language)
			{
				case LanguageType.vi:
					IsVietnameseChecked = true;
					break;
				case LanguageType.en:
					IsEnglishChecked = true;
					break;
			}

			_newLanguage = language == LanguageType.vi ? LanguageType.vi: LanguageType.en;
			
		}

		public void UpdateSubmitBtn() => IsSubmitBtnEnabled = !Equals(_newLanguage, DataModel.Language);


	}
}
