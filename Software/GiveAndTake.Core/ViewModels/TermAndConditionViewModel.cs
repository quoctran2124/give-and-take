﻿using GiveAndTake.Core.Helpers.Interface;
using GiveAndTake.Core.ViewModels.Base;
using MvvmCross.Commands;

namespace GiveAndTake.Core.ViewModels
{
	public class TermAndConditionViewModel : BaseViewModel
	{
		public string TermAndCondition => Strings["TermAndCondition"];
		public string TermAndConditionContentInHtml => DataModel.Language == LanguageType.vi ? AppConstants.TermAndConditionContentInHtmlVn : AppConstants.TermAndConditionContentInHtmlEn;

		public IMvxCommand BackPressedCommand => _backPressedCommand ?? (_backPressedCommand = new MvxCommand(BackPressed));
		public IMvxCommand OpenPolicyWebPageCommand => _openPolicyWebPageCommand ?? (_openPolicyWebPageCommand = new MvxCommand(OpenPolicyWebPage));

		private readonly IUrlHelper _urlHelper;
		private IMvxCommand _backPressedCommand;
		private IMvxCommand _openPolicyWebPageCommand;

		public TermAndConditionViewModel(IUrlHelper iUrlHelper)
		{
			_urlHelper = iUrlHelper;
		}

		private async void BackPressed()
		{
			await NavigationService.Close(this);
		}
		private void OpenPolicyWebPage()
		{
			_urlHelper.OpenUrl(AppConstants.PolicyWebPageLink);
		}
	}
}