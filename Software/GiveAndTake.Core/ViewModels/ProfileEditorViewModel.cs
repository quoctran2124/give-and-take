using FFImageLoading.Transformations;
using FFImageLoading.Work;
using GiveAndTake.Core.Exceptions;
using GiveAndTake.Core.Helpers;
using GiveAndTake.Core.Models;
using GiveAndTake.Core.Services;
using GiveAndTake.Core.ViewModels.Base;
using GiveAndTake.Core.ViewModels.Popup;
using MvvmCross.Commands;
using MvvmCross.Plugin.PictureChooser;
using Plugin.Permissions.Abstractions;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using I18NPortable;

namespace GiveAndTake.Core.ViewModels
{
	public class ProfileEditorViewModel : BaseViewModelResult<bool>
	{
		public string UserNameLabel => Strings["UserNameLabel"];

		public string BtnCancelTitle => Strings["CancelTitle"];

		public string BtnSaveTitle => Strings["SaveAPost"];

		public List<ITransformation> AvatarTransformations => new List<ITransformation> { new CircleTransformation() };

		public string AvatarUrl
		{
			get => _avatarUrl;
			set => SetProperty(ref _avatarUrl, value);
		}

		public string UserName
		{
			get => _userName;
			set => SetProperty(ref _userName, value);
		}

		public string Base64String
		{
			get => _base64String;
			set => SetProperty(ref _base64String, value);
		}

		private readonly AvatarData _avaData = new AvatarData();


		public IMvxCommand CameraButtonClickCommand =>
			_cameraButtonClickCommand ?? (_cameraButtonClickCommand = new MvxAsyncCommand(OnCameraButtonClicked));

		public IMvxCommand SaveCommand =>
			_saveClickCommand ?? (_saveClickCommand = new MvxAsyncCommand(OnSubmitted));

		public IMvxCommand ShowFullSizeAvatarImageCommand => _showFullSizeAvatarImageCommand ?? (_showFullSizeAvatarImageCommand = new MvxCommand(ShowFullSizeAvatarImage));

		public IMvxCommand BackPressedCommand => _backPressedCommand ?? (_backPressedCommand = new MvxAsyncCommand(OnBackPressed));
		public bool IsAvatarChanged => Base64String != null && Base64String != "(null)";

		private bool IsUserFullNameChanged => UserName != _dataModel.LoginResponse.Profile.DisplayName;

		private readonly IDataModel _dataModel;
		private readonly DebouncerHelper _debouncer;
		private readonly IMvxPictureChooserTask _pictureChooserTask;
		private readonly ILoadingOverlayService _overlayService;
		private string _avatarUrl;
		private string _userName;
		private string _base64String;
		private IMvxCommand _cameraButtonClickCommand;
		private IMvxCommand _saveClickCommand;
		private IMvxCommand _backPressedCommand;
		private IMvxCommand _showFullSizeAvatarImageCommand;
		private List<string> MenuOptions = new List<string>
		{
			Strings["TakePhoto"],
			Strings["SelectPhoto"]
		};

		public ProfileEditorViewModel(IDataModel dataModel, IMvxPictureChooserTask pictureChooserTask, ILoadingOverlayService overlayService)
		{
			_dataModel = dataModel;
			_pictureChooserTask = pictureChooserTask;
			_overlayService = overlayService;
			_debouncer = new DebouncerHelper();
		}

		public override Task Initialize()
		{
			UserName = _dataModel.LoginResponse.Profile.DisplayName;
			AvatarUrl = _dataModel.LoginResponse.Profile.AvatarUrl;
			return base.Initialize();
		}

		private async Task OnCameraButtonClicked()
		{
			var result =
				await NavigationService.Navigate<PopupExtensionOptionViewModel, List<string>, string>(MenuOptions);
			await Task.Delay(777);
			if (string.IsNullOrEmpty(result)) return;
			if (result == Strings["TakePhoto"])
			{
					PermissionHelper.GrandPermission(new List<Permission> { Permission.Camera, Permission.Storage }, HandleOnTakePhoto);
			}
			else if (result == Strings["SelectPhoto"])
			{
					PermissionHelper.GrandPermission(new List<Permission> { Permission.Storage }, HandleOnSelectPhoto);
			}
		}

		private void HandleOnTakePhoto()
		{
			_debouncer.Debouce(() =>
			{
				InvokeOnMainThread(() =>
				{
					_pictureChooserTask.TakePicture(AppConstants.ImageMaxSize, AppConstants.ImagePercentQuality, OnPicture, () => { });
				});
			});
		}

		private void HandleOnSelectPhoto()
		{
			_debouncer.Debouce(() =>
			{
				InvokeOnMainThread(() =>
				{
					_pictureChooserTask.ChoosePictureFromLibrary(AppConstants.ImageMaxSize, AppConstants.ImagePercentQuality, OnPicture, () => { });
				});
			});
		}

		private void OnPicture(Stream pictureStream)
		{
			var memoryStream = new MemoryStream();
			pictureStream.CopyTo(memoryStream);
			var bytes = memoryStream.ToArray();
			Base64String = JsonHelper.ConvertToBase64String(bytes);
		}
		private async Task OnSubmitted()
		{
			var isFinished = false;
			try
			{
				await _overlayService.ShowOverlay(Strings["LoadingDataOverlayTitle"]);
				_dataModel.LoginResponse.Profile = await ManagementService.UpdateCurrentUserProfile(new User
				{
					DisplayName = UserName,
					AvatarImageData = Base64String
				}, _dataModel.LoginResponse.Token);
				isFinished = true;				
			}
			catch (AppException.ApiException)
			{
				await NavigationService.Navigate<PopupWarningViewModel, string, bool>(Strings["ErrorConnectionMessage"]);
			}
			finally
			{
				await _overlayService.CloseOverlay();
				if (isFinished)
				{
					await NavigationService.Close(this, true);
				}
			}
		}

		private async Task OnBackPressed()
		{
			if (IsAvatarChanged || IsUserFullNameChanged)
			{
				var popupResult = await NavigationService.Navigate<PopupMessageViewModel, string, RequestStatus>(Strings["CancelEditProfileSettingConfirm"]);
				if (popupResult != RequestStatus.Submitted)
				{
					return;
				}
			}
			await NavigationService.Close(this, false);
		}

		private void ShowFullSizeAvatarImage()
		{
			//TODO later
			if (IsAvatarChanged)
			{
				_avaData.Base64String = Base64String;
				_avaData.ImageUrl = null;
			}
			else
			{
				_avaData.Base64String = null;
				_avaData.ImageUrl = AvatarUrl;
			}
			NavigationService.Navigate<AvatarImageViewModel, AvatarData>(_avaData);
		}
	}
}
