﻿using System;
using GiveAndTake.Core.Models;
using GiveAndTake.Core.ViewModels.Base;
using MvvmCross.Commands;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using GiveAndTake.Core.Exceptions;
using GiveAndTake.Core.Services;
using GiveAndTake.Core.ViewModels.Popup;
using MvvmCross;

namespace GiveAndTake.Core.ViewModels.TabNavigation
{
	public class TabNavigationViewModel : BaseViewModel
	{
		private readonly IDataModel _dataModel;
		public int NumberOfTab { get; set; }

		private const string ApplinksIdentify = "applinks";
		private const int PostIdLength = 36;
		private const int ConstAppLinkLength = 31;
		private IMvxAsyncCommand _showInitialViewModelsCommand;
		private ICommand _showErrorCommand;
		public IMvxAsyncCommand _showNotificationsCommand;
		private IMvxCommand _clearBadgeCommand;
		private int _notificationCount;
		private bool _applinkReceivedRegistered = false;
		public ICommand ShowErrorCommand => _showErrorCommand ?? (_showErrorCommand = new MvxCommand(InitErrorResponseAsync));
		public IMvxAsyncCommand ShowInitialViewModelsCommand =>
			_showInitialViewModelsCommand ??
			(_showInitialViewModelsCommand = new MvxAsyncCommand(ShowInitialViewModels));
		
		public IMvxAsyncCommand ShowNotificationsCommand => _showNotificationsCommand ?? new MvxAsyncCommand(ShowNotifications);
		public IMvxCommand ClearBadgeCommand => _clearBadgeCommand ?? (_clearBadgeCommand = new MvxCommand(OnBadgeCleared));

		

		public string AvatarUrl => _dataModel.LoginResponse.Profile.AvatarUrl;
		public TabNavigationViewModel(IDataModel dataModel)
		{
			_dataModel = dataModel;			
		}
		public override void ViewCreated()
		{
			base.ViewCreated();
			DataModel.NotificationReceived += OnNotificationReceived;
			DataModel.BadgeNotificationUpdated += OnBadgeReceived;
			if (!_applinkReceivedRegistered) DataModel.ApplinkReceived += OnApplinkReceived;
			_applinkReceivedRegistered = true;
			if (DataModel.ApplinkUrl != null && DataModel.IsHomeViewBeforeApplink)
			{
				DataModel.OnApplinkReceived();
			}
		}

		private async void OnApplinkReceived(object sender, EventArgs e)
		{
			if (_dataModel.ApplinkUrl.Length < ConstAppLinkLength)
			{
				return;
			}

			try
			{
				var _post = await ManagementService.GetPostDetail(
					_dataModel.ApplinkUrl.Substring(ConstAppLinkLength, PostIdLength), _dataModel.LoginResponse.Token);
				await NavigationService.Navigate<PostDetailViewModel, Post, NavigationResult>(_post);
			}
			catch (AppException.ApiException)
			{
				//Incase the user click on the link http://chovanhan.asia/, still go to our app but not go to our postdetail
				//If the Post has been deleted, it will diplay popup notify the user
				if (DataModel.ApplinkUrl?.IndexOf(ApplinksIdentify) >= 0)
				{
					await NavigationService.Navigate<PopupWarningViewModel, string, bool>(Strings["PostIsDeleted"]);
				}		
			}
			finally
			{
				DataModel.ApplinkUrl = null;
			}
		}

		private void OnBadgeReceived(object sender, int badge)
		{
			NotificationCount = badge;
		}

		public override void ViewDestroy(bool viewFinishing = true)
		{
			base.ViewDestroy(viewFinishing);
			DataModel.NotificationReceived -= OnNotificationReceived;
			DataModel.BadgeNotificationUpdated -= OnBadgeReceived;
			DataModel.ApplinkReceived -= OnApplinkReceived;
			_applinkReceivedRegistered = false;
			DataModel.ApplinkUrl = null;
		}

		
		public int NotificationCount
		{
			get => _notificationCount;
			set
			{
				//_notificationCount = value;
				//RaisePropertyChanged(() => NotificationCount);
				SetProperty(ref _notificationCount, value);
			}
		}

		public async void InitErrorResponseAsync()
		{
			var result = await NavigationService.Navigate<PopupWarningResponseViewModel, string, bool>(Strings["ErrorMessage"]);
			if (result)
			{
				System.Diagnostics.Process.GetCurrentProcess().CloseMainWindow();
			}
		}

		private void OnNotificationReceived(object sender, Notification notification)
		{
			//foreground (when app is alive)
			HandleNotificationClicked(notification);
		}


		private async Task ShowNotifications()
		{
			await NavigationService.Navigate<NotificationViewModel>();
		}

		private async Task ShowInitialViewModels()
		{
			var tasks = new List<Task>
			{
				NavigationService.Navigate<HomeViewModel>(),
				NavigationService.Navigate<NotificationViewModel>(),
				NavigationService.Navigate<ProfileViewModel>(),
			};

			NumberOfTab = tasks.Count;
			await Task.WhenAll(tasks);
			//the overlay will be closed here. Because if the overlay closes in LoginViewModel (I mean close early),..
			//.. the LoginView screen still remain appear without loading overlay..
			//..due to the main thread has to wait all of the tasks above to be reloaded.
			await Mvx.Resolve<ILoadingOverlayService>().CloseOverlay();
			if (DataModel.SelectedNotification != null)
			{
				//background (when app is destroyed)
				HandleNotificationClicked(DataModel.SelectedNotification);				
			}
		}
		private void OnBadgeCleared()
		{
			Task.Run(async () =>
			{
				try
				{
					await ManagementService.UpdateSeenNotificationStatus(true, DataModel.LoginResponse.Token);
				}
				catch (AppException.ApiException)
				{

				}
			});
			_dataModel.RaiseBadgeUpdated(0);
			_dataModel.Badge = 0;

		}
		private void HandleNotificationClicked(Notification notification)
		{
			// Handle both background and foreground when push notification is received
			// update Badge unread notification here
		}
	}
}
