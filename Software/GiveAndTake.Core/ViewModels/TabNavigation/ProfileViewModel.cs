﻿using FFImageLoading.Transformations;
using FFImageLoading.Work;
using GiveAndTake.Core.Exceptions;
using GiveAndTake.Core.Helpers;
using GiveAndTake.Core.Helpers.Interface;
using GiveAndTake.Core.Models;
using GiveAndTake.Core.Services;
using GiveAndTake.Core.ViewModels.Base;
using GiveAndTake.Core.ViewModels.Popup;
using I18NPortable;
using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GiveAndTake.Core.ViewModels.TabNavigation
{
	public class ProfileViewModel : BaseViewModel
	{
		public string RankTitle => Strings["RankTitle"];

		public string SentTitle => Strings["SentTitle"];

		public string LeftButtonTitle => Strings["MyPostsTitle"];

		public string RightButtonTitle => Strings["MyRequestsTitle"];

		public List<ITransformation> AvatarTransformations => new List<ITransformation> { new CircleTransformation() };

		public string AvatarUrl
		{
			get => _avatarUrl;
			set => SetProperty(ref _avatarUrl, value);
		}

		public string UserName
		{
			get => _userName;
			set => SetProperty(ref _userName, value);
		}

		public string RankType
		{
			get => _rankType;
			set => SetProperty(ref _rankType, value);
		}

		public string SentCount
		{
			get => _sentCount;
			set => SetProperty(ref _sentCount, value);
		}

		public bool IsPostsList
		{
			get => _isPostsList;
			set => SetProperty(ref _isPostsList, value);
		}

		public bool IsSearchResultNull
		{
			get => _isSearchResultNull;
			set => SetProperty(ref _isSearchResultNull, value);
		}

		public bool IsPostsRefreshing
		{
			get => _isPostsRefresh;
			set => SetProperty(ref _isPostsRefresh, value);
		}

		public bool IsRequestedPostsRefreshing
		{
			get => _isRequestedPostsRefresh;
			set => SetProperty(ref _isRequestedPostsRefresh, value);
		}

		public MvxObservableCollection<PostItemViewModel> PostViewModels
		{
			get => _postViewModels;
			set
			{
				SetProperty(ref _postViewModels, value);
				if (_dataModel.LoginResponse.Profile.AppreciationNumber == 1)
				{
					SentCount = "1 " + Strings["Time"];
				}
				else
				{
					SentCount = $"{_dataModel.LoginResponse.Profile.AppreciationNumber} " + Strings["Times"];
				}
			}
		}

		public MvxObservableCollection<PostItemViewModel> RequestedPostViewModels
		{
			get => _requestedPostViewModels;
			set => SetProperty(ref _requestedPostViewModels, value);
		}
		public IMvxCommand ShowFullSizeAvatarImageCommand =>
			_showFullSizeAvatarImageCommand ?? (_showFullSizeAvatarImageCommand = new MvxCommand(ShowFullSizeAvatarImage));

		public IMvxCommand ShowMyPostsCommand =>
			_showMyPostsCommand ?? (_showMyPostsCommand = new MvxCommand(ShowMyPosts));

		public IMvxCommand ShowMyRequestsCommand =>
			_showMyRequestsCommand ?? (_showMyRequestsCommand = new MvxAsyncCommand(ShowMyRequests));

		public IMvxCommand LoadMorePostsCommand =>
			_loadMorePostsCommand ?? (_loadMorePostsCommand = new MvxAsyncCommand(OnLoadMorePosts));

		public IMvxCommand LoadMoreRequestedPostsCommand =>
			_loadMoreRequestedPostsCommand ?? (_loadMoreRequestedPostsCommand = new MvxAsyncCommand(OnLoadMoreRequestedPosts));

		public IMvxCommand RefreshPostsCommand =>
			_refreshPostsCommand ?? (_refreshPostsCommand = new MvxAsyncCommand(OnRefreshPosts));

		public IMvxCommand RefreshRequestedPostsCommand =>
			_refreshRequestedPostsCommand ?? (_refreshRequestedPostsCommand = new MvxAsyncCommand(OnRefreshRequestedPosts));

		public IMvxCommand ShowMenuPopupCommand =>
			_showMenuPopupCommand ?? (_showMenuPopupCommand = new MvxAsyncCommand(ShowMenuSettingView));

		public IMvxInteraction LogoutFacebook =>
			_logoutFacebook ?? (_logoutFacebook = new MvxInteraction());

		public IMvxCommand CreatePostCommand =>
			_createPostCommand ?? (_createPostCommand = new MvxAsyncCommand(ShowNewPostView));

		private string _avatarUrl;
		private string _userName;
		private string _rankType;
		private string _sentCount;
		private bool _isPostsList;
		private bool _isPostsRefresh;
		private bool _isRequestedPostsRefresh;
		private bool _isSearchResultNull;
		private readonly IDataModel _dataModel;
		private readonly IEmailHelper _emailHelper;
		private readonly ILoadingOverlayService _overlayService;
		private IMvxCommand _showMyPostsCommand;
		private IMvxCommand _showMyRequestsCommand;
		private IMvxCommand _loadMorePostsCommand;
		private IMvxCommand _loadMoreRequestedPostsCommand;
		private IMvxCommand _refreshPostsCommand;
		private IMvxCommand _refreshRequestedPostsCommand;
		private IMvxCommand _showMenuPopupCommand;
		private MvxInteraction _logoutFacebook;
		private IMvxCommand _createPostCommand;
		private IMvxCommand _showFullSizeAvatarImageCommand;
		private MvxObservableCollection<PostItemViewModel> _postViewModels;
		private MvxObservableCollection<PostItemViewModel> _requestedPostViewModels;
		private List<Post> _myPosts;
		private List<Post> _requestedPosts;
		private ProfileReloadType _reloadType;

		private List<string> MenuSettingOptions = new List<string>
		{
			Strings["EditProfile"],
			Strings["ChangeLanguage"],
			Strings["SendFeedback"],
			Strings["About"],
			Strings["LogOut"]
		};

		public ProfileViewModel(IDataModel dataModel, ILoadingOverlayService overlayService, IEmailHelper emailHelper)
		{
			_dataModel = dataModel;
			_overlayService = overlayService;
			_emailHelper = emailHelper;
			AvatarUrl = _dataModel.LoginResponse.Profile.AvatarUrl;
			UserName = _dataModel.LoginResponse.Profile.DisplayName?.ToUpper();
			RankType = _dataModel.LoginResponse.Profile.MemberType.Translate();
			IsPostsList = true;
		}

		public override async Task Initialize()
		{
			await base.Initialize();
			await UpdateMyPostViewModels();
		}

		private void ShowMyPosts()
		{
			if (IsPostsList)
			{
				return;
			}

			IsPostsList = true;
		}

		private async Task ShowMyRequests()
		{
			if (!IsPostsList)
			{
				return;
			}

			IsPostsList = false;

			if (RequestedPostViewModels == null)
			{
				await _overlayService.ShowOverlay(Strings["LoadingDataOverlayTitle"]);
				await UpdateMyRequestedPostViewModels();
				await _overlayService.CloseOverlay();
			}
		}

		private async Task OnRefreshPosts()
		{
			IsPostsRefreshing = true;
			await UpdateMyPostViewModels();
			IsPostsRefreshing = false;
		}

		private async Task OnRefreshRequestedPosts()
		{
			IsRequestedPostsRefreshing = true;
			await UpdateMyRequestedPostViewModels();
			IsRequestedPostsRefreshing = false;
		}

		private async Task OnLoadMorePosts()
		{
			try
			{
				_dataModel.ApiMyPostsResponse = await ManagementService.GetMyPostList(_dataModel.LoginResponse.Profile.Id, $"page={_dataModel.ApiMyPostsResponse.Pagination.Page + 1}", _dataModel.LoginResponse.Token);

				if (_dataModel.ApiMyPostsResponse.Posts.Any())
				{
					PostViewModels.Last().IsSeparatorLineShown = true;
					PostViewModels.AddRange(GeneratePostItemViewModelCollection(_dataModel.ApiMyPostsResponse.Posts, _myPosts));
					PostViewModels.Last().IsSeparatorLineShown = false;
				}
			}
			catch (AggregateException)
			{
				var result = await NavigationService.Navigate<PopupMessageViewModel, string, RequestStatus>(Strings["ErrorConnectionMessage"]);
				if (result == RequestStatus.Submitted)
				{
					await OnLoadMorePosts();
				}
			}
		}

		private async Task OnLoadMoreRequestedPosts()
		{
			try
			{
				_dataModel.ApiMyRequestedPostResponse = await ManagementService.GetMyRequestedPosts($"page={_dataModel.ApiMyRequestedPostResponse.Pagination.Page + 1}", _dataModel.LoginResponse.Token);

				if (_dataModel.ApiMyRequestedPostResponse.Posts.Any())
				{
					RequestedPostViewModels.Last().IsSeparatorLineShown = true;
					RequestedPostViewModels.AddRange(GeneratePostItemViewModelCollection(_dataModel.ApiMyRequestedPostResponse.Posts, _requestedPosts));
					RequestedPostViewModels.Last().IsSeparatorLineShown = false;
				}
			}
			catch (AggregateException)
			{
				var result = await NavigationService.Navigate<PopupMessageViewModel, string, RequestStatus>(Strings["ErrorConnectionMessage"]);
				if (result == RequestStatus.Submitted)
				{
					await OnLoadMoreRequestedPosts();
				}
			}
		}

		private async Task UpdateMyPostViewModels()
		{
			try
			{
				_dataModel.ApiMyPostsResponse = await ManagementService.GetMyPostList(_dataModel.LoginResponse.Profile.Id, null, _dataModel.LoginResponse.Token);
				_myPosts = _dataModel.ApiMyPostsResponse.Posts;
				PostViewModels = new MvxObservableCollection<PostItemViewModel>();
				if (_dataModel.ApiMyPostsResponse.Posts.Any())
				{
					PostViewModels.AddRange(_dataModel.ApiMyPostsResponse.Posts.Select(GeneratePostViewModels));
					PostViewModels.Last().IsSeparatorLineShown = false;
					_reloadType = ProfileReloadType.MyPost;
				}

				IsSearchResultNull = PostViewModels.Any();
			}
			catch (AppException.ApiException)
			{
				var result = await NavigationService.Navigate<PopupMessageViewModel, string, RequestStatus>(Strings["ErrorConnectionMessage"]);
				if (result == RequestStatus.Submitted)
				{
					await UpdateMyPostViewModels();
				}
			}
		}

		private async Task UpdateMyRequestedPostViewModels()
		{
			try
			{
				_dataModel.ApiMyRequestedPostResponse = await ManagementService.GetMyRequestedPosts(null, _dataModel.LoginResponse.Token);
				_requestedPosts = _dataModel.ApiMyRequestedPostResponse.Posts;
				RequestedPostViewModels = new MvxObservableCollection<PostItemViewModel>();
				if (_dataModel.ApiMyRequestedPostResponse.Posts.Any())
				{
					RequestedPostViewModels.AddRange(_dataModel.ApiMyRequestedPostResponse.Posts.Select(GeneratePostViewModels));
					RequestedPostViewModels.Last().IsSeparatorLineShown = false;
					_reloadType = ProfileReloadType.MyRequestPost;
				}
				IsSearchResultNull = RequestedPostViewModels.Any();
			}
			catch (AppException.ApiException)
			{
				var result = await NavigationService.Navigate<PopupMessageViewModel, string, RequestStatus>(Strings["ErrorConnectionMessage"]);
				if (result == RequestStatus.Submitted)
				{
					await UpdateMyRequestedPostViewModels();
				}
			}
		}

		private async void ReloadData()
		{
			switch (_reloadType)
			{
				case ProfileReloadType.MyPost:
					await UpdateMyPostViewModels();
					break;
				case ProfileReloadType.MyRequestPost:
					await UpdateMyRequestedPostViewModels();
					break;
			}
		}

		private async Task ShowNewPostView()
		{
			if (_dataModel.Categories != null)
			{
				try
				{
					var result = await NavigationService.Navigate<CreatePostViewModel, bool>();
					if (result)
					{
						await UpdateProfileViewModel();
					}
				}
				catch (AppException.ApiException)
				{
					await NavigationService.Navigate<PopupWarningViewModel, string, bool>(Strings["ErrorConnectionMessage"]);
				}
			}
		}

		private async Task UpdateProfileViewModel()
		{
			try
			{
				await _overlayService.ShowOverlay(Strings["LoadingDataOverlayTitle"]);
				if (IsPostsList)
				{
					await UpdateMyPostViewModels();
				}
				else
				{
					await UpdateMyRequestedPostViewModels();
				}
			}
			catch (AppException.ApiException)
			{
				await NavigationService.Navigate<PopupWarningViewModel, string, bool>(Strings["ErrorConnectionMessage"]);
			}
			finally
			{
				await _overlayService.CloseOverlay();
			}
		}

		private MvxObservableCollection<PostItemViewModel> GeneratePostItemViewModelCollection(List<Post> newPosts, List<Post> currentPosts)
		{
			var newPostViewModels = new MvxObservableCollection<PostItemViewModel>();
			foreach (var post in newPosts)
			{
				if (currentPosts.All(p => !p.PostId.Equals(post.PostId)))
				{
					newPostViewModels.Add(GeneratePostViewModels(post));
					currentPosts.Add(post);
				}
			}
			return newPostViewModels;
		}

		private PostItemViewModel GeneratePostViewModels(Post post)
		{
			post.IsMyPost = IsPostsList;
			return new PostItemViewModel(_dataModel, post, ReloadData);
		}

		private async Task ShowMenuSettingView()
		{
			var result = await NavigationService.Navigate<PopupExtensionOptionViewModel, List<string>, string>(MenuSettingOptions);

			if (string.IsNullOrEmpty(result)) return;

			if (result == Strings["EditProfile"])
			{
				await NavigationService.Navigate<ProfileEditorViewModel, bool>();
				AvatarUrl = _dataModel.LoginResponse.Profile.AvatarUrl;
				UserName = _dataModel.LoginResponse.Profile.DisplayName?.ToUpper();
			}
			else if (result == Strings["SendFeedback"])
			{
				_emailHelper.OpenFeedbackEmail();
			}
			else if (result == Strings["About"])
			{
				await NavigationService.Navigate<AboutViewModel>();
			}
			else if (result == Strings["LogOut"])
			{
				_logoutFacebook.Raise();
				_dataModel.ApiPostsResponse = null;
				await Task.WhenAll(
					ManagementService.Logout(_dataModel.LoginResponse.Token),
					ManagementService.DeletePushNotificationToken(Mvx.Resolve<IDeviceInfo>().DeviceToken, Mvx.Resolve<IDeviceInfo>().MobilePlatform,
						_dataModel.LoginResponse.Token),
					NavigationService.Navigate<LoginViewModel>());
				_dataModel.LoginResponse = null;
			}
			else if (result == Strings["ChangeLanguage"])
			{
				var resultChangeLanguage = await NavigationService.Navigate<PopupChangeLanguageViewModel, bool>();
				if (resultChangeLanguage)
				{
					await Task.Run(async () =>
					{
						await ManagementService.SendPushNotificationUserInformation(new PushNotificationUserInformation()
						{
							DeviceToken = Mvx.Resolve<IDeviceInfo>().DeviceToken,
							MobilePlatform = Mvx.Resolve<IDeviceInfo>().MobilePlatform,
							Language = DataModel.Language.ToString()
						}, _dataModel.LoginResponse.Token);
					});
					await NavigationService.Navigate<MasterViewModel>();
				}
			}
		}

		private void ShowFullSizeAvatarImage()
		{
			var avaData = new AvatarData()
			{
				ImageUrl = AvatarUrl,
				Base64String = null
			};
			NavigationService.Navigate<AvatarImageViewModel, AvatarData>(avaData);
		}
	}
}
