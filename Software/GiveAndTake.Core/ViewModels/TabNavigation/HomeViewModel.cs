using System;
using GiveAndTake.Core.Exceptions;
using GiveAndTake.Core.Models;
using GiveAndTake.Core.ViewModels.Base;
using GiveAndTake.Core.ViewModels.Popup;
using MvvmCross.Commands;
using MvvmCross.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GiveAndTake.Core.Services;
using I18NPortable;

namespace GiveAndTake.Core.ViewModels.TabNavigation
{
	public class HomeViewModel : BaseViewModel
	{
		#region Properties

		public string SearchResultTitle => Strings["SearchResultNullTitle"];

		public IMvxCommand ShowLocationFiltersCommand =>
			_showLocationFiltersCommand ??
			(_showLocationFiltersCommand = new MvxAsyncCommand(ShowLocationFiltersPopup));

		public IMvxCommand ShowSortFiltersCommand =>
			_showSortFiltersCommand ?? (_showSortFiltersCommand = new MvxAsyncCommand(ShowSortFiltersPopup));

		public IMvxCommand ShowCategoriesCommand =>
			_showCategoriesCommand ?? (_showCategoriesCommand = new MvxAsyncCommand(ShowCategoriesPopup));

		public IMvxCommand CreatePostCommand =>
			_createPostCommand ?? (_createPostCommand = new MvxAsyncCommand(ShowNewPostView));

		public IMvxCommand SearchCommand =>
			_searchCommand ?? (_searchCommand = new MvxAsyncCommand(OnSearching));

		public IMvxCommand CloseSearchBarCommand =>
			_searchCommand ?? (_searchCommand = new MvxAsyncCommand(InitDataModels));


		public IMvxCommand LoadMoreCommand =>
			_loadMoreCommand ?? (_loadMoreCommand = new MvxAsyncCommand(OnLoadMore));

		public IMvxCommand RefreshCommand =>
			_refreshCommand ?? (_refreshCommand = new MvxAsyncCommand(OnRefresh));
		public IMvxCommand BackPressedCommand =>
			_backPressedCommand ?? (_backPressedCommand = new MvxAsyncCommand(OnBackPressedCommand));

		public IMvxInteraction ShowProfileTab => 
			_showProfileTab ?? (_showProfileTab = new MvxInteraction());
		public bool IsSearched
		{
			get => _isSearched;
			set => SetProperty(ref _isSearched, value);
		}

		public bool IsRefreshing
		{
			get => _isRefresh;
			set => SetProperty(ref _isRefresh, value);
		}

		public bool IsClearButtonShown
		{
			get => _isClearButtonShown;
			set
			{
				_isClearButtonShown = value;
				RaisePropertyChanged();
			} 
		}

		public bool IsCategoryFilterActivated
		{
			get => _isCategoryFilterActivated;
			set => SetProperty(ref _isCategoryFilterActivated, value);
		}

		public bool IsSortFilterActivated
		{
			get => _isSortFilterActivated;
			set => SetProperty(ref _isSortFilterActivated, value);
		}

		public bool IsLocationFilterActivated
		{
			get => _isLocationFilterActivated;
			set => SetProperty(ref _isLocationFilterActivated, value);
		}

		public bool IsSearchResultNull
		{
			get => _isSearchResultNull;
			set => SetProperty(ref _isSearchResultNull, value);
		}

		public string CurrentQueryString
		{
			get => _currentQueryString;
			set => SetProperty(ref _currentQueryString, value);
		}

		public MvxObservableCollection<PostItemViewModel> PostItemViewModelCollection
		{
			get => _postItemViewModelCollection;
			set => SetProperty(ref _postItemViewModelCollection, value);
		}

		private bool _isSearchResultNull;
		private bool _isLocationFilterActivated;
		private bool _isSortFilterActivated;
		private bool _isCategoryFilterActivated;
		private bool _isRefresh;
		private bool _isClearButtonShown;
		private string _currentQueryString;
		private readonly IDataModel _dataModel;
		private MvxObservableCollection<PostItemViewModel> _postItemViewModelCollection;
		private Category _selectedCategory;
		private ProvinceCity _selectedProvinceCity;
		private SortFilter _selectedSortFilter;
		private IMvxCommand _showLocationFiltersCommand;
		private IMvxCommand _showSortFiltersCommand;
		private IMvxCommand _showCategoriesCommand;
		private IMvxCommand _createPostCommand;
		private IMvxCommand _searchCommand;
		private IMvxCommand _loadMoreCommand;
		private IMvxCommand _refreshCommand;
		private IMvxCommand _backPressedCommand;
		private readonly ILoadingOverlayService _overlay;
		private bool _isSearched;
		private MvxInteraction _showProfileTab;
		private List<Post> _posts;

		#endregion

		public HomeViewModel(IDataModel dataModel, ILoadingOverlayService loadingOverlayService)
		{
			_dataModel = dataModel;
			_overlay = loadingOverlayService;
		}

		public override Task Initialize() => InitDataModels();

		private async Task InitDataModels()
		{
			if (_postItemViewModelCollection == null )
			{
				try
				{					
					await _overlay.ShowOverlay(Strings["LoadingDataOverlayTitle"]);
					await UpdatePostViewModelCollection();
					if (!DataModel.IsHomeViewBeforeApplink && DataModel.ApplinkUrl != null)
					{
						
						DataModel.OnApplinkReceived();
					}					
					DataModel.IsHomeViewBeforeApplink = true;
				}
				catch (AppException.ApiException)
				{
					await NavigationService.Navigate<PopupWarningViewModel, string,bool>(Strings["ErrorConnectionMessage"]);
					await InitDataModels();
				}
				finally
				{
					await _overlay.CloseOverlay();					
				}
				Task.Run(async () =>
				{
					var badge = await ManagementService.GetBadgeFromServer(DataModel.LoginResponse.Token);
					if (badge != 0)
					{
						DataModel.RaiseBadgeUpdated(badge);
					}
				});
			}						
		}

		private async Task UpdateAllPopupListDataModel()
		{
			try
			{
				_dataModel.Categories = _dataModel.Categories ?? (await ManagementService.GetCategories()).Categories;
				_dataModel.ProvinceCities = _dataModel.ProvinceCities ?? (await ManagementService.GetProvinceCities()).ProvinceCities;
				_dataModel.SortFilters = ManagementService.GetShortFilters();
				_selectedCategory = _selectedCategory ?? _dataModel.Categories.First();
				_selectedProvinceCity = _selectedProvinceCity ?? _dataModel.ProvinceCities.First(p => p.ProvinceCityName.Translate() == Strings["DefaultLocationFilter"]);
				_selectedSortFilter = _selectedSortFilter ?? _dataModel.SortFilters.First();
			}
			catch (AppException.ApiException)
			{
				await NavigationService.Navigate<PopupWarningViewModel, string, bool>(Strings["ErrorConnectionMessage"]);
			}
		}

		private async Task UpdatePostViewModelCollection()
		{
			await UpdateAllPopupListDataModel();
			_dataModel.ApiPostsResponse = await ManagementService.GetPostList(GetFilterParams(), _dataModel.LoginResponse.Token);
			_posts = _dataModel.ApiPostsResponse.Posts;
			PostItemViewModelCollection = new MvxObservableCollection<PostItemViewModel>(_dataModel.ApiPostsResponse.Posts.Select(GeneratePostViewModels));
			if (PostItemViewModelCollection.Any())
			{
				PostItemViewModelCollection.Last().IsSeparatorLineShown = false;
			}
			IsSearchResultNull = PostItemViewModelCollection.Any();
		}

		private async Task OnLoadMore()
		{
			try
			{
				_dataModel.ApiPostsResponse = await ManagementService.GetPostList($"{GetFilterParams()}&page={_dataModel.ApiPostsResponse.Pagination.Page + 1}", _dataModel.LoginResponse.Token);
				if (_dataModel.ApiPostsResponse.Posts.Any())
				{
					PostItemViewModelCollection.Last().IsSeparatorLineShown = true;
					PostItemViewModelCollection.AddRange(GeneratePostItemViewModelCollection(_dataModel.ApiPostsResponse.Posts, _posts));
					PostItemViewModelCollection.Last().IsSeparatorLineShown = false;
				}
			}
			catch (AppException.ApiException)
			{
				await NavigationService.Navigate<PopupWarningViewModel, string, bool>(Strings["ErrorConnectionMessage"]);
			}					
		}

		private async Task OnRefresh()
		{
			try
			{
				IsRefreshing = true;
				if (_dataModel.Categories == null || _dataModel.SortFilters == null ||
				    _dataModel.ProvinceCities == null)
				{
					await InitDataModels();
				}
				else
				{					
					await UpdatePostViewModelCollection();
				}									
			}
			catch (AppException.ApiException)
			{
				await NavigationService.Navigate<PopupWarningViewModel, string, bool>(Strings["ErrorConnectionMessage"]);
			}
			finally
			{
				IsRefreshing = false;
			}

		}

		private async Task OnBackPressedCommand()
		{
			if (IsSearched)
			{
				try
				{
					await _overlay.ShowOverlay(Strings["LoadingDataOverlayTitle"]);
					CurrentQueryString = null;
					await UpdatePostViewModelCollection();
					IsSearched = false;
					IsClearButtonShown = false;
				}
				catch (AppException.ApiException)
				{
					await NavigationService.Navigate<PopupWarningViewModel, string, bool>(Strings["ErrorConnectionMessage"]);
				}
				finally
				{
					await _overlay.CloseOverlay();
				}
			}
		}

		private MvxObservableCollection<PostItemViewModel> GeneratePostItemViewModelCollection(List<Post> newPosts, List<Post> currentPosts)
		{
			var newPostViewModels = new MvxObservableCollection<PostItemViewModel>();
			foreach (var post in newPosts)
			{
				if (currentPosts.All(p => !p.PostId.Equals(post.PostId)))
				{
					newPostViewModels.Add(GeneratePostViewModels(post));
					currentPosts.Add(post);
				}
			}
			return newPostViewModels;
		}

		private PostItemViewModel GeneratePostViewModels(Post post)
		{
			post.IsMyPost = post.User.Id == _dataModel.LoginResponse.Profile.Id;
			return new PostItemViewModel(_dataModel, post, ReloadData)
			{
				ShowProfileTab = () => { _showProfileTab.Raise();},
				IsStatusShown = false
			};
		}

		private async void ReloadData()
		{
			await UpdatePostViewModelWithOverlay();
		}

		private async Task ShowCategoriesPopup()
		{
			if (_dataModel.Categories != null)
			{
				var result = await NavigationService.Navigate<PopupListViewModel, PopupListParam, string>(new PopupListParam
				{
					Title = Strings["PopupCategoriesTitle"],
					Items = _dataModel.Categories.Select(c => c.CategoryName.Translate()).ToList(),
					SelectedItem = _selectedCategory.CategoryName.Translate()
				});

				if (string.IsNullOrEmpty(result)) return;

				_selectedCategory = _dataModel.Categories.First(c => c.CategoryName.Translate() == result);
				IsCategoryFilterActivated = _selectedCategory != _dataModel.Categories.First();
				await UpdatePostViewModelWithOverlay();
			}
		}

		private async Task ShowSortFiltersPopup()
		{
			if (_dataModel.SortFilters != null)
			{
				var result = await NavigationService.Navigate<PopupListViewModel, PopupListParam, string>(new PopupListParam
				{
					Title = Strings["PopupSortFiltersTitle"],
					Items = _dataModel.SortFilters.Select(s => s.FilterName).ToList(),
					SelectedItem = _selectedSortFilter.FilterName
				});
				if (string.IsNullOrEmpty(result)) return;
				_selectedSortFilter = _dataModel.SortFilters.First(s => s.FilterName == result);
				IsSortFilterActivated = _selectedSortFilter.FilterTag != _dataModel.SortFilters.First().FilterTag;
				await UpdatePostViewModelWithOverlay();
			}			
		}

		private async Task ShowLocationFiltersPopup()
		{
			if (_dataModel.SortFilters != null || _dataModel.ProvinceCities != null)
			{
				var result = await NavigationService.Navigate<PopupListViewModel, PopupListParam, string>(new PopupListParam
				{
					Title = Strings["PopupLocationFiltersTitle"],
					Items = _dataModel.ProvinceCities.Select(c => c.ProvinceCityName.Translate()).ToList(),
					SelectedItem = _selectedProvinceCity.ProvinceCityName.Translate()
				});

				if (string.IsNullOrEmpty(result)) return;

				_selectedProvinceCity = _dataModel.ProvinceCities.First(c => c.ProvinceCityName.Translate() == result);
				IsLocationFilterActivated = _selectedProvinceCity.ProvinceCityName.Translate() != Strings["DefaultLocationFilter"];
				await UpdatePostViewModelWithOverlay();
			}			
		}

		private async Task ShowNewPostView()
		{
			if (_dataModel.Categories != null)
			{
				try
				{
					var result = await NavigationService.Navigate<CreatePostViewModel, bool>();
					if (result)
					{
						await UpdatePostViewModelCollection();
					}
				}
				catch (AppException.ApiException)
				{
					await NavigationService.Navigate<PopupWarningViewModel, string, bool>(Strings["ErrorConnectionMessage"]);
				}
			}			
		}

		private async Task OnSearching()
		{
			await UpdatePostViewModelWithOverlay();
			IsSearched = true;
		}

		private async Task UpdatePostViewModelWithOverlay()
		{
			try
			{
				await _overlay.ShowOverlay(Strings["LoadingDataOverlayTitle"]);
				await UpdatePostViewModelCollection();
			}
			catch (AppException.ApiException)
			{
				await NavigationService.Navigate<PopupWarningViewModel, string, bool>(Strings["ErrorConnectionMessage"]);
			}
			finally
			{
				await _overlay.CloseOverlay();
			}
		}

		public override void ViewDestroy(bool viewFinishing = true)
		{
			base.ViewDestroy(viewFinishing);
			_dataModel.ApiPostsResponse = null;
		}

		public async override void ViewAppeared()
		{
			base.ViewAppeared();
		}

		private string GetFilterParams()
		{
			var parameters = new List<string>();
			if (!string.IsNullOrEmpty(CurrentQueryString))
			{
				parameters.Add($"keyword={CurrentQueryString}");
			}
			if (_selectedSortFilter != null)
			{
				parameters.Add($"order={_selectedSortFilter.FilterTag}");
			}
			if (_dataModel.Categories != null)
			{
				if (_selectedCategory?.Id != _dataModel.Categories.First().Id)
				{
					parameters.Add($"categoryId={_selectedCategory?.Id}");
				}
			}
			if (_selectedProvinceCity != null)
			{
				parameters.Add($"provinceCityId={_selectedProvinceCity.Id}");
			}						
			return string.Join("&", parameters);
		}
	}
}