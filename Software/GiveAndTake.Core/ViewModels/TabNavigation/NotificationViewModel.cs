﻿using GiveAndTake.Core.Exceptions;
using GiveAndTake.Core.Models;
using GiveAndTake.Core.Services;
using GiveAndTake.Core.ViewModels.Base;
using GiveAndTake.Core.ViewModels.Popup;
using GiveAndTake.Core.ViewModels.Popup.MyRequest;
using GiveAndTake.Core.ViewModels.Popup.OtherRequest;
using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.ViewModels;
using System.Linq;
using System.Threading.Tasks;


namespace GiveAndTake.Core.ViewModels.TabNavigation
{
	public class NotificationViewModel : BaseViewModel
	{
		public bool IsRefreshing
		{
			get => _isRefresh;
			set => SetProperty(ref _isRefresh, value);
		}

		public MvxObservableCollection<NotificationItemViewModel> NotificationItemViewModels
		{
			get => _notificationItemViewModel;
			set => SetProperty(ref _notificationItemViewModel, value);
		}



		private readonly IDataModel _dataModel;
		private readonly string _token;
		private readonly ILoadingOverlayService _overlay;
		private MvxObservableCollection<NotificationItemViewModel> _notificationItemViewModel;
		private bool _isRefresh;
		//Khang creates this
		private int _notiCount;
		private IMvxCommand _refreshCommand;
		private IMvxCommand _loadMoreCommand;
		public IMvxCommand RefreshCommand => _refreshCommand = _refreshCommand ?? new MvxCommand(OnRefresh);
		public IMvxCommand LoadMoreCommand => _loadMoreCommand = _loadMoreCommand ?? new MvxAsyncCommand(OnLoadMore);

		public NotificationViewModel(IDataModel dataModel, ILoadingOverlayService loadingOverlayService)
		{
			_dataModel = dataModel;
			_token = _dataModel.LoginResponse.Token;
			_overlay = loadingOverlayService;
		}

		public override async Task Initialize()
		{
			await base.Initialize();
			await UpdateNotificationViewModels();
			//for iphone when app is destroyed
			if (DataModel.SelectedNotification != null)
			{
				OnItemClicked(DataModel.SelectedNotification);
				DataModel.SelectedNotification = null;
			}
		}

		private void OnBadgeReceived(object sender, int badge)
		{
			if (badge != 0)
			{
				//Review ThanhVo Check all place use Task.Run in this view model. Many place use it. why use Task.Run?
				//Answer: The list of notification will be updated in background if there is new coming value of badge. 
				Task.Run(() => { UpdateNotificationViewModels(); });
			}
		}
		
		public override void ViewCreated()
		{
			base.ViewCreated();
			_dataModel.NotificationReceived += OnNotificationReceived;
			_dataModel.BadgeNotificationUpdated += OnBadgeReceived;
			Task.Run(async () =>
			{
				try
				{
					await ManagementService.UpdateSeenNotificationStatus(true, DataModel.LoginResponse.Token);
				}
				catch (AppException.ApiException)
				{
					//Review ThanhVo What should do if it has exception
					//Answer: This is done in background to let the server know that all notification are seen. None of popup will be shown to the user.
					//So should we just log the exception?
				}				
			});			
		}

		public override void ViewDisappearing()
		{
			base.ViewDisappearing();
			DataModel.SelectedNotification = null;
		}

		public override void ViewDestroy(bool viewFinishing = true)
		{
			base.ViewDestroy(viewFinishing);
			_dataModel.BadgeNotificationUpdated -= OnBadgeReceived;
			_dataModel.NotificationReceived -= OnNotificationReceived;
		}

		public void OnNotificationReceived(object sender, Notification notification)
		{
			if (DataModel.SelectedNotification != null)
			{				
				OnItemClicked(notification);
				Task.Run(async () => { await UpdateNotificationViewModels(); });
				Task.Run(async () =>
				{
					try
					{
						await ManagementService.UpdateSeenNotificationStatus(true, DataModel.LoginResponse.Token);
					}
					catch (AppException.ApiException)
					{

					}
				});
			}			
		}

		public async Task UpdateNotificationViewModels()
		{
			try
			{
				//Review ThanhVo use constant from AppConstant for 20 value
				_dataModel.ApiNotificationResponse = await ManagementService.GetNotificationList($"limit=20&language={DataModel.Language}", _token);
				_notiCount = _dataModel.ApiNotificationResponse.NumberOfNotiNotSeen;
				NotificationItemViewModels = new MvxObservableCollection<NotificationItemViewModel>(_dataModel.ApiNotificationResponse.Notifications.Select(GenerateNotificationItem));
			}
			catch (AppException.ApiException)
			{
				await NavigationService.Navigate<PopupWarningViewModel, string, bool>(Strings["ErrorConnectionMessage"]);
			}
		}

		public async Task UpdateNotificationViewModelOverLay()
		{
			await _overlay.ShowOverlay(Strings["LoadingDataOverlayTitle"]);
			await UpdateNotificationViewModels();
			await _overlay.CloseOverlay();
		}

		private async Task OnLoadMore()
		{
			_dataModel.ApiNotificationResponse = await ManagementService.GetNotificationList($"limit={AppConstants.NumberOfRequestPerPage}&page={_dataModel.ApiNotificationResponse.Pagination.Page + 1}&language={_dataModel.Language}", _token);
			if (_dataModel.ApiNotificationResponse.Notifications.Any())
			{
				NotificationItemViewModels.AddRange(
					_dataModel.ApiNotificationResponse.Notifications.Select(GenerateNotificationItem));
			}
		}

		private NotificationItemViewModel GenerateNotificationItem(Notification notification)
		{
			var notificationItem = new NotificationItemViewModel(notification)
			{
				ClickAction = OnItemClicked,
			};
			return notificationItem;
		}

		private async void OnItemClicked(Notification notification)
		{
			try
			{
				switch (notification.Type)
				{
					case "Like":
						await HandleLikeAndIsRejectedType(notification);
						break;

					case "Comment":
						await NavigationService.Navigate<PopupMessageViewModel, string>(Strings["DefaultWarningMessage"]);
						break;

					case "Request":
						await HandleRequestType(notification);
						break;

					case "BlockedPost":
						await NavigationService.Navigate<PopupMessageViewModel, string>(Strings["DefaultWarningMessage"]);
						break;

					case "Warning":
						await NavigationService.Navigate<PopupMessageViewModel, string>(Strings["DefaultWarningMessage"]);
						break;
				}
				Task.Run(async () => { await UpdateNotificationViewModels(); });
			}
			catch (AppException.ApiException)
			{
				await NavigationService.Navigate<PopupWarningViewModel, string, bool>(Strings["ErrorConnectionMessage"]);
			}		
		}

		private async Task HandleCancelRequestType(Notification notification)
		{
			await Mvx.Resolve<ILoadingOverlayService>().ShowOverlay(Strings["LoadingDataOverlayTitle"]);
			var post = await ManagementService.GetPostDetail(notification.RelevantId.ToString(), _token);
			await Mvx.Resolve<ILoadingOverlayService>().CloseOverlay();
			await NavigationService.Navigate<RequestsViewModel, Post, bool>(post);

			await ManagementService.UpdateReadStatus(notification.Id.ToString(), true, _token);
		}

		private async Task HandleRequestType(Notification notification)
		{
			try
			{
				await _overlay.ShowOverlay(Strings["LoadingDataOverlayTitle"]);
				var request = await ManagementService.GetRequestById(notification.RelevantId, _token);
				await _overlay.CloseOverlay();
				if (request.Post == null)
				{
					await NavigationService.Navigate<PopupWarningViewModel, string, bool>(Strings["PostIsDeleted"]);
					await ManagementService.UpdateReadStatus(notification.Id.ToString(), true, _token);
					return;
				}
				var requestStatus = request.RequestStatus;
				var popupResult = PopupRequestStatusResult.Cancelled;
				switch (requestStatus)
				{
					case RequestStatus.Pending:
						popupResult = await NavigationService.Navigate<OtherRequestPendingViewModel, Request, PopupRequestStatusResult>(request);
						break;
					case RequestStatus.Received:
						popupResult = await NavigationService.Navigate<MyRequestReceivedViewModel, Request, PopupRequestStatusResult>(request);
						break;

					case RequestStatus.Approved:
						if (request.Post.User.Id.Equals(_dataModel.LoginResponse.Profile.Id))
						{
							popupResult = await NavigationService.Navigate<OtherRequestApprovedViewModel, Request, PopupRequestStatusResult>(request);

						}
						else popupResult = await NavigationService.Navigate<MyRequestApprovedViewModel, Request, PopupRequestStatusResult>(request);
						break;
					case RequestStatus.Rejected:
						
						await NavigationService.Navigate<PostDetailViewModel, Post, NavigationResult>(request.Post);
						break;
				}

				switch (popupResult)
				{
					case PopupRequestStatusResult.Received:
						await ReceiveGift(request.Id);
						break;

					case PopupRequestStatusResult.Removed:
						await CancelOldRequest(request.Post);
						break;

					case PopupRequestStatusResult.Rejected:
						OnRequestRejected(request);
						break;

					case PopupRequestStatusResult.Approved:
						OnRequestAccepted(request);
						break;

					case PopupRequestStatusResult.ShowPostDetail:					
						await NavigationService.Navigate<PostDetailViewModel, Post, NavigationResult>(request.Post);
						break;

					case PopupRequestStatusResult.ShowTakerProfile:
						await NavigationService.Navigate<UserProfileViewModel, User>(request.User);
						break;
					case PopupRequestStatusResult.ShowGiverProfile:
						await NavigationService.Navigate<UserProfileViewModel, User>(request.Post.User);
						break;
				}

				await ManagementService.UpdateReadStatus(notification.Id.ToString(), true, _token);
			}
			catch (AppException.ApiException)
			{
				await NavigationService.Navigate<PopupWarningViewModel, string, bool>(Strings["ErrorConnectionMessage"]);
			}
			finally
			{
				await _overlay.CloseOverlay();
			}
		}

		private async void OnRequestRejected(Request request)
		{
			var result = await NavigationService.Navigate<PopupMessageViewModel, string, RequestStatus>(Strings["RequestRejectingMessage"]);

			if (result == RequestStatus.Submitted)
			{
				var isSaved = await ManagementService.ChangeStatusOfRequest(request.Id, "Rejected", _token);
				await _overlay.CloseOverlay();
				if (isSaved)
				{
					await NavigationService.Navigate<PopupWarningViewModel, string, bool>(Strings["SuccessfulRejectionMessage"]);
				}
			}
		}

		private async void OnRequestAccepted(Request request)
		{
			var result = await NavigationService.Navigate<PopupResponseViewModel, Request, RequestStatus>(request);
			if (result == RequestStatus.Submitted)
			{
				await NavigationService.Navigate<PopupWarningViewModel, string, bool>(Strings["SuccessfulAcceptanceMessage"]);
			}
		}

		private async Task HandleLikeAndIsRejectedType(Notification notification)
		{
			await _overlay.ShowOverlay(Strings["LoadingDataOverlayTitle"]);
			_dataModel.CurrentPost = await ManagementService.GetPostDetail(notification.RelevantId.ToString(), _token);
			_dataModel.CurrentPost.IsMyPost = notification.Type == "Like";			
			await _overlay.CloseOverlay();

			await NavigationService.Navigate<PostDetailViewModel, Post, NavigationResult>(_dataModel.CurrentPost);

			await ManagementService.UpdateReadStatus(notification.Id.ToString(), true, _token);
		}

		private async Task ReceiveGift(string requestId)
		{
			await _overlay.ShowOverlay(Strings["UpdateOverLayTitle"]);
			try
			{
				await ManagementService.ChangeStatusOfRequest(requestId, RequestStatus.Received.ToString(), _dataModel.LoginResponse.Token);
			}
			catch (AppException.ApiException)
			{
				await NavigationService.Navigate<PopupWarningViewModel, string, bool>(Strings["ErrorConnectionMessage"]);
			}
			finally
			{
				await _overlay.CloseOverlay();
			}
		}

		private async Task CancelOldRequest(Post post)
		{
			var popupResult = await NavigationService.Navigate<PopupMessageViewModel, string, RequestStatus>(Strings["CancelRequestConfirm"]);
			if (popupResult != RequestStatus.Submitted)
			{
				return;
			}
			await _overlay.ShowOverlay(Strings["UpdateOverLayTitle"]);
			try
			{
				await ManagementService.CancelUserRequest(post.PostId, _dataModel.LoginResponse.Token);
			}
			catch (AppException.ApiException)
			{
				await NavigationService.Navigate<PopupWarningViewModel, string, bool>(Strings["ErrorConnectionMessage"]);
			}
			finally
			{
				await _overlay.CloseOverlay();
			}
		}

		private async void OnRefresh()
		{
			IsRefreshing = true;
			await UpdateNotificationViewModels();
			//Review ThanhVo should await Task.Run. Do we need to use Task.Run in this case?
			//Answer: This is done in background to let the server know that all notification are seen. None of popup will be shown to the user.
			Task.Run(async () =>
			{
				try
				{
					await ManagementService.UpdateSeenNotificationStatus(true, DataModel.LoginResponse.Token);
				}
				catch (AppException.ApiException)
				{

				}
			});
			IsRefreshing = false;
		}
	}
}
