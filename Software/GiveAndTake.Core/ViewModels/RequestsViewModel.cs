﻿using GiveAndTake.Core.Exceptions;
using GiveAndTake.Core.Models;
using GiveAndTake.Core.Services;
using GiveAndTake.Core.ViewModels.Base;
using GiveAndTake.Core.ViewModels.Popup;
using GiveAndTake.Core.ViewModels.Popup.OtherRequest;
using MvvmCross.Commands;
using MvvmCross.ViewModels;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace GiveAndTake.Core.ViewModels
{
	public class RequestsViewModel : BaseViewModel<Post, bool>
	{
		public string Title => Strings["RequestsTitle"];

		public bool IsRefreshing
		{
			get => _isRefresh;
			set => SetProperty(ref _isRefresh, value);
		}

		public int NumberOfRequest
		{
			get => _numberOfRequest;
			set => SetProperty(ref _numberOfRequest, value);
		}

		public MvxObservableCollection<RequestItemViewModel> RequestItemViewModels
		{
			get => _requestItemViewModels;
			set => SetProperty(ref _requestItemViewModels, value);
		}

		public IMvxCommand RefreshCommand => _refreshCommand = _refreshCommand ?? new MvxCommand(OnRefresh);
		public IMvxCommand LoadMoreCommand => _loadMoreCommand = _loadMoreCommand ?? new MvxAsyncCommand(OnLoadMore);

		public IMvxCommand BackPressedCommand =>
			_backPressedCommand = _backPressedCommand ?? new MvxCommand(OnBackPressed);

		private async void OnBackPressed()
		{
			await NavigationService.Close(this, _isReloadInProfileView);
		}

		private readonly IDataModel _dataModel;
		private MvxObservableCollection<RequestItemViewModel> _requestItemViewModels;
		private int _numberOfRequest;
		private bool _isRefresh;
		private IMvxCommand _refreshCommand;
		private IMvxCommand _loadMoreCommand;
		private IMvxCommand _backPressedCommand;
		private string _postId;
		private Post _post;
		private readonly ILoadingOverlayService _overlay;
		private bool _isReloadInProfileView = false;

		public RequestsViewModel(IDataModel dataModel, ILoadingOverlayService loadingOverlayService)
		{
			_dataModel = dataModel;
			_overlay = loadingOverlayService;
		}

		private async Task OnLoadMore()
		{
			try
			{
				_dataModel.ApiRequestsResponse = await ManagementService.GetRequestOfPost(_postId,
					$"limit={AppConstants.NumberOfRequestPerPage}&page={_dataModel.ApiRequestsResponse.Pagination.Page + 1}", _dataModel.LoginResponse.Token);
				if (_dataModel.ApiRequestsResponse.Requests.Any())
				{
					RequestItemViewModels.Last().IsSeperatorShown = true;
					RequestItemViewModels.AddRange(_dataModel.ApiRequestsResponse.Requests.Select(GenerateRequestItem));
					RequestItemViewModels.Last().IsSeperatorShown = false;
				}
			}
			catch (AppException.ApiException)
			{
				await NavigationService.Navigate<PopupWarningViewModel, string, bool>(Strings["ErrorConnectionMessage"]);
			}
		}

		private RequestItemViewModel GenerateRequestItem(Request request)
		{
			var requestItem = new RequestItemViewModel(request)
			{
				ClickAction = OnItemClicked,
				AcceptAction = OnRequestAccepted,
				RejectAction = OnRequestRejected
			};
			return requestItem;
		}

		private async void OnRequestRejected(Request request)
		{
			var result = await NavigationService.Navigate<PopupMessageViewModel, string, RequestStatus>(Strings["RequestRejectingMessage"]);
			if (result == RequestStatus.Submitted)
			{
				try
				{
					await _overlay.ShowOverlay(Strings["LoadingDataOverlayTitle"]);
					await ManagementService.ChangeStatusOfRequest(request.Id, RequestStatus.Rejected.ToString(),
						_dataModel.LoginResponse.Token);
					await UpdateRequestItemViewModelCollection();
					_isReloadInProfileView = true;
				}
				catch (AppException.ApiException)
				{
					await NavigationService.Navigate<PopupWarningViewModel, string, bool>(Strings["ErrorConnectionMessage"]);
				}
				finally
				{
					await _overlay.CloseOverlay();
				}
			}
		}

		private async void OnRequestAccepted(Request request)
		{
			var result = await NavigationService.Navigate<PopupResponseViewModel, Request, RequestStatus>(request);
			if (result == RequestStatus.Submitted)
			{
				try
				{
					await Task.Delay(777);
					await _overlay.ShowOverlay(Strings["LoadingDataOverlayTitle"]);
					await UpdateRequestItemViewModelCollection();
				}
				catch (AppException.ApiException)
				{
					await NavigationService.Navigate<PopupWarningViewModel, string, bool>(Strings["ErrorConnectionMessage"]);
				}
				finally
				{
					await _overlay.CloseOverlay();
				}
			}
		}

		private async void OnItemClicked(Request request)
		{
			var requestId = new Guid(request.Id);
			var requestResponse = await ManagementService.GetRequestById(requestId, _dataModel.LoginResponse.Token);
			var requestStatus = requestResponse.RequestStatus;
			var popupResult = PopupRequestStatusResult.Cancelled;
			switch (requestStatus)
			{
				case RequestStatus.Pending:
					popupResult = await NavigationService.Navigate<OtherRequestPendingViewModel, Request, PopupRequestStatusResult>(requestResponse);
					break;
				case RequestStatus.Approved:
					popupResult = await NavigationService.Navigate<OtherRequestApprovedViewModel, Request, PopupRequestStatusResult>(requestResponse);
					break;
				case RequestStatus.Received:
					popupResult = await NavigationService.Navigate<OtherRequestApprovedViewModel, Request, PopupRequestStatusResult>(requestResponse);
					break;
			}
			switch (popupResult)
			{
				case PopupRequestStatusResult.Rejected:
					OnRequestRejected(request);
					break;

				case PopupRequestStatusResult.Approved:
					OnRequestAccepted(request);
					break;

				case PopupRequestStatusResult.ShowPostDetail:
					await NavigationService.Navigate<PostDetailViewModel, Post, NavigationResult>(_post);
					break;

				case PopupRequestStatusResult.ShowTakerProfile:
					await NavigationService.Navigate<UserProfileViewModel, User>(requestResponse.User);
					break;
				case PopupRequestStatusResult.ShowGiverProfile:
					await NavigationService.Navigate<UserProfileViewModel, User>(requestResponse.Post.User);
					break;
			}
		}

		private async void OnRefresh()
		{
			IsRefreshing = true;
			await UpdateRequestItemViewModelCollection();
			IsRefreshing = false;
		}

		public async Task UpdateRequestItemViewModelCollection()
		{
			_dataModel.ApiRequestsResponse = await ManagementService.GetRequestOfPost(_postId, $"limit={AppConstants.NumberOfRequestPerPage}", _dataModel.LoginResponse.Token);
			NumberOfRequest = _dataModel.ApiRequestsResponse.Pagination.Totals;
			RequestItemViewModels = new MvxObservableCollection<RequestItemViewModel>(_dataModel.ApiRequestsResponse.Requests.Select(GenerateRequestItem));
			if (RequestItemViewModels.Any())
			{
				RequestItemViewModels.Last().IsSeperatorShown = false;
			}
		}

		public override void Prepare(Post post)
		{
			_postId = post.PostId;
			_post = post;
		}

		public override async void ViewAppearing()
		{
			base.ViewAppearing();
			await LoadRequestListData();
		}

		public async Task LoadRequestListData()
		{
			try
			{
				await _overlay.ShowOverlay(Strings["LoadingDataOverlayTitle"]);
				await UpdateRequestItemViewModelCollection();
			}
			catch (AppException.ApiException)
			{
				await NavigationService.Navigate<PopupWarningViewModel, string, bool>(Strings["ErrorConnectionMessage"]);
				await LoadRequestListData();
			}
			finally
			{
				await _overlay.CloseOverlay();
			}
		}
	}
}
