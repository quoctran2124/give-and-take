﻿using System;
using FFImageLoading.Transformations;
using FFImageLoading.Work;
using GiveAndTake.Core.Exceptions;
using GiveAndTake.Core.Helpers;
using GiveAndTake.Core.Models;
using GiveAndTake.Core.Services;
using GiveAndTake.Core.ViewModels.Base;
using GiveAndTake.Core.ViewModels.Popup;
using I18NPortable;
using MvvmCross.Commands;
using MvvmCross.UI;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using GiveAndTake.Core.Helpers.Interface;
using GiveAndTake.Core.ViewModels.Popup.MyRequest;
using MvvmCross;

namespace GiveAndTake.Core.ViewModels
{
	public class NavigationResult
	{
		public bool ShouldReload { get; set; }
		public bool ShouldNavigateToProfileViewModel { get; set; }
	}

	public class PostDetailViewModel : BaseViewModel<Post, NavigationResult>
	{
		#region Properties

		public IMvxCommand ShowGiverProfileCommand =>
			_showGiverProfileCommand ?? (_showGiverProfileCommand = new MvxAsyncCommand(ShowGiverProfile));

		public IMvxCommand ShowMenuPopupCommand =>
			_showMenuPopupCommand ?? (_showMenuPopupCommand = new MvxAsyncCommand(ShowMenuView));

		public IMvxCommand OnRequestIconClickCommand =>
			_onRequestIconClickCommand ?? (_onRequestIconClickCommand = new MvxAsyncCommand(OnRequestIconClicked));

		public IMvxCommand OnAppreciationIconClickCommand =>
			_onAppreciationIconClickCommand ?? (_onAppreciationIconClickCommand = new MvxAsyncCommand(OnAppreciationIconClicked));

		public IMvxCommand<int> ShowFullImageCommand =>
			_showFullImageCommand ?? (_showFullImageCommand = new MvxCommand<int>(ShowFullImage));

		public IMvxCommand NavigateLeftCommand =>
			_navigateLeftCommand ?? (_navigateLeftCommand = new MvxCommand(() => PostImageIndex--));

		public IMvxCommand NavigateRightCommand =>
			_navigateRightCommand ?? (_navigateRightCommand = new MvxCommand(() => PostImageIndex++));

		public IMvxCommand<int> UpdateImageIndexCommand =>
			_updateImageIndexCommand ?? (_updateImageIndexCommand = new MvxCommand<int>(index => PostImageIndex = index));

		public IMvxCommand BackPressedCommand =>
			_backPressedCommand ?? (_backPressedCommand = new MvxCommand(() => NavigationService.Close(this, new NavigationResult { ShouldReload = IsLoadInHomeView })));

		public IMvxCommand ShareCommand =>
			_shareCommand ?? (_shareCommand = new MvxCommand(OnShare));
		
		public string CategoryName
		{
			get => $"   {_categoryName}   ";
			set => SetProperty(ref _categoryName, value);
		}

		public string Address
		{
			get => _address;
			set => SetProperty(ref _address, value);
		}

		public string Status
		{
			get => _status;
			set => SetProperty(ref _status, value);
		}

		public List<Image> PostImages
		{
			get => _postImages;
			set => SetProperty(ref _postImages, value);
		}

		public int RequestCount
		{
			get => _requestCount;
			set => SetProperty(ref _requestCount, value);
		}

		public int CommentCount
		{
			get => _commentCount;
			set => SetProperty(ref _commentCount, value);
		}

		public int AppreciationCount
		{
			get => _appreciationCount;
			set => SetProperty(ref _appreciationCount, value);
		}

		public string CategoryBackgroundColor
		{
			get => _categoryBackgroundColor;
			set => SetProperty(ref _categoryBackgroundColor, value);
		}

		public string AvatarUrl
		{
			get => _avatarUrl;
			set => SetProperty(ref _avatarUrl, value);
		}

		public string UserName
		{
			get => _userName;
			set => SetProperty(ref _userName, value);
		}

		public string CreatedTime
		{
			get => _createdTime;
			set => SetProperty(ref _createdTime, value);
		}

		public string PostTitle
		{
			get => _postTitle;
			set => SetProperty(ref _postTitle, value);
		}

		public string PostDescription
		{
			get => _postDescription;
			set => SetProperty(ref _postDescription, value);
		}

		public int PostImageIndex
		{
			get => _postImageIndex;
			set
			{
				SetProperty(ref _postImageIndex, value);
				_dataModel.PostImageIndex = value;
				UpdateImageIndexIndicator();
				UpdateNavigationButtons();
			}
		}

		public bool CanNavigateLeft
		{
			get => _canNavigateLeft;
			set => SetProperty(ref _canNavigateLeft, value);
		}

		public bool CanNavigateRight
		{
			get => _canNavigateRight;
			set => SetProperty(ref _canNavigateRight, value);
		}

		public string ImageIndexIndicator
		{
			get => _imageIndexIndicator;
			set => SetProperty(ref _imageIndexIndicator, value);
		}

		public bool IsRequestIconActivated
		{
			get => _isRequestIconActivated;
			set => SetProperty(ref _isRequestIconActivated, value);
		}

		public bool IsAppreciationIconActivated
		{
			get => _isAppreciationIconActivated;
			set => SetProperty(ref _isAppreciationIconActivated, value);
		}

		public bool IsLoadInHomeView
		{
			get => _isLoadInHomeView;
			set => SetProperty(ref _isLoadInHomeView, value);
		}

		public MvxColor StatusColor
		{
			get => _statusColor;
			set => SetProperty(ref _statusColor, value);
		}

		public bool IsThreeDotsButtonShown
		{
			get => _isThreeDotsButtonShown;
			set => SetProperty(ref _isThreeDotsButtonShown, value);
		}

		public List<ITransformation> AvatarTransformations => new List<ITransformation> { new CircleTransformation() };

		public string ShareFacebookTitle => Strings["ShareTitle"];
		private string _shareContentAddress = Strings["ShareContentAddress"];
		private string _shareContentCategory = Strings["ShareContentCategory"];
		private string _shareContentTitle = Strings["ShareContentTitle"];
		private string _shareContentCreatedDay = Strings["ShareContentCreatedDay"];
		private string _shareContentDescription = Strings["ShareContentDescription"];

		private bool _isThreeDotsButtonShown;
		private readonly IDataModel _dataModel;
		private IMvxCommand _shareCommand;
		private IMvxCommand _showGiverProfileCommand;
		private IMvxCommand _showMenuPopupCommand;
		private IMvxCommand _onRequestIconClickCommand;
		private IMvxCommand _onAppreciationIconClickCommand;
		private IMvxCommand _navigateLeftCommand;
		private IMvxCommand _navigateRightCommand;
		private IMvxCommand _backPressedCommand;
		private IMvxCommand<int> _showFullImageCommand;
		private IMvxCommand<int> _updateImageIndexCommand;
		private string _categoryName;
		private string _address;
		private string _status;
		private string _categoryBackgroundColor;
		private string _avatarUrl;
		private string _userName;
		private string _createdTime;
		private string _postTitle;
		private string _postDescription;
		private string _imageIndexIndicator;
		private int _requestCount;
		private int _commentCount;
		private int _appreciationCount;
		private int _postImageIndex;
		private bool _canNavigateLeft;
		private bool _canNavigateRight;
		private List<Image> _postImages;
		private Post _post;
		private bool _isRequestIconActivated;
		private bool _isAppreciationIconActivated;
		private readonly ILoadingOverlayService _overlay;
		private bool _isBackFromFullImage;
		private bool _isLoadFirstTime = true;
		private bool _isLoadInHomeView;
		private MvxColor _statusColor;
		private readonly ColorHelper _colorHelper;

		#endregion

		#region Constructor

		public PostDetailViewModel(IDataModel dataModel, ILoadingOverlayService loadingOverlayService)
		{
			_colorHelper = new ColorHelper();
			_dataModel = dataModel;
			_overlay = loadingOverlayService;
		}

		public override void Prepare(Post post)
		{
			_post = post;
			_post.IsMyPost = post.User.Id.Equals(_dataModel.LoginResponse.Profile.Id);
			PostImages = _post.Images;
			_dataModel.PostImageIndex = 0;
		}

		public override async void ViewAppearing()
		{
			base.ViewAppearing();

			if (!_isBackFromFullImage && _isLoadFirstTime)
			{
				await LoadCurrentPostDataWithOverlay(Strings["LoadingDataOverlayTitle"]);
			}
			_isBackFromFullImage = false;
			_isLoadFirstTime = false;
		}

		private async Task LoadCurrentPostDataWithOverlay(string overlayTitle)
		{
			try
			{
				await _overlay.ShowOverlay(overlayTitle);
				await LoadCurrentPostData();
			}
			catch (AppException.ApiException)
			{
				await NavigationService.Navigate<PopupWarningViewModel, string, bool>(Strings["ErrorConnectionMessage"]);
				await Task.Delay(777);//for iphone
				await LoadCurrentPostDataWithOverlay(overlayTitle);
			}
			finally
			{
				await _overlay.CloseOverlay();
			}
		}

		private async Task LoadCurrentPostData()
		{
			_dataModel.CurrentPost = await ManagementService.GetPostDetail(_post.PostId, _dataModel.LoginResponse.Token);
			_dataModel.CurrentPost.IsMyPost = _post.IsMyPost;
			_post = _dataModel.CurrentPost;

			RequestCount = _post.RequestCount;
			AppreciationCount = _post.AppreciationCount;
			
			IsRequestIconActivated = _post.IsMyPost ? RequestCount > 0 : _post.IsRequested;
			IsAppreciationIconActivated = _post.IsAppreciated;
			CategoryName = _post.Category.CategoryName.Translate();
			AvatarUrl = _post.User.AvatarUrl;
			UserName = _post.User.DisplayName ?? AppConstants.DefaultUserName;
			CreatedTime = TimeHelper.ToTimeAgo(_post.CreatedTime);
			Address = _post.ProvinceCity.ProvinceCityName.Translate();
			PostDescription = _post.Description;
			PostTitle = _post.Title;
			PostImages = _post.Images;
			CommentCount = _post.CommentCount;
			Status = _post.IsMyPost ? _post.PostStatus.Translate() : _post.RequestedPostStatus?.Translate();
			StatusColor = _colorHelper.GetStatusColor(Status);
			CategoryBackgroundColor = _post.Category.BackgroundColor;
			PostImageIndex = 0;
			var postOptions = MenuOptionHelper.GetMenuOptions(Status);
			IsThreeDotsButtonShown = postOptions.Count != 0;
		}

		private void ShowFullImage(int position)
		{
			PostImageIndex = position;
			NavigationService.Navigate<PostImageViewModel, bool>()
				.ContinueWith(task => PostImageIndex = _dataModel.PostImageIndex);
			_isBackFromFullImage = true;
		}

		private async Task ShowMenuView()
		{
			var postOptions = MenuOptionHelper.GetMenuOptions(Status);

			var result = await NavigationService.Navigate<PopupExtensionOptionViewModel, List<string>, string>(postOptions);

			if (string.IsNullOrEmpty(result))
			{
				return;
			}

			if (result == Strings["MarkGiving"])
			{
				await ChangeStatusOfPost(null, PostStatus.Giving);
				await LoadCurrentPostDataWithOverlay(Strings["UpdateOverLayTitle"]);
			}
			else if (result == Strings["MarkGiven"])
			{
				var warningMessage = _post.RequestCount > 0 ? Strings["ConfirmChangeStatusOfPost"] : null;
				await ChangeStatusOfPost(warningMessage, PostStatus.Gave);
				await LoadCurrentPostDataWithOverlay(Strings["UpdateOverLayTitle"]);
			}
			else if (result == Strings["MarkReceived"])
			{
				await ChangeStatusOfRequest();
			}
			else if (result == Strings["Modify"])
			{
				await EditPost();
			}
			else if (result == Strings["ViewPostRequests"])
			{
				var isChangesInRequest = await NavigationService.Navigate<RequestsViewModel, Post, bool>(_post);
				IsLoadInHomeView = isChangesInRequest;
				RequestCount = _dataModel.ApiRequestsResponse.Pagination.Totals;
				IsRequestIconActivated = RequestCount > 0;
			}
			else if (result == Strings["Delete"])
			{
				await ChangeStatusOfPost(Strings["ConfirmDeletePost"], PostStatus.Deleted);
				await Task.Delay(777); //for iOS					
				await NavigationService.Close(this, new NavigationResult { ShouldReload = true });
			}
			else if (result == Strings["CancelRequest"])
			{
				await CancelOldRequest();
			}
			else
			{
				if (!_post.IsCurrentUserReported)
				{
					var requireReload = await NavigationService.Navigate<PopupReportViewModel, Post, bool>(_post);
					if (requireReload)
					{
						await LoadCurrentPostDataWithOverlay(Strings["LoadingDataOverlayTitle"]);
					}

				}
				else
				{
					await NavigationService.Navigate<PopupWarningViewModel, string, bool>(Strings["ReportPostNotAvailableMessage"]);
				}
			}

		}

		private async Task EditPost()
		{
			_dataModel.CurrentPost = _post;
			var result = await NavigationService.Navigate<CreatePostViewModel, ViewMode, bool>(ViewMode.EditPost);
			if (!result)
			{
				return;
			}
			await LoadCurrentPostDataWithOverlay(Strings["LoadingDataOverlayTitle"]);
			IsLoadInHomeView = true;
		}

		private async Task ChangeStatusOfPost(string warningMessage, PostStatus status)
		{
			if (!string.IsNullOrEmpty(warningMessage))
			{
				var userConfirmation = await NavigationService.Navigate<PopupMessageViewModel, string, RequestStatus>(warningMessage);
				if (userConfirmation != RequestStatus.Submitted)
				{
					return;
				}
			}
			try
			{
				await _overlay.ShowOverlay(Strings["ProcessingDataOverLayTitle"]);
				await ManagementService.ChangeStatusOfPost(_post.PostId, status.ToString(), _dataModel.LoginResponse.Token);
			}
			catch (AppException.ApiException)
			{
				await NavigationService.Navigate<PopupWarningViewModel, string, bool>(Strings["ErrorConnectionMessage"]);
			}
			finally
			{
				await _overlay.CloseOverlay();
			}
		}
		
		private async Task OnRequestIconClicked()
		{
			if (_post.IsMyPost)
			{
				var isChangesInRequest = await NavigationService.Navigate<RequestsViewModel, Post, bool>(_post);
				IsLoadInHomeView = isChangesInRequest;
				RequestCount = _dataModel.ApiRequestsResponse.Pagination.Totals;
				IsRequestIconActivated = RequestCount > 0;
			}
			else
			{
				if (IsRequestIconActivated)
				{
					await ReviewMyRequest();
				}
				else
				{
					await CreateNewRequest();
				}
			}
		}

		private async Task OnAppreciationIconClicked()
		{
			var result = await ManagementService.AppreciateAPost(_post.PostId, _dataModel.LoginResponse.Token);
			if (result.StatusCode == 200)
			{
				await LoadCurrentPostDataWithOverlay(Strings["UpdateOverLayTitle"]);
			}
		}

		private async Task CreateNewRequest()
		{
			var result = await NavigationService.Navigate<PopupCreateRequestViewModel, Post, RequestStatus>(_post);
			if (result == RequestStatus.Submitted)
			{
				await LoadCurrentPostDataWithOverlay(Strings["UpdateOverLayTitle"]);
			}
		}

		private async Task CancelOldRequest()
		{
			var popupResult = await NavigationService.Navigate<PopupMessageViewModel, string, RequestStatus>(Strings["CancelRequestConfirm"]);
			if (popupResult != RequestStatus.Submitted)
			{
				return;
			}
			try
			{
				await _overlay.ShowOverlay(Strings["UpdateOverLayTitle"]);
				await ManagementService.CancelUserRequest(_post.PostId, _dataModel.LoginResponse.Token);
				await LoadCurrentPostData();
			}
			catch (AppException.ApiException)
			{
				await NavigationService.Navigate<PopupWarningViewModel, string, bool>(Strings["ErrorConnectionMessage"]);
			}
			finally
			{
				await _overlay.CloseOverlay();
			}
		}

		private async Task ReceiveGift(string requestId)
		{
			try
			{
				await _overlay.ShowOverlay(Strings["UpdateOverLayTitle"]);
				await ManagementService.ChangeStatusOfRequest(requestId, RequestStatus.Received.ToString(), _dataModel.LoginResponse.Token);
				await LoadCurrentPostData();
			}
			catch (AppException.ApiException)
			{
				await NavigationService.Navigate<PopupWarningViewModel, string, bool>(Strings["ErrorConnectionMessage"]);
			}
			finally
			{
				await _overlay.CloseOverlay();
			}
		}

		private async Task ReviewMyRequest()
		{
			try
			{
				await _overlay.ShowOverlay(Strings["UpdateOverLayTitle"]);
				var request = await ManagementService.GetRequestOfCurrentUserByPostId(_post.PostId, _dataModel.LoginResponse.Token);
				await _overlay.CloseOverlay();
				var requestStatus = request.RequestStatus;
				PopupRequestStatusResult popupResult = PopupRequestStatusResult.Cancelled;
				switch (requestStatus)
				{
					case RequestStatus.Pending:
						popupResult = await NavigationService.Navigate<MyRequestPendingViewModel, Request, PopupRequestStatusResult>(request);
						break;
					case RequestStatus.Received:
						popupResult = await NavigationService.Navigate<MyRequestReceivedViewModel, Request, PopupRequestStatusResult>(request);
						break;
					case RequestStatus.Approved:
						popupResult = await NavigationService.Navigate<MyRequestApprovedViewModel, Request, PopupRequestStatusResult>(request);
						break;
					case RequestStatus.Rejected:
						await CreateNewRequest();
						break;
				}

				switch (popupResult)
				{
					case PopupRequestStatusResult.Received:
						await ReceiveGift(request.Id);
						break;
					case PopupRequestStatusResult.Removed:
						await CancelOldRequest();
						break;
					case PopupRequestStatusResult.ShowPostDetail:
						await NavigationService.Navigate<PostDetailViewModel, Post, NavigationResult>(_post);
						break;
					case PopupRequestStatusResult.ShowTakerProfile:
						await NavigationService.Navigate<UserProfileViewModel, User>(request.User);
						break;
					case PopupRequestStatusResult.ShowGiverProfile:
						await NavigationService.Navigate<UserProfileViewModel, User>(request.Post.User);
						break;
					case PopupRequestStatusResult.Cancelled:
						return;
				}
			}
			catch (Exception e)
			{
				if (e.Message != AppConstants.NotFound)
				{
					await NavigationService.Navigate<PopupWarningViewModel, string, bool>(Strings["PostIsDeleted"]);
				}
				else await NavigationService.Navigate<PopupWarningViewModel, string, bool>(Strings["ErrorConnectionMessage"]);
			}
			finally
			{
				await _overlay.CloseOverlay();
			}
		}

		private void UpdateNavigationButtons()
		{
			CanNavigateLeft = PostImages.Count > 1 && PostImageIndex > 0;
			CanNavigateRight = PostImages.Count > 1 && PostImageIndex < PostImages.Count - 1;
		}

		private void UpdateImageIndexIndicator()
		{
			var totalImage = _postImages.Count == 0 ? 1 : PostImages.Count;
			ImageIndexIndicator = _postImageIndex + 1 + " / " + totalImage;
		}

		private async Task ChangeStatusOfRequest()
		{
			try
			{
				await _overlay.ShowOverlay(Strings["UpdateOverLayTitle"]);
				var request = await ManagementService.GetRequestOfCurrentUserByPostId(_post.PostId, _dataModel.LoginResponse.Token);
				await ManagementService.ChangeStatusOfRequest(request.Id, RequestStatus.Received.ToString(), _dataModel.LoginResponse.Token);
				await LoadCurrentPostData();
			}
			catch (AppException.ApiException)
			{
				await NavigationService.Navigate<PopupWarningViewModel, string, bool>(Strings["ErrorConnectionMessage"]);
			}
			finally
			{
				await _overlay.CloseOverlay();
			}
		}

		private async Task ShowGiverProfile()
		{
			if (_post.IsMyPost)
			{
				await NavigationService.Close(this, new NavigationResult { ShouldNavigateToProfileViewModel = true });
			}
			else
			{
				await NavigationService.Navigate<UserProfileViewModel, User>(_post.User);
			}
		}

		private void OnShare()
		{
			var content = _shareContentAddress  + ": " + _post.ProvinceCity.ProvinceCityName.Translate() + "\n" +
			              _shareContentCategory + ": " + _post.Category.CategoryName.Translate() + "\n" +
			              _shareContentTitle + ": " + _post.Title+ "\n" +
						  _shareContentCreatedDay+ ": " + _post.CreatedTime.ToString(AppConstants.DateStringFormat) + "\n" +
			              _shareContentDescription + ": " + "\n" + _post.Description + "\n" +
						  AppConstants.GiveAndTakeHashtag;
			Mvx.Resolve<IFacebookHelper>().ShareFacebookContent(content, AppConstants.ShareFacebookLink + _post.PostId);
		}

		#endregion
	}
}
