using System.Threading.Tasks;
using GiveAndTake.Core.Exceptions;
using GiveAndTake.Core.Models;
using GiveAndTake.Core.ViewModels.Base;
using GiveAndTake.Core.ViewModels.Popup;
using MvvmCross.Commands;
using GiveAndTake.Core.Services;
using GiveAndTake.Core.Helpers;
using MvvmCross;

namespace GiveAndTake.Core.ViewModels
{
	public class LoginViewModel : BaseViewModel
	{
		private readonly IDataModel _dataModel;
		private readonly ILoadingOverlayService _overlay;
		private User _user;
		public string LoginTitle => Strings["LoginTitle"];
		public string AcceptTermAndConditionInHtml => DataModel.Language == LanguageType.vi ? AppConstants.AcceptTermAndConditionInHtmlVn : AppConstants.AcceptTermAndConditionInHtmlEn;

		public User User
		{
			get => _user;
			set => SetProperty(ref _user, value);
		}

		private IMvxAsyncCommand _handleLostConnectionCommand;
		private IMvxCommand _loginCommand;
		private IMvxCommand _showConditionCommand;

		public IMvxCommand LoginCommand => _loginCommand ?? (_loginCommand = new MvxAsyncCommand<User>(OnLoginSuccess));
		public IMvxCommand ShowConditionCommand => _showConditionCommand ?? (_showConditionCommand = new MvxCommand(NavigateToConditionView));
		public IMvxAsyncCommand HandleLostConnectionCommand => _handleLostConnectionCommand ?? (_handleLostConnectionCommand = new MvxAsyncCommand(HandleLostInternetConnection));
		public LoginViewModel(IDataModel dataModel,ILoadingOverlayService loadingOverlayService)
		{
			_dataModel = dataModel;
			_overlay = loadingOverlayService;
		}

		private async Task HandleLostInternetConnection()
		{
			await NavigationService.Navigate<PopupWarningViewModel, string, bool>(Strings["ErrorConnectionMessage"]);
		}

		private async Task OnLoginSuccess(User baseUser)
		{
			try
			{
				await _overlay.ShowOverlay(Strings["LoginProcessOverLayTitle"]);
				_dataModel.LoginResponse = await ManagementService.LoginFacebook(baseUser);
				if (_dataModel.LoginResponse.Profile.Status == AppConstants.UserStatusBlocked)
				{
					await _overlay.CloseOverlay();
					await NavigationService.Navigate<PopupWarningBlockedUserViewModel>();
				}
				else
				{
					await Task.Run(async () =>
					{
						await ManagementService.SendPushNotificationUserInformation(new PushNotificationUserInformation()
						{
							DeviceToken = Mvx.Resolve<IDeviceInfo>().DeviceToken,
							MobilePlatform = Mvx.Resolve<IDeviceInfo>().MobilePlatform,
							Language = DataModel.Language.ToString()
						}, _dataModel.LoginResponse.Token);
					});
					await NavigationService.Navigate<MasterViewModel>();
					await NavigationService.Close(this);	
				}
			}
			catch (AppException.ApiServiceUnavailableException)
			{
				await _overlay.CloseOverlay();
				await NavigationService.Navigate<PopupAppUpdateViewModel>();
			}
			catch (AppException.ApiException)
			{
				await _overlay.CloseOverlay();
				await NavigationService.Navigate<PopupWarningViewModel, string, bool>(Strings["ErrorConnectionMessage"]);
			}
		}

		private void NavigateToConditionView()
		{
			NavigationService.Navigate<TermAndConditionViewModel>();
		}
	}
}
