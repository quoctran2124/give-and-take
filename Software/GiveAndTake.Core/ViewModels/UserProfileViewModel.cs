using FFImageLoading.Transformations;
using FFImageLoading.Work;
using GiveAndTake.Core.Exceptions;
using GiveAndTake.Core.Helpers;
using GiveAndTake.Core.Models;
using GiveAndTake.Core.Services;
using GiveAndTake.Core.ViewModels.Base;
using GiveAndTake.Core.ViewModels.Popup;
using I18NPortable;
using MvvmCross.Commands;
using MvvmCross.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GiveAndTake.Core.ViewModels.TabNavigation;

namespace GiveAndTake.Core.ViewModels
{
	public class UserProfileViewModel : BaseViewModel<User>
    {
		public string RankTitle => Strings["RankTitle"];

		public string SentTitle => Strings["SentTitle"];

		public List<ITransformation> AvatarTransformations => new List<ITransformation> { new CircleTransformation() };


		public string AvatarUrl
		{
			get => _avatarUrl;
			set => SetProperty(ref _avatarUrl, value);
		}

		public string UserName
		{
			get => _userName;
			set => SetProperty(ref _userName, value);
		}

		public string RankType
		{
			get => _rankType;
			set => SetProperty(ref _rankType, value);
		}

		public string SentCount
		{
			get => _sentCount;
			set => SetProperty(ref _sentCount, value);
		}

		public bool IsSearchResultNull
		{
			get => _isSearchResultNull;
			set => SetProperty(ref _isSearchResultNull, value);
		}

		public bool IsPostsRefreshing
		{
			get => _isPostsRefresh;
			set => SetProperty(ref _isPostsRefresh, value);
		}

		public MvxObservableCollection<PostItemViewModel> PostViewModels
		{
			get => _postViewModels;
			set
			{
				SetProperty(ref _postViewModels, value);
				SentCount = $"{_posts?.FirstOrDefault()?.User?.AppreciationNumber ?? 0} " + Strings["Times"];
			}
		}

		public IMvxCommand ShowFullSizeAvatarImageCommand =>
			_showFullSizeAvatarImageCommand ?? (_showFullSizeAvatarImageCommand = new MvxCommand(ShowFullSizeAvatarImage));

		public IMvxCommand RefreshPostsCommand =>
		    _refreshPostsCommand ?? (_refreshPostsCommand = new MvxAsyncCommand(OnRefreshPosts));

	    public IMvxCommand LoadMorePostsCommand =>
			_loadMorePostsCommand ?? (_loadMorePostsCommand = new MvxAsyncCommand(OnLoadMorePosts));

	    public IMvxCommand OpenConversationCommand =>
			_openConversationCommand ?? (_openConversationCommand = new MvxAsyncCommand(OpenConversation));

	    public IMvxCommand BackPressedCommand =>
		    _backPressedCommand ?? (_backPressedCommand = new MvxAsyncCommand(OnBackPressed));

	    public IMvxCommand ShowMenuPopupCommand =>
		    _showMenuPopupCommand ?? (_showMenuPopupCommand = new MvxAsyncCommand(ShowMenuSettingView));

		private User _user;
		private string _userName;
		private string _rankType;
		private string _avatarUrl;
		private string _sentCount;
		private bool _isPostsRefresh;
		private bool _isSearchResultNull;
	    private readonly IDataModel _dataModel;
	    private ApiPostsResponse _apiPostsResponse;
		private IMvxCommand _refreshPostsCommand;
		private IMvxCommand _loadMorePostsCommand;
		private IMvxCommand _openConversationCommand;
		private IMvxCommand _backPressedCommand;
		private IMvxCommand _showFullSizeAvatarImageCommand;
	    private IMvxCommand _showMenuPopupCommand;
		private MvxObservableCollection<PostItemViewModel> _postViewModels;
	    private List<Post> _posts;
	    private readonly ILoadingOverlayService _overlay;

		public UserProfileViewModel(IDataModel dataModel, ILoadingOverlayService overlay)
		{
			_dataModel = dataModel;
			_overlay = overlay;
		}

	    public override void Prepare(User user)
	    {
		    _user = user;
			AvatarUrl = user.AvatarUrl;
		    UserName = user.DisplayName?.ToUpper();
		    RankType = user.MemberType.Translate();
		}

	    public override Task Initialize()
	    {
			//TODO: Add loading indicator here
		    return UpdatePostsViewModels();
	    }

	    private async Task OnRefreshPosts()
		{
			IsPostsRefreshing = true;
			await UpdatePostsViewModels();
			IsPostsRefreshing = false;
		}

		private async Task OnLoadMorePosts()
		{
			try
			{
				_apiPostsResponse = await ManagementService.GetMyPostList(_user.Id, $"page={_apiPostsResponse.Pagination.Page + 1}", _dataModel.LoginResponse.Token);

				if (_apiPostsResponse.Posts.Any())
				{
					PostViewModels.Last().IsSeparatorLineShown = true;
					PostViewModels.AddRange(GeneratePostItemViewModelCollection(_apiPostsResponse.Posts, _posts));
					PostViewModels.Last().IsSeparatorLineShown = false;
				}
			}
			catch (AggregateException)
			{
				var result = await NavigationService.Navigate<PopupMessageViewModel, string, RequestStatus>(Strings["ErrorConnectionMessage"]);
				if (result == RequestStatus.Submitted)
				{
					await OnLoadMorePosts();
				}
			}
		}

		private async Task UpdatePostsViewModels()
		{
			try
			{
				_apiPostsResponse = await ManagementService.GetMyPostList(_user.Id, null, _dataModel.LoginResponse.Token);
				_posts = _apiPostsResponse.Posts;
				PostViewModels = new MvxObservableCollection<PostItemViewModel>();
				if (_apiPostsResponse.Posts.Any())
				{
					PostViewModels.AddRange(_apiPostsResponse.Posts.Select(GeneratePostViewModels));
					PostViewModels.Last().IsSeparatorLineShown = false;
				}

				IsSearchResultNull = PostViewModels.Any();
			}
			catch (AppException.ApiException)
			{
				var result = await NavigationService.Navigate<PopupMessageViewModel, string, RequestStatus>(Strings["ErrorConnectionMessage"]);
				if (result == RequestStatus.Submitted)
				{
					await UpdatePostsViewModels();
				}
			}
		}
	    private MvxObservableCollection<PostItemViewModel> GeneratePostItemViewModelCollection(List<Post> newPosts, List<Post> currentPosts)
	    {
		    var newPostViewModels = new MvxObservableCollection<PostItemViewModel>();
		    foreach (var post in newPosts)
		    {
			    if (currentPosts.All(p => !p.PostId.Equals(post.PostId)))
			    {
				    newPostViewModels.Add(GeneratePostViewModels(post));
				    currentPosts.Add(post);
			    }
		    }
		    return newPostViewModels;
	    }

		private PostItemViewModel GeneratePostViewModels(Post post)
		{
			post.IsMyPost = false;
			return new PostItemViewModel(_dataModel, post)
			{
				IsStatusShown = false
			};
		}

		private async Task OpenConversation()
		{
			//TODO: close current view and navigate to conversation view
			await NavigationService.Navigate<PopupWarningViewModel, string>(Strings["DefaultWarningMessage"]);
		}

		private void ShowFullSizeAvatarImage()
		{
			var avaData = new AvatarData()
			{
				ImageUrl = AvatarUrl,
				Base64String = null
			};
			NavigationService.Navigate<AvatarImageViewModel, AvatarData>(avaData);
		}

	    private Task OnBackPressed() => NavigationService.Close(this);

	    private async Task ShowMenuSettingView()
	    {
		    var result = await NavigationService.Navigate<PopupExtensionOptionViewModel, List<string>, string>(MenuOptionHelper.OtherProfileMenuOption);

		    if (string.IsNullOrEmpty(result)) return;

		    if (result == Strings["BlockUser"])
		    {
			    await BlockUser();
			}
	    }

	    private async Task BlockUser()
	    {
		    var confirmResult = await NavigationService.Navigate<PopupAlertViewModel, PopupMessageParam, RequestStatus>(
			    new PopupMessageParam
			    {
				    Title = Strings["BlockUserConfirm"],
				    Message = Strings["BlockUserContent"],
				    ConfirmTitle = Strings["BlockLabel"],
					CancelTitle = Strings["CancelTitle"]
			    });
		    if (confirmResult != RequestStatus.Submitted)
		    {
			    return;
		    }

		    try
		    {
			    await _overlay.ShowOverlay(Strings["LoadingDataOverlayTitle"]);
				await ManagementService.BlockUser(_user.Id, _dataModel.LoginResponse.Token);
				await NavigationService.Navigate<MasterViewModel>();
		    }
		    catch (AppException.ApiException)
		    {
			    await NavigationService.Navigate<PopupWarningViewModel, string, bool>(Strings["ErrorConnectionMessage"]);
		    }
		    finally
		    {
			    await _overlay.CloseOverlay();
		    }
		}
	}
}
