﻿using GiveAndTake.Core.Helpers;
using GiveAndTake.Core.Models;
using GiveAndTake.Core.ViewModels.Base;
using GiveAndTake.Core.ViewModels.Popup;
using MvvmCross.Commands;
using MvvmCross.Plugin.PictureChooser;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using GiveAndTake.Core.Exceptions;
using GiveAndTake.Core.Services;
using I18NPortable;
using Plugin.Permissions.Abstractions;

namespace GiveAndTake.Core.ViewModels
{
	public class CreatePostViewModel : BaseViewModel<ViewMode, bool>
	{
		private readonly IDataModel _dataModel;
		private readonly IMvxPictureChooserTask _pictureChooserTask;

		private MvxCommand _takePictureCommand;
		private ICommand _submitCommand;
		private IMvxAsyncCommand _showPhotoCollectionCommand;
		private IMvxCommand _showCategoriesCommand;
		private IMvxCommand _showProvinceCityCommand;
		private IMvxCommand _backPressedCommand;
		private IMvxCommand<List<byte[]>> _imageCommand;
		private readonly ILoadingOverlayService _overlay;

		private readonly DebouncerHelper _debouncer;
		private string _postDescription;
		private string _postTitle;
		private string _category = Strings["DefaultCategoryCreatePostName"];
		private string _provinceCity = Strings["DefaultLocationFilter"];
		private byte[] _bytes;
		private string _selectedImage;
		private Category _selectedCategory;
		private ProvinceCity _selectedProvinceCity;
		private List<PostImage> _postImages = new List<PostImage>();
		private bool _visibleSelectedImage;
		private string _postId;
		private ViewMode _viewModelMode;
		private bool _isSubmitBtnEnabled;
		private bool _isLoadFirstTime = true;

		public ICommand SubmitCommand => _submitCommand ?? (_submitCommand = new MvxAsyncCommand(InitSubmit));
		public IMvxAsyncCommand ShowPhotoCollectionCommand =>
			_showPhotoCollectionCommand ?? (_showPhotoCollectionCommand = new MvxAsyncCommand(ShowPhotoCollection));
		public IMvxCommand ShowCategoriesCommand =>
			_showCategoriesCommand ?? (_showCategoriesCommand = new MvxCommand(ShowCategoriesPopup));
		public IMvxCommand ShowProvinceCityCommand =>
			_showProvinceCityCommand ?? (_showProvinceCityCommand = new MvxCommand(ShowLocationFiltersPopup));
		public IMvxCommand BackPressedCommand =>
			_backPressedCommand ?? (_backPressedCommand = new MvxCommand(BackPressed));
		public IMvxCommand<List<byte[]>> ImageCommand =>
			_imageCommand ?? (_imageCommand = new MvxCommand<List<byte[]>>(InitNewImage));
		public IMvxCommand TakePictureCommand =>
			_takePictureCommand = _takePictureCommand ?? new MvxCommand(DoTakePicture);


		public string PostDescription
		{
			get => _postDescription;
			set
			{
				SetProperty(ref _postDescription, value);
				UpdateSubmitBtn();
			}
		}

		public string PostTitle
		{
			get => _postTitle;
			set
			{
				SetProperty(ref _postTitle, value);
				UpdateSubmitBtn();
			}
		}

		public byte[] Bytes
		{
			get => _bytes;
			set { _bytes = value; RaisePropertyChanged(() => Bytes); }
		}

		public string Category
		{
			get => _category;
			set => SetProperty(ref _category, value);
		}

		public string ProvinceCity
		{
			get => _provinceCity;
			set => SetProperty(ref _provinceCity, value);
		}

		public string SelectedImage
		{
			get => _selectedImage;
			set => SetProperty(ref _selectedImage, value);
		}

		public bool VisibleSelectedImage
		{
			get => _visibleSelectedImage;
			set => SetProperty(ref _visibleSelectedImage, value);
		}

		public List<PostImage> PostImages
		{
			get => _postImages;
			set
			{
				_postImages = value;
				VisibleSelectedImage = (_postImages.Count != 0);
				InitSelectedImage();
			}
		}

		public bool IsSubmitBtnEnabled
		{
			get => _isSubmitBtnEnabled;
			set => SetProperty(ref _isSubmitBtnEnabled, value);
		}

		public string PostDescriptionPlaceHolder => Strings["CreatePostDescriptionPlaceHolder"];
		public string PostTitlePlaceHolder => Strings["CreatePostTitlePlaceHolder"];
		public string BtnSubmitTitle { get; set; } = Strings["CreatePostBtnSubmitTitle"];
		public string BtnCancelTitle => Strings["CancelTitle"];

		private const string SelectedPictures = "SelectedPictures";
		private const string Selected1Picture = "Selected1Picture";

		public CreatePostViewModel(IMvxPictureChooserTask pictureChooserTask, IDataModel dataModel, ILoadingOverlayService loadingOverlayService)
		{
			_overlay = loadingOverlayService;
			_debouncer = new DebouncerHelper();
			_dataModel = dataModel;
			_pictureChooserTask = pictureChooserTask;
			_selectedCategory = _dataModel.Categories.FirstOrDefault(category => category.CategoryName.Translate().Equals(Strings["DefaultCategoryCreatePostName"]));
			_selectedProvinceCity = _selectedProvinceCity ?? _dataModel.ProvinceCities.First(p => p.ProvinceCityName.Translate().Equals(Strings["DefaultLocationFilter"]));
		}

		private async void BackPressed()
		{
			if (_viewModelMode == ViewMode.EditPost)
			{
				var result = await NavigationService.Navigate<PopupMessageViewModel, string, RequestStatus>(Strings["CancelEditPostConfirmMessage"]);
				if (result == RequestStatus.Submitted)
				{
					await NavigationService.Close(this, false);
				}
			}
			else
			{
				var result = await NavigationService.Navigate<PopupMessageViewModel, string, RequestStatus>(Strings["DeleteConfirmationMessage"]);
				if (result == RequestStatus.Submitted)
				{
					await NavigationService.Close(this, false);
				}
			}
		}

		private async void ShowCategoriesPopup()
		{
			var result = await NavigationService.Navigate<PopupListViewModel, PopupListParam, string>(new PopupListParam
			{
				Title = Strings["PopupCategoriesTitle"],
				Items = _dataModel.Categories.Select(c => c.CategoryName.Translate()).Skip(1).ToList(),
				SelectedItem = _selectedCategory.CategoryName.Translate()
			});

			if (string.IsNullOrEmpty(result))
			{
				return;
			}

			_selectedCategory = _dataModel.Categories.First(c => c.CategoryName.Translate() == result);
			Category = result;
		}

		private async void ShowLocationFiltersPopup()
		{
			var result = await NavigationService.Navigate<PopupListViewModel, PopupListParam, string>(new PopupListParam
			{
				Title = Strings["PopupLocationFiltersTitle"],
				Items = _dataModel.ProvinceCities.Select(c => c.ProvinceCityName.Translate()).ToList(),
				SelectedItem = _selectedProvinceCity.ProvinceCityName.Translate()
			});

			if (string.IsNullOrEmpty(result))
			{
				return;
			}

			_selectedProvinceCity = _dataModel.ProvinceCities.First(c => c.ProvinceCityName.Translate() == result);
			ProvinceCity = result;
		}

		private async Task ShowPhotoCollection()
		{
				PostImages =
					await NavigationService.Navigate<PhotoCollectionViewModel, List<PostImage>, List<PostImage>>(
						PostImages);
		}

		private void DoTakePicture()
		{
			PermissionHelper.GrandPermission(new List<Permission> { Permission.Camera, Permission.Storage }, () => _debouncer.Debouce(() =>
			{
				InvokeOnMainThread(() =>
				{
					_pictureChooserTask.TakePicture(AppConstants.ImageMaxSize, AppConstants.ImagePercentQuality, OnPicture, () => { });
				});
			}));
		}

		private void OnPicture(Stream pictureStream)
		{
			var imageBytes = new List<byte[]>();
			var memoryStream = new MemoryStream();
			pictureStream.CopyTo(memoryStream);
			Bytes = memoryStream.ToArray();
			imageBytes.Add(Bytes);
			InitNewImage(imageBytes);
		}

		public async void InitNewImage(List<byte[]> imageByte)
		{
			if (imageByte == null)
			{
				await Task.Delay(100);
				await _overlay.ShowOverlay(Strings["ImageProcessing"]);
			}
			else
			{
				foreach (var img in imageByte)
				{
					var image = new PostImage()
					{
						ImageData = JsonHelper.ConvertToBase64String(img),
						ViewMode = ViewMode.CreatePost,
					};
					PostImages.Add(image);
				}

				VisibleSelectedImage = (_postImages.Count != 0);
				InitSelectedImage();
				await _overlay.CloseOverlay();
			}
		}

		public async Task InitSubmit()
		{
			var success = false;
			try
			{
				await _overlay.ShowOverlay(Strings["UpdateOverLayTitle"]);
				if (_viewModelMode == ViewMode.EditPost)
				{
					await InitEditPost();
				}
				else
				{
					await InitCreateNewPost();
				}
				success = true;
			}
			catch (AppException.ApiException)
			{
				await NavigationService.Navigate<PopupWarningViewModel, string, bool>(Strings["ErrorConnectionMessage"]);
			}
			finally
			{
				await _overlay.CloseOverlay(777);
				if (success)
				{
					await NavigationService.Close(this, true);
				}
			}
		}

		public async Task InitCreateNewPost()
		{
			await _overlay.ShowOverlay(Strings["UploadDataOverLayTitle"]);
			var post = new CreatePost()
			{
				Title = PostTitle,
				Description = PostDescription,
				PostImages = _postImages,
				PostCategory = (_selectedCategory.CategoryName.Translate() == Strings["DefaultCategoryCreatePostName"])
					? AppConstants.DefaultCategoryCreatePostId
					: _selectedCategory.Id,
				Address = _selectedProvinceCity.Id,
			};
			await ManagementService.CreatePost(post, _dataModel.LoginResponse.Token);
		}

		public async Task InitEditPost()
		{
			var post = new EditPost()
			{
				PostId = _postId,
				Title = PostTitle,
				Description = PostDescription,
				PostImages = _postImages,
				PostCategory = (_selectedCategory.CategoryName.Translate() == Strings["DefaultCategoryCreatePostName"]) ? AppConstants.DefaultCategoryCreatePostId : _selectedCategory.Id,
				Address = _selectedProvinceCity.Id,
			};
			await ManagementService.EditPost(post, _postId, _dataModel.LoginResponse.Token);
		}

		private void InitSelectedImage()
		{
			SelectedImage = PostImages.Count == 1 ? Selected1Picture.Translate() : string.Format(SelectedPictures.Translate(), PostImages.Count);
		}

		public void UpdateSubmitBtn() => IsSubmitBtnEnabled = !string.IsNullOrEmpty(_postTitle) && !string.IsNullOrEmpty(_postDescription);

		public override void Prepare(ViewMode mode)
		{
			if (mode != ViewMode.EditPost)
			{
				return;
			};
			_viewModelMode = ViewMode.EditPost;
			_postId = _dataModel.CurrentPost.PostId;
			_selectedCategory = _dataModel.CurrentPost.Category;
			_selectedProvinceCity = _dataModel.CurrentPost.ProvinceCity;
			_postTitle = _dataModel.CurrentPost.Title;
			_postDescription = _dataModel.CurrentPost.Description;
			IsSubmitBtnEnabled = true;
			BtnSubmitTitle = Strings["SaveAPost"];
			Category = _selectedCategory.CategoryName.Translate();
		}

		public override async void ViewAppearing()
		{
			base.ViewAppearing();

			if (_viewModelMode == ViewMode.EditPost && _isLoadFirstTime)
			{
				await LoadCurrentPostDataWithOverlay(Strings["LoadingDataOverlayTitle"]);
			}

			_isLoadFirstTime = false;
		}

		private void EditImageList()
		{
			var imageList = _dataModel.CurrentPost.Images;
			foreach (var img in imageList)
			{
				var image = new PostImage()
				{
					ImageData = img.OriginalImage,
					ViewMode = ViewMode.EditPost,
				};
				PostImages.Add(image);
			}
			VisibleSelectedImage = (_postImages.Count != 0);
			InitSelectedImage();
		}

		private async Task LoadCurrentPostDataWithOverlay(string overlayTitle)
		{
			try
			{
				await _overlay.ShowOverlay(overlayTitle);
				EditImageList();
			}
			catch (AppException.ApiException)
			{
				await NavigationService.Navigate<PopupWarningViewModel, string, bool>(Strings["ErrorConnectionMessage"]);
				await Task.Delay(777);//for iphone
				await LoadCurrentPostDataWithOverlay(overlayTitle);
			}
			finally
			{
				await _overlay.CloseOverlay();
			}
		}
	}
}
