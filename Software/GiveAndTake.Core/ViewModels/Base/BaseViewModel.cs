﻿using GiveAndTake.Core.Services;
using MvvmCross;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using System.Threading.Tasks;
using GiveAndTake.Core.Models;
using I18NPortable;
using System;
using System.Reflection;

namespace GiveAndTake.Core.ViewModels.Base
{
	public abstract class BaseViewModel : MvxViewModel
	{
		private const string ToastText = "ToastText";

		private IMvxNavigationService _navigationService;
		public override IMvxNavigationService NavigationService => _navigationService ?? (_navigationService = Mvx.Resolve<IMvxNavigationService>());

		private IManagementService _managementService;
		public IManagementService ManagementService => _managementService ?? (_managementService = Mvx.Resolve<IManagementService>());

		private IAppDataLoader _appDataLoader;
		public IAppDataLoader AppDataLoader => _appDataLoader ?? (_appDataLoader = Mvx.Resolve<IAppDataLoader>());

		private IDataModel _dataModel;
		public IDataModel DataModel => _dataModel ?? (_dataModel = Mvx.Resolve<IDataModel>());

		public static II18N Strings => I18N.Current;

		public override void ViewAppeared()
		{
			base.ViewAppeared();
			DataModel.LanguageUpdated += OnLanguageChanged;
		}

		public override void ViewDisappeared()
		{
			base.ViewDisappeared();
			DataModel.LanguageUpdated -= OnLanguageChanged;
		}

		public virtual void OnActive()
		{
			
		}

		public virtual void OnDeactive()
		{
			
		}

		protected virtual void OnLanguageChanged(object sender, EventArgs e)
		{
			var properties = GetType().GetRuntimeProperties();

			foreach (var property in properties)
			{
				if (property.PropertyType == typeof(string) && property.Name != ToastText)
				{
					RaisePropertyChanged(property.Name);
				}
			}
		}
	}

	public abstract class BaseViewModel<TParameter> : BaseViewModel, IMvxViewModel<TParameter>
	{
		public abstract void Prepare(TParameter parameter);
	}

	public abstract class BaseViewModelResult<TResult> : BaseViewModel, IMvxViewModelResult<TResult>
	{
		public TaskCompletionSource<object> CloseCompletionSource { get; set; }

		public override void ViewDestroy(bool viewFinishing = true)
		{
			if (viewFinishing && CloseCompletionSource != null && !CloseCompletionSource.Task.IsCompleted && !CloseCompletionSource.Task.IsFaulted)
				CloseCompletionSource?.TrySetCanceled();

			base.ViewDestroy(viewFinishing);
		}
	}
	
	public abstract class BaseViewModel<TParameter, TResult> : BaseViewModelResult<TResult>, IMvxViewModel<TParameter, TResult>
	{
		public abstract void Prepare(TParameter parameter);

		public override void ViewDestroy(bool viewFinishing = true)
		{
			if (viewFinishing && CloseCompletionSource != null && !CloseCompletionSource.Task.IsCompleted && !CloseCompletionSource.Task.IsFaulted)
				CloseCompletionSource?.TrySetCanceled();

			base.ViewDestroy(viewFinishing);
		}
	}
}