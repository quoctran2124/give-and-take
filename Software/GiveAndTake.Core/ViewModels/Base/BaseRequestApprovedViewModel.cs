﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FFImageLoading.Transformations;
using FFImageLoading.Work;
using GiveAndTake.Core.Models;
using MvvmCross.Commands;

namespace GiveAndTake.Core.ViewModels.Base
{
	public class BaseRequestApprovedViewModel : BaseViewModel<Request, PopupRequestStatusResult>
	{
		public string MyRequestPopupTitle => Strings["RequestApprovalTitle"];
		public IMvxCommand CloseCommand => _closeCommand ?? (_closeCommand = new MvxCommand(HandleOnClosed));
		public List<ITransformation> PostTransformations => new List<ITransformation> { new CornersTransformation(15, CornerTransformType.AllRounded) };
		public List<ITransformation> AvatarTransformations => new List<ITransformation> { new CircleTransformation() };
		public IMvxCommand ShowTakerProfileCommand => _showTakerProfileCommand ?? (_showTakerProfileCommand = new MvxCommand(HandleOnShowTakerProfile));
		public IMvxCommand ShowGiverProfileCommand => _showGiverProfileCommand ?? (_showGiverProfileCommand = new MvxCommand(HandleOnShowGiverProfile));
		public IMvxCommand ImgPostClickedCommand => _showPostDetailCommand ?? (_showPostDetailCommand = new MvxCommand(HandleOnShowPostDetail));

		private string _imgPostUrl;
		private string _takerAvatarUrl;
		private string _takerFullName;
		private string _requestCreatedDate;
		private string _requestMessage;

		private string _giverAvatarUrl;
		private string _giverFullName;
		private string _postCreatedDate;
		private string _approvedMessage;

		private Request _request;
		private IMvxCommand _closeCommand;
		private IMvxCommand _showTakerProfileCommand;
		private IMvxCommand _showGiverProfileCommand;
		private IMvxCommand _showPostDetailCommand;

		public string ImgPostUrl
		{
			get => _imgPostUrl;
			set => SetProperty(ref _imgPostUrl, value);
		}
		public string TakerAvatarUrl
		{
			get => _takerAvatarUrl;
			set => SetProperty(ref _takerAvatarUrl, value);
		}
		public string TakerFullName
		{
			get => _takerFullName;
			set => SetProperty(ref _takerFullName, value);
		}
		public string RequestCreatedDate
		{
			get => _requestCreatedDate;
			set => SetProperty(ref _requestCreatedDate, value);
		}
		public string RequestMessage
		{
			get => _requestMessage;
			set => SetProperty(ref _requestMessage, value);
		}
		public string GiverAvatarUrl
		{
			get => _giverAvatarUrl;
			set => SetProperty(ref _giverAvatarUrl, value);
		}
		public string GiverFullName
		{
			get => _giverFullName;
			set => SetProperty(ref _giverFullName, value);
		}
		public string PostCreatedDate
		{
			get => _postCreatedDate;
			set => SetProperty(ref _postCreatedDate, value);
		}
		public string ApprovedMessage
		{
			get => _approvedMessage;
			set => SetProperty(ref _approvedMessage, value);
		}

		public override void Prepare(Request request)
		{
			_request = request;
		}

		public override Task Initialize()
		{
			ImgPostUrl = _request.Post.Images != null && _request.Post.Images.Any() ? _request.Post.Images.ElementAt(0).OriginalImage : "";
			TakerAvatarUrl = _request.User.AvatarUrl;
			TakerFullName  = _request.User.DisplayName;
			RequestCreatedDate = _request.CreatedTime.ToString(AppConstants.DateStringFormat);
			RequestMessage = _request.RequestMessage;

			GiverAvatarUrl = _request.Post.User.AvatarUrl;
			GiverFullName = _request.Post.User.DisplayName;
			PostCreatedDate = _request.Response.CreatedTime.ToString(AppConstants.DateStringFormat);
			ApprovedMessage = _request.Response.ResponseMessage;

			return base.Initialize();
		}

		private void HandleOnClosed() => NavigationService.Close(this, PopupRequestStatusResult.Cancelled);
		private void HandleOnShowTakerProfile() => NavigationService.Close(this, PopupRequestStatusResult.ShowTakerProfile);
		private void HandleOnShowGiverProfile() => NavigationService.Close(this, PopupRequestStatusResult.ShowGiverProfile);
		private void HandleOnShowPostDetail() => NavigationService.Close(this, PopupRequestStatusResult.ShowPostDetail);
	}
}
