﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;
using GiveAndTake.Core.Models;
using GiveAndTake.Core.ViewModels.Base;
using GiveAndTake.Core.ViewModels.Popup;
using I18NPortable;
using MvvmCross;
using MvvmCross.Commands;

namespace GiveAndTake.Core.ViewModels
{
	public class SelectLanguageViewModel : BaseViewModel
	{

		public SelectLanguageViewModel()
		{
			IsVietnamese = true;
		}

		public string SelectLanguageVnText => AppConstants.SelectLanguageVnText;
		public string SelectLanguageEnText => AppConstants.SelectLanguageEnText;
		public string VnTextTitle => AppConstants.VnTextTitle;
		public string EnTextTitle => AppConstants.EnTextTitle;
		private bool _isVietnamese;
		private bool _isEnglish;

		public bool IsVietnamese
		{
			get => _isVietnamese;
			set => SetProperty(ref _isVietnamese, value);
		}
		public bool IsEnglish
		{
			get => _isEnglish;
			set => SetProperty(ref _isEnglish, value);
		}

		private IMvxCommand _submmitCommand;
		private IMvxCommand _vietnameseCommand;
		private IMvxCommand _englishCommand;


		public IMvxCommand SubmitCommand => _submmitCommand ?? (_submmitCommand = new MvxCommand(OnSubmitCommand));
		public IMvxCommand VietnameseCommand =>_vietnameseCommand ?? (_vietnameseCommand = new MvxCommand(OnVietnameseCommand));
		public IMvxCommand EnglishCommand => _englishCommand ?? (_englishCommand = new MvxCommand(OnEnglishCommand));

		private void OnVietnameseCommand()
		{
			IsVietnamese = true;
		}

		private void OnEnglishCommand()
		{
			IsVietnamese = false;
		}

		private void OnSubmitCommand()
		{
			ChangeLanguage(IsVietnamese ? LanguageType.vi : LanguageType.en);
		}

		private void ChangeLanguage(LanguageType language)
		{
			var appDataLoader = Mvx.Resolve<IAppDataLoader>();
			var appData = appDataLoader.AppData;
			appData.IsNotFirstStartApp = true;
			appDataLoader.SaveData(appData);

			I18N.Current.Locale = language.ToString();
			DataModel.Language = language;
			AppDataLoader.UpdateLanguage(language);
			DataModel.RaiseLanguageUpdated();

			NavigationService.Navigate<LoginViewModel>();
			NavigationService.Close(this);
		}
	}
}
