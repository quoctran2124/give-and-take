﻿using System;

namespace GiveAndTake.Core.Exceptions
{
	public class AppException
	{
		public class ApiException : Exception
		{
			public ApiException(string message) : base(message) { }
		}

		public class ApiServiceUnavailableException : ApiException
		{
			public ApiServiceUnavailableException(string message) : base(message) { }
		}
	}
	
}
