﻿namespace GiveAndTake.Core
{
	public enum FontType
	{
		Light,
		Regular,
		Medium,
		Bold,
	}
	public enum RequestMethod
	{
		Get,
		Put,
		Post,
		Delete
	}

	public enum NetworkStatus
	{
		Success,
		NoWifi,
		Timeout,
		Exception
	}

	public enum PopupListType
	{
		FilterType,
		MenuType
	}

	public enum RequestStatus
	{
		Pending,
		Approved,
		Received,
		Rejected,
		Cancelled,
		Submitted
	}
	public enum PostStatus
	{
		Giving,
		Gave,
		Deleted
	}

	public enum PopupRequestStatusResult
	{
		Cancelled,
		Removed,
		Rejected,
		Approved,
		Received,
		ShowPostDetail,
		ShowTakerProfile,
		ShowGiverProfile
	}

	public enum ListViewItemType
	{
		MyPosts,
		MyRequests
	}

	public enum ViewMode
	{
		CreatePost,
		EditPost,
	}

	public enum PopupReason
	{
	    InappropriateContentReason,
    	InappropriatePictureReason, 
    	FakeContentReason, 
    	OtherReason,
    }
    
	public enum ProfileReloadType
	{
		MyPost,
		MyRequestPost
	}

	public enum BottomTabBar
	{
		Home = 0,
		Notification = 1,
		Profile = 2
	}

	public enum LanguageType
	{
		vi,
		en
	}
}