using System;
using System.Drawing;

namespace GiveAndTake.Core
{
	// REVIEW [KHOA]: all texts should get from locale files -> support multi-language later if needed
	public static class AppConstants
	{
		public const string AppTitle = "Cho và Nhận";

#if DEVELOPMENT
		public const string ApiUrl = "https://api.dev.chovanhan.asia/api/v1/";
#else
		public const string ApiUrl = "https://api.chovanhan.asia/api/v1/";
#endif

		public const int ApiTimeout = 300; // seconds
		public const int ApiVersion = 1; 

		//App Url
		public const string PlayStoreUrl = "https://play.google.com/store/apps/details?id=com.sioux.giveandtake";
		public const string AppStoreUrl = "itms://itunes.apple.com/de/app/id1444250710";

		//API Url Path
		public const string GetCategories = "categories/app/list";
		public const string GetPostList = "post/app/list";
		public const string GetMyPostList = "post/app/listPostOfUser";
		public const string GetPostDetail = "post/app/detail";
		public const string GetPostOfUser = "post/app/listPostOfUser";
		public const string GetMyRequestedPosts = "post/app/listRequestedPostOfUser";
		public const string ChangeStatusOfPost = "post/status";
		public const string EditPost = "post/app/update";
		public const string LoginFacebook = "user/login/facebook";
		public const string LogoutApp = "user/logout";
		public const string CreatePost = "post/app/create";
		public const string CreateRequest = "request/create";
		public const string CheckUserRequest = "request/checkUserRequest";
		public const string CancelUserRequest = "request/deleteCurrentUserRequest";
		public const string GetUserProfile = "user";
		public const string BlockUserUrl = "user/block";	
		public const string GetProvinceCities = "provincecity/list";
		public const string GetRequestOfPost = "request/list";
		public const string GetRequestById = "request/getRequestById";
		public const string GetRequestOfCurrentUserByPostId = "request/getRequestOfCurrentUserByPostId";
		public const string ChangeStatusOfRequest = "request/status";
		public const string CheckIfRequestProcessed = "request/checkIfRequestProcessed";
		public const string GetNotificationList = "notification/list";
		public const string UpdateReadStatus = "notification/updateReadStatus";
		public const string GetResponseById = "response/getResponseById";
		public const string RegisterPushNotificationUserInformation = "deviceIdentity/registerDevice";
		public const string GcmNotificationKey = "gcm.notification.notification";
		public const string GcmBadgeKey = "gcm.notification.aps";
		public const string UpdateSeenStatus = "notification/updateSeenStatus";
		public const string GetBadgeFromServer = "notification/getUnSeenNotificationNumber";
		public const string DeletePushNotificationToken = "deviceIdentity/delete";
		public const string AppreciateAPost = "post/app/appreciate";
		public static string CreateResponse = "response/create";
		public const string CreatePostReport = "report/createReport";

		//Token
		public const string Token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6ImFkbWluIiwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvc2lkIjoiOWM4NzIxMGEtM2E2ZC00MGM5LTgwMmItOGRkZWVhN2RlMDU3IiwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvbmFtZSI6IkFkbWluIEFkbWluIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9yb2xlIjpbIlVzZXIiLCJBZG1pbiJdLCJuYmYiOjE1MzY2MzM4MTYsImV4cCI6MTUzOTIyNTgxNiwiaXNzIjoiR2l2ZWF3YXkiLCJhdWQiOiJFdmVyeW9uZSJ9.2Jz4t5mnrhXSbr93gtVtSjDdI9nXB412-uwN40xd-aU";
		public const string ApiKey = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6ImFkbWluIiwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvc2lkIjoiMjMwMzc5MmItZGI1MC00YzlhLTk3MjAtN2JkMWVjN2QzM2U4IiwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvbmFtZSI6IkFkbWluIEFkbWluIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9yb2xlIjpbIkFkbWluIiwiVXNlciJdLCJuYmYiOjE1MzM4ODY3MzMsImV4cCI6MTUzNjQ3ODczMywiaXNzIjoiR2l2ZWF3YXkiLCJhdWQiOiJFdmVyeW9uZSJ9.2A8md3WFGYT-w2Kz0OMAUAj7e20L-wTxIfKL4KyEOko";

		//Navigation Tab
		public const string HomeTab = "Home";
		public const string NotificationTab = "Notification";
		public const string ConversationTab = "Conversation";
		public const string ProfileTab = "Profile";
		public const int ProfileTabIndex = 2;
		
		public static int NumberOfRequestPerPage = 20;
		public static int NumOfFragmentViewPager = 3;

		public const string DateStringFormat = "dd.MM.yyyy";

		public static string SupportContactValue = "(0236)1022";
		public static string SupportContactPhone = "02361022";
		public const string DefaultFeedbackEmail = "chovanhan.team@gmail.com";

		public static string DefaultPostImage = "default_post";
		public static string NotFound = "Not Found";

		public const string DefaultUserName = "username";

		public const string GiveAndTakeHashtag = "#Chovanhan";

		public const int ImageMaxSize = 3840;
		public const int ImagePercentQuality = 50;

		public const string SelectLanguageVnText = "Lựa chọn ngôn ngữ";
		public const string SelectLanguageEnText = "(Select your language)";
		public const string VnTextTitle = "Tiếng Việt";
		public const string EnTextTitle = "English";

		public const string DefaultCategoryCreatePostId = "0c109358-fae2-42bd-b2f3-45cbe98a5dbd";

		public static string UserStatusBlocked = "Blocked";

		public static string WarningBlockedUserMessageVn = "<h style=\"text-align: center;\">Tài khoản của bạn đã bị khóa! " +
														 "<br/>" +
														 "Xin vui lòng liên hệ email: " +
														 "<br/>" +
														 "<a href=\"temp.com\">chovanhan.team@gmail.com</a> " +
														 "<br/>" +
														 "để biết thêm thông tin chi tiết!</h>";
		public static string WarningBlockedUserMessageEn = "<h style=\"text-align: center;\">Your account has been locked! " +
		                                                   "<br/>" +
														   "Please contact email: " +
		                                                   "<br/>" +
		                                                   "<a href=\"temp.com\">chovanhan.team@gmail.com</a> " +
		                                                   "<br/>" +
														   "for more details!</h>";

		public const string ShareFacebookLink = "http://chovanhan.asia/applinks/";
		public const string PolicyWebPageLink = "http://chovanhan.asia/chinh-sach-bao-mat/";

		public const string AcceptTermAndConditionInHtmlVn = "Bằng việc đăng nhập bạn đã đồng ý" +
															"<br/>" +
															"<a href =\"./\">điều khoản và điều kiện</a> " +
															"của chúng tôi";

		public const string AcceptTermAndConditionInHtmlEn = "By logging in you agree to " +
		                                                     "<br/>" +
		                                                     "<a href =\"./\">our terms and conditions</a> ";

		public const string TermAndConditionContentInHtmlVn = "<div>Chào mừng bạn đến với " +
			"<font color='#3fb8ea'><b>Cho và Nhận</b></font> " +
			"<br/>" + "<br/>" +

			"Các Điều khoản này sẽ giúp điều chỉnh việc bạn sử dụng ứng dụng Cho và Nhận " +
			"một cách đúng đắn và phù hợp, vì vậy vui lòng đọc kỹ các điều khoản sử dụng này trước khi sử dụng. " +
			"Một khi bạn đã đăng nhập vào ứng dụng, điều đó sẽ được hiểu là bạn đã đồng ý bị ràng buộc bởi các Điều khoản, Chính sách bảo mật của ứng dụng Cho và Nhận " +
			". Nếu bạn không đồng ý với tất cả các điều khoản và điều kiện của thỏa thuận này, bạn không nên và không được phép sử dụng ứng dụng Cho và Nhận. " +
			"Thỏa thuận này áp dụng cho tất cả người dùng sử dụng ứng dụng và không có trường hợp ngoại lệ. " +
			"<br/>" + "<br/>" +

			"<font color='#3fb8ea'><b>ỨNG DỤNG CHO VÀ NHẬN LÀ GÌ?</font></b> " +
			"<br/>" + "<br/>" +
			"Cho và Nhận là một ứng dụng phi lợi nhuận nơi người dùng có thể chia sẻ miễn phí các vật phẩm tới cộng đồng, " +
			"đồng thời có thể nhận được những thứ mà mình đang tìm kiếm. Ứng dụng cho phép người dùng đồng thời cho tặng vật phẩm và đề nghị " +
			"nhận vật phẩm từ người dùng khác hoàn toàn miễn phí mà không có bất kỳ các hoạt động tài chính như mua bán trao đổi bằng tiền." +
			"<br/>" + "<br/>" +

			"<font color='#3fb8ea'><b>MỤC ĐÍCH CỦA ỨNG DỤNG:</font></b> " +
			"<br/>" + "<br/>" +
			"Sứ mệnh của Cho và Nhận " +
			" là mang lại cho mọi người khả năng xây dựng cộng đồng chia sẻ, giúp đỡ lẫn nhau và đưa mọi người đến gần nhau hơn thông qua các hành động " +
			" cho đi và nhận lại. Nhằm thúc đẩy sứ mệnh này, ứng dụng Cho và Nhận được xây dựng với mục đích:" +
			"<br/>" + "<br/>" +
			"Tổng hợp các thông tin cho và nhận: Ứng dụng sẽ gửi các thông báo về việc trao tặng những vật phẩm còn giá trị sử dụng cho những người cần nó." +
			"<br/>" + "<br/>" +
			"Kết nối các nhà hảo tâm: Là nơi để các tổ chức từ thiện, các nhà hảo tâm có thể gặp gỡ nhau, cùng tổ chức các chương trình thiện nguyện." +
			"<br/>" + "<br/>" +
			"Giúp đỡ những người khó khăn: Thông qua ứng dụng, việc chuyển tặng những món quà quý giá, hữu ích và cần thiết đến những người có hoàn cảnh khó khăn sẽ trở nên dễ dàng hơn." +
			"<br/>" + "<br/>" +
			"Nâng cao ý thức cộng đồng: Khuyến khích, ghi nhận tinh thần hỗ trợ, giúp đỡ giữa các cá nhân và tổ chức trong cộng đồng, thúc đẩy nhận thức, xây dựng nên một cộng đồng tương thân tương ái, phát triển xã hội nhân văn hơn, tình người hơn." +
			"<br/>" + "<br/>" +

			"<font color='#3fb8ea'><b>TÀI KHOẢN NGƯỜI DÙNG CỦA BẠN</font></b> " +
			"<br/>" + "<br/>" +
			"<b>Đăng ký tài khoản người dùng:</b>" +
			"<br/>" + "<br/>" +
			"Bạn có thể tìm hiểu các thông tin về ứng dụng ở website chính thức của ứng dụng mà không cần có tài khoản nhưng bạn cần phải có tài khoản " +
			"để đăng nhập và trao đổi thông tin cho và nhận trên ứng dụng. Chúng tôi có thể cho phép bạn đăng ký ứng dụng Cho và Nhận bằng các dịch vụ " +
			"của bên thứ ba như Facebook. Nếu bạn sử dụng dịch vụ của bên thứ ba để đăng ký tài khoản, bạn có thể được yêu cầu cung cấp thông tin đăng " +
			"nhập của mình cho dịch vụ bên thứ ba đó. Chúng tôi không ghi nhận hoặc lưu trữ mật khẩu cho các dịch vụ bên thứ ba như vậy. Vui lòng xem " +
			"lại các thông báo bảo mật và cài đặt của bạn cho dịch vụ bên thứ ba đó trước khi sử dụng để đăng nhập vào ứng dụng Cho và Nhận. Tài khoản " +
			"của bạn là dành riêng cho bạn và bạn không được chia sẻ hoặc cho phép bất kỳ người nào khác sử dụng tài khoản của bạn. Bạn không được sử " +
			"dụng tài khoản của người khác, tạo tài khoản cho người khác, hoặc tạo và sử dụng tài khoản để mạo danh người khác. Bạn sẽ phải chịu trách " +
			"nhiệm cho tất cả các hoạt động phát sinh từ tài khoản của bạn." +
			"<br/>" + "<br/>" +
			"Ngoài ra, ứng dụng Cho và Nhận cũng hỗ trợ tính năng đăng nhập tự động sau khi bạn đăng nhập lần đầu tiên và bạn chỉ có thể đăng nhập bằng " +
			"tài khoản khác sau khi đăng xuất tài khoản của mình khỏi ứng dụng. Bạn phải tự chịu trách nhiệm cho tất cả các thiệt hại do để xảy ra các " +
			"truy cập trái phép vào ứng dụng từ tài khoản của bạn. Bạn phải thông báo ngay cho chúng tôi nếu bạn biết hoặc có lý do để nghi ngờ về bất " +
			"kỳ hoạt động sử dụng trái phép nào đó từ tài khoản của bạn." +
			"<br/>" + "<br/>" +

			"<b>Chính sách đại diện quyền lợi người dùng – cam kết của bạn với chúng tôi:</b>" +
			"<br/>" + "<br/>" +
			"Bạn phải bảo đảm rằng: " +
			"<b>(1)</b> tất cả thông tin bạn cung cấp cho chúng tôi là trung thực, chính xác, đầy đủ và sẽ luôn trung thực, chính xác và đầy đủ; " +
			"<b>(2)</b> bạn sẽ tuân thủ các điều khoản và điều kiện của Thỏa thuận này và mọi thỏa thuận khác mà bạn là đối tượng liên quan đến việc " +
			"sử dụng ứng dụng Cho và Nhận từ tài khoản của bạn; " +
			"<b>(3)</b> bạn sẽ không sử dụng hoặc truy cập ứng dụng cho các mục đích lừa đảo, chiếm đoạt tài sản; và " +
			"<b>(4)</b> quyền truy cập, sử dụng tài khoản ứng dụng của bạn sẽ không tạo ra các hành vi vi phạm bất kỳ thỏa thuận, hợp đồng, điều khoản sử " +
			"dụng nào khác hoặc bất kỳ luật lệ và quy định nào khác mà bạn phải tuân theo. " +
			"<b>(5)</b> Bạn phải luôn tuân thủ pháp luật và các quy định của chính " +
			"phủ sở tại trong việc trao đổi thông tin vật phẩm và giữ thuần phong mỹ tục, phù hợp với đạo đức con người trong các hoạt động đăng tải hình " +
			"ảnh, thông tin và trao đổi vật phẩm được diễn ra trên ứng dụng Cho và Nhận. " +
			"<b>(6)</b> Bạn sẽ không trao tặng những vật phẩm có thể gây nguy hiểm cho mọi người hay cố ý thực hiện hành vi gây hại người khác. " +
			"<b>(7)</b> Chúng tôi có thể giới hạn khả năng sử dụng ứng dụng của bạn, bao gồm việc " +
			"gửi các cảnh báo vi phạm khi có những báo cáo phản hồi từ phía những người dùng khác về các hành vi không phù hợp và không đúng đắn của bạn " +
			"khi thực hiện đăng tải các thông tin về hình ảnh, nội dung phản cảm, phản động, chống phá chính phủ, không phù hợp với thuần phong mỹ tục " +
			"và đạo đức con người; ngoài ra chúng tôi có thể đơn phương thực hiện đình chỉ và khóa tài khoản ứng dụng của bạn mà không cần báo trước nếu " +
			"như bạn vi phạm quá nhiều lần hoặc mức độ hành vi vi phạm quá lớn hoặc trong các trường hợp nếu chúng tôi có cơ sở hợp lý để nghi ngờ rằng " +
			"thông tin mà bạn cung cấp là không đúng sự thật, không chính xác, lỗi thời, không đầy đủ hoặc vi phạm pháp luật." +
			"<br/>" + "<br/>" +

			"<font color='#3fb8ea'><b>CÁCH THỨC SỬ DỤNG ỨNG DỤNG</font></b> " +
			"<br/>" + "<br/>" +
			"<b>Khi bạn là người cho tặng vật phẩm:</b>" +
			"<br/>" + "<br/>" +
			"1. Khi bạn quyết định trao tặng một vật phẩm trên Cho và Nhận, bạn sẽ đăng bài viết về vật phẩm, trong đó mô tả chi tiết về vật " +
			"phẩm (hình ảnh, thể loại, tình trạng sử dụng, địa điểm giao nhận…)" +
			"<br/>" + "<br/>" +
			"2. Bạn sẽ nhận được các lời đề nghị nhận vật phẩm và bạn sẽ xét duyệt các lời đề nghị này." +
			"<br/>" + "<br/>" +
			"3. Bạn thực hiện xác nhận đồng ý với đối với đề nghị mà bạn cho rằng phù hợp và muốn trao tặng vật phẩm hoặc từ chối các đề nghị chưa phù hợp." +
			"<br/>" + "<br/>" +

			"<b>Khi bạn là người muốn nhận vật phẩm:</b>" +
			"<br/>" + "<br/>" +
			"1. Khi bạn muốn nhận một vật phẩm, bạn sẽ vào ứng dụng để tìm kiếm vật phẩm mong muốn dựa vào hình ảnh, thể loại, tình trạng sử dụng… để xác định " +
			"vật phẩm phù hợp." +
			"<br/>" + "<br/>" +
			"2. Sau khi tìm thấy các vật phẩm mong muốn, bạn sẽ vào bài đăng của chủ sở hữu vật phẩm và gửi đề nghị được nhận vật phẩm cho người đăng tin." +
			"<br/>" + "<br/>" +
			"3. Nếu người đăng tin đồng ý lời đề nghị của bạn, bạn sẽ tiến hành trao đổi thông tin về cách thức nhận vật phẩm (trao đổi chi tiết về thời " +
			"gian, địa điểm, cách thức nhận vật phẩm với chủ sở hữu)." +
			"<br/>" + "<br/>" +

			"<font color='#3fb8ea'><b>QUYỀN HẠN VÀ NGHĨA VỤ CỦA CHÚNG TÔI</font></b> " +
			"<br/>" + "<br/>" +
			"Chúng tôi không cung cấp các cơ sở hay kho lưu trữ các vật phẩm, các hoạt động trao tặng đều được tiến hành thông qua các trao đổi thông tin cá nhân " +
			"giữa người cho và người nhận." +
			"<br/>" + "<br/>" +
			"Chúng tôi không thu bất cứ chi phí nào liên quan đến các hoạt động trao tặng và phát sinh bởi vì đây là một dịch vụ phi lợi nhuận, phục vụ cộng đồng " +
			"và xã hội; do đó, tất cả các hành động liên quan đến tài chính như thu phí, tính phí dịch vụ từ các cá nhân, tổ chức không phải là nhà quản lý ứng " +
			"dụng Cho và Nhận hoặc mạo danh ứng dụng Cho và Nhận sẽ được coi là vi phạm pháp luật và có thể dẫn đến việc truy cứu trách nhiệm hình sự và dân sự. " +
			"Bạn có thể báo cáo và gửi các phản hồi nếu phát hiện các hoạt động không hợp pháp này cho chúng tôi qua email: " +
			"<b>chovanhan.team@gmail.com.</b>" +
			"<br/>" + "<br/>" +
			"Chúng tôi có quyền thêm các tính năng mới vào ứng dụng, ngừng cung cấp ứng dụng, hoặc bất kỳ một thành phần nào trong ứng dụng, hoặc đình chỉ, xóa, " +
			"sửa đổi hoặc vô hiệu hóa quyền truy cập vào ứng dụng, hoặc bất kỳ phần nào trong bất kỳ lúc nào theo quyết định của chúng tôi và chúng tôi hoàn toàn " +
			"có thể không cần thông báo cho bạn. Chúng tôi sẽ cố gắng gửi các thông báo về mọi thay đổi được thực hiện trên ứng dụng thông qua công cụ thông báo " +
			"trên ứng dụng, qua tài khoản của bạn hoặc qua email trước khi thực hiện các thay đổi đối với ứng dụng. Trong mọi trường hợp, chúng tôi sẽ không chịu " +
			"trách nhiệm về việc xóa hoặc vô hiệu hóa quyền truy cập ứng dụng hoặc bất kỳ phần nào trong đó." +
			"<br/>" + "<br/>" +
			"Chúng tôi có thể đơn phương xóa bài đăng vật phẩm của bạn theo sự đánh giá của chúng tôi. Lý do từ chối không giới hạn, có thể bao gồm: Hình ảnh nhạy " +
			"cảm/phản cảm, vật phẩm là các vật cấm, chất kích thích trái phép trong quy định của pháp luật nhà nước hiện hành..." +
			"<br/>" + "<br/>" +
			"Bất kỳ Vật phẩm hay hoạt động trao đổi nào được thực hiện thông qua ứng dụng Cho và Nhận đều thuộc về trách nhiệm của bạn và bạn phải tự chịu trách nhiệm " +
			"để đảm bảo cho sự an toàn về thông tin, tài sản, sự an toàn của bản thân trong các hoạt động trao đổi cho nhận bởi chúng tôi sẽ không can thiệp vào các " +
			"hoạt động này và chúng tôi sẽ không chịu trách nhiệm về vấn đề này." +
			"<br/>" + "<br/>" +
			"Chúng tôi hoan nghênh tất cả các ý kiến, phản hồi, thông tin hoặc tài liệu của bạn để đóng góp cho sự hoàn thiện của ứng dụng Cho và Nhận. Phản hồi của bạn " +
			"sẽ trở thành tài sản của chúng tôi khi bạn gửi nó cho chúng tôi. Bằng cách sử dụng tính năng gửi phản hồi trong ứng dụng, bạn được xem là đồng ý chuyển " +
			"nhượng các ý tưởng và thông tin từ bạn cho chúng tôi một cách tự nguyện. Chúng tôi sẽ tự do sử dụng, sao chép, phân phối, xuất bản và sửa đổi các Phản " +
			"hồi của bạn trên cơ sở không hạn chế và không hề vi phạm luật bản quyền và sở hữu trí tuệ." +
			"<br/>" + "<br/>" +
			"Chúng tôi tôn trọng quyền riêng tư thông tin cá nhân của tất cả người dùng, tất cả dữ liệu cá nhân được thu thập từ ứng dụng của người dùng sẽ được xử lý " +
			"theo Chính Sách Bảo Mật của chúng tôi – " +
			"<span><a href =\"" +
			PolicyWebPageLink +
			"\">tham khảo tại link</a></span>. " +
			"Khi chấp nhận Thỏa thuận này, bạn cũng đồng ý cho việc xử lý và sử dụng dữ liệu cá nhân của bạn theo chính sách bảo mật của chúng tôi." +
			"<br/>" + "<br/>" +
			"Chúng tôi có thể báo cáo các hoạt động gian lận của bạn hoặc vi phạm Thỏa thuận này với các cơ quan thực thi pháp luật có liên quan và chúng tôi cũng có " +
			"quyền cung cấp các thông tin cần thiết cho cảnh sát hoặc các bên thực thi pháp luật nếu như chúng tôi phát hiện hoặc được thông báo về các hoạt động phi " +
			"pháp, đe dọa gây nguy hiểm đến tài sản và tính mạng con người." +
			"<br/>" + "<br/>" +

			"<font color='#3fb8ea'><b>LIÊN HỆ VỚI CHÚNG TÔI</font></b> " +
			"<br/>" + "<br/>" +
			"Nếu bạn có bất kỳ câu hỏi hoặc thắc mắc nào về Thỏa thuận này hoặc về ứng dụng Cho và Nhận, vui lòng liên hệ với chúng tôi qua email: " +
			"<b>chovanhan.team@gmail.com.</b>"+
			"</div>"
			;

		public const string TermAndConditionContentInHtmlEn = "<div>Welcome to " +
			"<font color='#3fb8ea'><b>Cho va Nhan</b></font> " +
			"<br/>" + "<br/>" +

			"These Terms will help you adjust your use of \"Cho va Nhan\" application " +
			"properly and appropriately, so please read these terms carefully before using. " +
			"Once you have logged in to the application, it will be understood that you have agreed to be bound by the Terms and Privacy Policy of \"Cho va nhan\" application " +
			". If you do not agree with these terms and conditions of agreement, you should not and must not be albe to \"Cho va nhan\" application. " +
			"This agreement applies to all users without exceptions. " +
			"<br/>" + "<br/>" +
			
			"<font color='#3fb8ea'><b>WHAT IS \"CHO VA NHAN\" APPLICATION?</font></b> " +
			"<br/>" + "<br/>" +
			"\"Cho va nhan\" is a non-profit application where users can share free items to the community, " +
			"At the same time, you can get what you are looking for. The application allows users to simultaneously donate items " +
			"and offer to receive items from other users completely free without any financial activities such as cash exchange." +
			"<br/>" + "<br/>" +

			"<font color='#3fb8ea'><b>PURPOSE OF THE APPLICATION:</font></b> " +
			"<br/>" + "<br/>" +
			"The mission of \"Cho va nhan\" " +
			" is to give people the ability to build a shared community, help each other and bring people closer together through giving and receiving actions. " +
			"To promote this mission, \"Cho va nhan\" application is built with the purpose of:" +
			"<br/>" + "<br/>" +
			"Summary of information give and receive: The application sends notifications about the donation of valuable items for those who need it." +
			"<br/>" + "<br/>" +
			"Connecting benefactors: As a place for charitable organizations, benefactors can meet, organize volunteer programs." +
			"<br/>" + "<br/>" +
			"Helping difficult people: Through the application, it is easier to give precious, useful and necessary gifts to people in difficult circumstances." +
			"<br/>" + "<br/>" +
			"Raising awareness of the community: Encouraging, recognizing the spirit of support and assistance among individuals and organizations in the community, promoting awareness, building a community of mutual affection." +
			"<br/>" + "<br/>" +

			"<font color='#3fb8ea'><b>YOUR USER ACCOUNT</font></b> " +
			"<br/>" + "<br/>" +
			"<b>Register user account:</b>" +
			"<br/>" + "<br/>" +
			"You can find out information about the application at the official website of the application without an account but you need to have an account to log in and exchange information for give and receive on the application. " +
			"We allow you to subscribe to \"Cho va nhan\" apps using third-party services such as Facebook. " +
			"If you use a third party service to register for an account, you may be required to provide your login information to that third party service. " +
			"We do not record or store passwords for such third party services. " +
			"Please review your security and installation messages for that third party service before using to log in \"Cho va nhan\" application. " +
			"Your account is exclusive to you and you are not allowed to share or allow anyone else to use your account. " +
			"You may not be albe to use someone else's account, create accounts for others, or create and use accounts to impersonate others. " +
			"You will be responsible for all activities arising from your account." +
			"<br/>" + "<br/>" +
			"In addition, \"Cho va nhan\" also support automatic login after you log in for the first time and you can only log in with another account after logging out of your account from the app. " +
			"You are responsible for all damages caused by unauthorized access to the application from your account. " +
			"You must notify us immediately if you know or have reason to suspect any unauthorized use of your account. " +
			"<br/>" + "<br/>" +

			"<b>Policies represent user interests – Your commitment to us:</b>" +
			"<br/>" + "<br/>" +
			"You must ensure that: " +
			"<b>(1)</b> All information you provide us is truthful, accurate, complete and will always be truthful, accurate and complete; " +
			"<b>(2)</b> You will comply with the terms and conditions of this Agreement and any other agreements with which you are subject to the use of \"Cho va nhan\" application from your account; " +
			"<b>(3)</b> You will not use or access the application for fraudulent or appropriated properties; and " +
			"<b>(4)</b> Your access and use of your application account will not create violations of any other agreements, contracts, terms of use or any other laws and regulations that you must follow. " +
			"<b>(5)</b> You must always abide by the laws and regulations of the local government in exchanging information and keeping the traditions and  " +
			"customs in line with people's morality in the activities of posting  " +
			"images, information and Item exchange takes place on \"Cho va nhan\" application. " +
			"<b>(6)</b> You will not give away items that may endanger people or intentionally commit acts of harming others.  " +
			"<b>(7)</b> We may limit your ability to use the application, including sending infringement alerts when there are feedback reports from other users " +
			"about your inappropriate and improper behavior when posting information about images, objectionable content, reactionary, anti-government,  " +
			"not consistent with fine customs and morality; In addition, we can unilaterally suspend and lock your application " +
			"account without prior notice if you violate too many times or the level of violation is too great or in the case if we have reasonable grounds " +
			"to suspect that the information you provide is untrue, inaccurate, outdated, incomplete or in violation of the law. " +
			"<br/>" + "<br/>" +

			"<font color='#3fb8ea'><b>HOW TO USE THE APPLICATION</font></b> " +
			"<br/>" + "<br/>" +
			"<b>When you are a giver:</b>" +
			"<br/>" + "<br/>" +
			"1. When you decide to give an item on \"Cho va nhan\", you will post an article about the item, which describes the item in detail (image, category, usage status, delivery location ...)" +
			"<br/>" + "<br/>" +
			"2. You will receive requests for items and you will review these offers." +
			"<br/>" + "<br/>" +
			"3. You make a confirmation of agreement with the proposal that you think is appropriate and would like to donate the item or reject inappropriate offers." +
			"<br/>" + "<br/>" +

			"<b>When you are the person who wants to receive the item:</b>" +
			"<br/>" + "<br/>" +
			"1. When you want to receive an item, you will search for the desired item based on the image, category, usage status ... to determine the appropriate item." +
			"<br/>" + "<br/>" +
			"2. After finding the desired item, you will enter the item owner's post and send the request to receive the item for the giver." +
			"<br/>" + "<br/>" +
			"3. If the giver agrees to your offer, you will exchange information on how to get the item (exchange details about the time, place, and how to get the item with the owner)." +
			"<br/>" + "<br/>" +

			"<font color='#3fb8ea'><b>OUR RIGHTS AND OBLIGATIONS</font></b> " +
			"<br/>" + "<br/>" +
			"We do not provide facilities or repositories of items, donations are conducted through the exchange of personal information between giver and taker." +
			"<br/>" + "<br/>" +
			"We do not charge any fees related to activities awarded and generated because this is a non-profit service, serving the community " +
			"and society; therefore, all financial-related actions such as charging for services from individuals and organizations who are not \"Cho va nhan\" " +
			"application managers or impersonating \"Cho va nhan\" application will be considered It is a violation of law and may lead to " +
			"criminal and civil prosecution. You can report and send feedback if detecting these illegal activities to us via email: " +
			"<b>chovanhan.team@gmail.com.</b>" +
			"<br/>" + "<br/>" +
			"We reserve the right to add new features to the application, discontinue the application, or any component in the application, or " +
			"suspend, delete, modify or disable access to the application, or Any part in any time at our discretion and we absolutely may not need to notify you. " +
			"We will endeavor to send notices about any changes made on the application through the application notification tool, via your account or via email before making changes to the application. " +
			"In any case, we will not be responsible for deleting or disabling application access or any part thereof." +
			"<br/>" + "<br/>" +
			"We may unilaterally delete your item post according to our evaluation. " +
			"Reasons for refuse may include: Sensitive / offensive images, items that are prohibited, illegal stimulants in the current state law ..." +
			"<br/>" + "<br/>" +
			"Any Item or exchange activity performed through \"Cho va nhan\" application is your responsibility and you are solely responsible for " +
			"ensuring the safety of information, property, and security. All of your own exchange activities for giving or receiving will not interfere by us  and we will not be responsible for this issue." +
			"<br/>" + "<br/>" +
			"We welcome all of your comments, feedback, information or materials to contribute to the completion of \"Cho va nhan\" application. Phản hồi của bạn " +
			"Your feedback will become our property when you send it to us. By using the feature to send feedback in the application, you are " +
			"considered to agree to transfer ideas and information from you voluntarily. We will be free to use, copy, distribute, publish and modify your " +
			"feedback on an unlimited basis and without violating copyright and intellectual property laws." +
			"<br/>" + "<br/>" +
			"We respect the privacy of personal information of all users, all personal data collected from the user will be processed according to our Privacy Policy – " +
			"<span><a href =\"" +
			PolicyWebPageLink +
			"\">Refer to the link</a></span>. " +
			"By accepting this Agreement, you also agree to the processing and use of your personal data in accordance with our privacy policy." +
			"<br/>" + "<br/>" +
			"We may report your fraudulent activities or violate this Agreement with relevant law enforcement agencies and we also have the right to " +
			"provide the necessary information to the police or real parties. law enforcement if we discover or be informed of illegal activities, " +
			"threatening to endanger human property and lives." +
			"<br/>" + "<br/>" +

			"<font color='#3fb8ea'><b>CONTACT US</font></b> " +
			"<br/>" + "<br/>" +
			"If you have any questions or concerns regarding this Agreement of \"Cho va nhan\" application, please contact us via email: " +
			"<b>chovanhan.team@gmail.com.</b>" +
			"</div>"
			;
	}
}