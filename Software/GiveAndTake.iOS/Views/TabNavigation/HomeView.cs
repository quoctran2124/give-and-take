﻿using GiveAndTake.Core;
using GiveAndTake.Core.ViewModels.TabNavigation;
using GiveAndTake.iOS.Controls;
using GiveAndTake.iOS.Helpers;
using GiveAndTake.iOS.Views.Base;
using GiveAndTake.iOS.Views.TableViewCells;
using GiveAndTake.iOS.Views.TableViewSources;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Commands;
using MvvmCross.Platforms.Ios.Binding.Views.Gestures;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using MvvmCross.Platforms.Ios.Views;
using MvvmCross.ViewModels;
using System;
using UIKit;

namespace GiveAndTake.iOS.Views.TabNavigation
{
	[MvxTabPresentation(TabIconName = "Images/home_off",
		TabSelectedIconName = "Images/home_on",
		WrapInNavigationController = true)]
	public class HomeView : BaseView
	{
		public IMvxCommand LoadMoreCommand { get; set; }

		public IMvxCommand SearchCommand { get; set; }

		public IMvxInteraction ShowProfileTab
		{
			get => _showProfileTab;
			set
			{
				if (_showProfileTab != null)
					_showProfileTab.Requested -= OnShowProfileTabRequested;

				_showProfileTab = value;
				_showProfileTab.Requested += OnShowProfileTabRequested;
			}
		}

		private UIScrollView _scrollView;
		private UIView _contentView;
		private UIButton _btnFilter;
		private UIButton _btnSort;
		private UIButton _btnCategory;
		private UISearchBar _searchBar;
	    private UIView _separatorLine;
        private UITableView _postsTableView;
		private PostItemTableViewSource<PostItemViewCell> _postTableViewSource;
		private MvxUIRefreshControl _refreshControl;
		private UIButton _newPostButton;
		private PopupItemLabel _searchResult;
		private IMvxInteraction _showProfileTab;

		protected override void InitView()
		{
			View.AddGestureRecognizer(new UITapGestureRecognizer(HideKeyboard)
			{
				CancelsTouchesInView = false
			});

			InitScrollAndContentView();
			InitFilterBar();
            InitPostsTableView();
			InitNewPostButton();

			RegisterViewToCenterOnKeyboardShown(_contentView);
			IsViewMoved = false;

			_searchBar.ShouldBeginEditing += OnTextFieldEditting;
		}

		private void InitScrollAndContentView()
		{
			_scrollView = UIHelper.CreateScrollView(0, 0, UIColor.Clear);
			View.AddSubview(_scrollView);
			View.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_scrollView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Top, 1, ResolutionHelper.StatusHeight + DimensionHelper.HeaderBarHeight),
				NSLayoutConstraint.Create(_scrollView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_scrollView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_scrollView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Bottom, 1, 0),
			});

			_contentView = UIHelper.CreateView(0, 0, UIColor.Clear);
			_scrollView.AddSubview(_contentView);
			_scrollView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _scrollView,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _scrollView,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Width, NSLayoutRelation.Equal, _scrollView,
					NSLayoutAttribute.Width, 1, 0),
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Height, NSLayoutRelation.Equal, _scrollView,
					NSLayoutAttribute.Height, 1, 0),
			});
		}

		private void InitFilterBar()
		{
			_btnFilter = UIHelper.CreateImageButton(DimensionHelper.FilterSize, DimensionHelper.FilterSize, ImageHelper.LocationButtonDefault, ImageHelper.LocationButtonSelected);

			_contentView.Add(_btnFilter);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_btnFilter, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Top, 1, DimensionHelper.MarginShort),
				NSLayoutConstraint.Create(_btnFilter, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Right, 1, -DimensionHelper.MarginNormal)
			});

			_btnSort = UIHelper.CreateImageButton(DimensionHelper.FilterSize, DimensionHelper.FilterSize, ImageHelper.SortButtonDefault, ImageHelper.SortButtonSelected);

			_contentView.Add(_btnSort);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_btnSort, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _btnFilter,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_btnSort, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _btnFilter,
					NSLayoutAttribute.Left, 1, -DimensionHelper.MarginNormal)
			});

			_btnCategory = UIHelper.CreateImageButton(DimensionHelper.FilterSize, DimensionHelper.FilterSize, ImageHelper.CategoryButtonDefault, ImageHelper.CategoryButtonSelected);
			_contentView.Add(_btnCategory);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_btnCategory, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _btnFilter,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_btnCategory, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _btnSort,
					NSLayoutAttribute.Left, 1, -DimensionHelper.MarginNormal)
			});

			_searchBar = UIHelper.CreateSearchBar(DimensionHelper.FilterSize, DimensionHelper.FilterSize);
			_searchBar.SearchButtonClicked += OnSearchSubmit;
			_searchBar.TextChanged += OnCancelClicked;
			_contentView.Add(_searchBar);

			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_searchBar, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _btnFilter,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_searchBar, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _btnCategory,
					NSLayoutAttribute.Left, 1, -DimensionHelper.MarginNormal),
				NSLayoutConstraint.Create(_searchBar, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Left, 1, DimensionHelper.MarginShort)
			});
			

			_separatorLine = UIHelper.CreateView(DimensionHelper.MenuSeparatorLineHeight, DimensionHelper.HeaderBarLogoWidth, ColorHelper.GreyLineColor);
			_contentView.Add(_separatorLine);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_separatorLine, NSLayoutAttribute.Width, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Width, 1, 0),
				NSLayoutConstraint.Create(_separatorLine, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _searchBar,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.MarginShort)
			});
		}

		private void OnCancelClicked(object sender, UISearchBarTextChangedEventArgs e)
		{
			if (_searchBar.Text == "")
			{
				HeaderBar.BackPressedCommand.Execute();
				_searchBar.EndEditing(true);
			}
		}


		protected override void Dispose(bool disposing)
		{
			_searchBar.SearchButtonClicked -= OnSearchSubmit;
			_searchBar.TextChanged -= OnCancelClicked;
			base.Dispose(disposing);
		}

		private void InitPostsTableView()
		{
			_postsTableView = UIHelper.CreateTableView(0, 0);
			_postTableViewSource = new PostItemTableViewSource<PostItemViewCell>(_postsTableView)
			{
				LoadMoreEvent = () => LoadMoreCommand?.Execute()
			};

			_postsTableView.Source = _postTableViewSource;
			_refreshControl = new MvxUIRefreshControl();
			_postsTableView.RefreshControl = _refreshControl;

			_contentView.Add(_postsTableView);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_postsTableView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _separatorLine, NSLayoutAttribute.Bottom, 1, DimensionHelper.MarginShort),
				NSLayoutConstraint.Create(_postsTableView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.Left, 1, DimensionHelper.MarginShort),
				NSLayoutConstraint.Create(_postsTableView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_postsTableView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.Right, 1, - DimensionHelper.MarginShort)
			});

			_searchResult = UIHelper.CreateLabel(UIColor.Black, DimensionHelper.BigTextSize);
			_searchResult.Text = "Không tìm thấy kết quả nào";
			_contentView.Add(_searchResult);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_searchResult, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_searchResult, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.CenterY, 1, 0)
			});
		}

		private void InitNewPostButton()
		{
			_newPostButton = UIHelper.CreateImageButton(DimensionHelper.NewPostSize, DimensionHelper.NewPostSize, ImageHelper.NewPost);

			_contentView.Add(_newPostButton);

			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_newPostButton, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Bottom, 1, -DimensionHelper.MarginNormal - TabBarController.TabBar.Frame.Height),
				NSLayoutConstraint.Create(_newPostButton, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Right, 1, -DimensionHelper.MarginNormal)
			});
		}

		protected override void CreateBinding()
		{
			base.CreateBinding();
			var set = this.CreateBindingSet<HomeView, HomeViewModel>();

			set.Bind(_postTableViewSource)
				.To(vm => vm.PostItemViewModelCollection);

			set.Bind(_searchBar)
				.For(v => v.Text)
				.To(vm => vm.CurrentQueryString);

			set.Bind(this)
				.For(v => v.LoadMoreCommand)
				.To(vm => vm.LoadMoreCommand);

			set.Bind(this)
				.For(v => v.SearchCommand)
				.To(vm => vm.SearchCommand);

			set.Bind(_refreshControl)
				.For(v => v.IsRefreshing)
				.To(vm => vm.IsRefreshing);

			set.Bind(_refreshControl)
				.For(v => v.RefreshCommand)
				.To(vm => vm.RefreshCommand);

			set.Bind(_btnCategory.Tap())
				.For(v => v.Command)
				.To(vm => vm.ShowCategoriesCommand);

			set.Bind(_btnFilter.Tap())
				.For(v => v.Command)
				.To(vm => vm.ShowLocationFiltersCommand);

			set.Bind(_btnSort.Tap())
				.For(v => v.Command)
				.To(vm => vm.ShowSortFiltersCommand);

			set.Bind(_newPostButton.Tap())
				.For(v => v.Command)
				.To(vm => vm.CreatePostCommand);

			set.Bind(_btnCategory)
				.For(v => v.Selected)
				.To(vm => vm.IsCategoryFilterActivated);

			set.Bind(_btnFilter)
				.For(v => v.Selected)
				.To(vm => vm.IsLocationFilterActivated);

			set.Bind(_btnSort)
				.For(v => v.Selected)
				.To(vm => vm.IsSortFilterActivated);

			set.Bind(_searchResult)
				.For(v => v.Text)
				.To(vm => vm.SearchResultTitle);

			set.Bind(_searchResult)
				.For("Visibility")
				.To(vm => vm.IsSearchResultNull);

			set.Bind(this)
				.For(view => view.ShowProfileTab)
				.To(viewModel => viewModel.ShowProfileTab)
				.OneWay();

			set.Bind(HeaderBar)
				.For(v => v.BackButtonIsShown)
				.To(vm => vm.IsSearched);
			set.Bind(HeaderBar)
				.For(v => v.BackPressedCommand)
				.To(vm => vm.BackPressedCommand);
			set.Apply();
		}

		private void HideKeyboard() => View.EndEditing(true);

		private void OnSearchSubmit(object sender, EventArgs e)
		{
			_searchBar.ResignFirstResponder();
			SearchCommand.Execute();
		}

		private void OnShowProfileTabRequested(object sender, EventArgs e)
		{
			TabBarController.SelectedIndex = AppConstants.ProfileTabIndex;
		}
	}
}