using FFImageLoading;
using FFImageLoading.Transformations;
using FFImageLoading.Work;
using GiveAndTake.Core;
using GiveAndTake.Core.Models;
using GiveAndTake.Core.ViewModels.TabNavigation;
using GiveAndTake.iOS.Controls;
using GiveAndTake.iOS.Helpers;
using MvvmCross;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Commands;
using MvvmCross.Platforms.Ios.Views;
using System.Collections.Generic;
using System.Threading.Tasks;
using UIKit;

namespace GiveAndTake.iOS.Views.TabNavigation
{
	public class TabNavigationView : MvxTabBarViewController<TabNavigationViewModel>
	{
		public IMvxCommand ClearBadgeCommand { get; set; }

		private int _notificationCount;
		private bool _isOnNotificationViewTab;
		private CustomMvxCachedImageView _imgAvatar;
		private CustomMvxCachedImageView _imgAvatarSelected;

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);
			ViewModel.ShowInitialViewModelsCommand.Execute();
			ConfigTabBar(animated);
			CreateBinding();
		}

		private void CreateBinding()
		{
			var bindingSet = this.CreateBindingSet<TabNavigationView, TabNavigationViewModel>();

			bindingSet.Bind(this)
				.For(v => v.NotificationCount)
				.To(vm => vm.NotificationCount);

			bindingSet.Bind(this)
				.For(v => v.ClearBadgeCommand)
				.To(vm => vm.ClearBadgeCommand);

			bindingSet.Apply();
		}

		public int NotificationCount
		{
			get => _notificationCount;
			set
			{
				_notificationCount = value;
				if (value == 0)
				{
					TabBar.Items[(int)BottomTabBar.Notification].BadgeValue = null;
					UpdateBadgeIcon(0);
				}
				else
				{
					TabBar.Items[(int)BottomTabBar.Notification].BadgeValue = value.ToString();
				}
			}
		}

		private async void ConfigTabBar(bool animated)
		{
			TabBar.BackgroundColor = UIColor.White;
			if (TabBar.Items.Length != ViewModel.NumberOfTab)
			{
				ViewModel.ShowErrorCommand.Execute(null);
				return;
			}

			await ConfigProfileTab();

			foreach (var tabBarItem in TabBar.Items)
			{
				tabBarItem.Image = tabBarItem.Image
					.ImageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal);
				tabBarItem.SelectedImage = tabBarItem.SelectedImage
					.ImageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal);
				tabBarItem.ImageInsets = new UIEdgeInsets(5.5f, 0, -5.5f, 0);
				tabBarItem.Title = null;
			}

			if (Mvx.Resolve<IDataModel>().SelectedNotification != null)
			{
				SelectedIndex = (int) BottomTabBar.Notification;
			}

			ViewControllerSelected += TabBarControllerOnViewControllerSelected;
			NavigationController?.SetNavigationBarHidden(true, animated);
		}

		private async Task ConfigProfileTab()
		{
			_imgAvatar = UIHelper.CreateCustomImageView(DimensionHelper.ImageAvatarSize,
				DimensionHelper.ImageAvatarSize, ImageHelper.AvtOff, DimensionHelper.ImageAvatarSize / 2);
			_imgAvatarSelected = UIHelper.CreateCustomImageView(DimensionHelper.ImageAvatarSize,
				DimensionHelper.ImageAvatarSize, ImageHelper.AvtOff, DimensionHelper.ImageAvatarSize / 2);

			if (!string.IsNullOrEmpty(ViewModel.AvatarUrl))
			{
				await ImageService.Instance.LoadUrl(ViewModel.AvatarUrl)
					.DownSample((int) DimensionHelper.ImageAvatarSize, (int) DimensionHelper.ImageAvatarSize)
					.Transform(new List<ITransformation> {new CircleTransformation()})
					.IntoAsync(_imgAvatar);
				await ImageService.Instance.LoadUrl(ViewModel.AvatarUrl)
					.DownSample((int) DimensionHelper.ImageAvatarSize, (int) DimensionHelper.ImageAvatarSize)
					.Transform(new List<ITransformation>
						{new CircleTransformation(DimensionHelper.AvatarBorderWidth, "3fb8ea")})
					.IntoAsync(_imgAvatarSelected);
			}

			TabBar.Items[(int) BottomTabBar.Profile].Image = _imgAvatar.Image;
			TabBar.Items[(int) BottomTabBar.Profile].SelectedImage = _imgAvatarSelected.Image;
		}


		private void TabBarControllerOnViewControllerSelected(object sender, UITabBarSelectionEventArgs e)
		{
			var selectedView = ((UINavigationController)e.ViewController).TopViewController as MvxViewController;

			if (selectedView == null) return;

			if (_isOnNotificationViewTab)
			{
				ClearBadgeCommand.Execute();
				_isOnNotificationViewTab = false;
			}

			if (SelectedIndex == (int) BottomTabBar.Notification)
			{
				ClearBadgeCommand.Execute();
				UpdateBadgeIcon(0);
				_isOnNotificationViewTab = true;
			}
		}

		private void UpdateBadgeIcon(int badgeValue)
		{
			var settings = UIUserNotificationSettings.GetSettingsForTypes(UIUserNotificationType.Badge, null);
			UIApplication.SharedApplication.RegisterUserNotificationSettings(settings);
			UIApplication.SharedApplication.ApplicationIconBadgeNumber = badgeValue;
		}
	}
}