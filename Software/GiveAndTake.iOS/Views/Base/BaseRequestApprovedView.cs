﻿using GiveAndTake.Core;
using GiveAndTake.Core.ViewModels.Base;
using GiveAndTake.iOS.Controls;
using GiveAndTake.iOS.Helpers;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Binding.Views.Gestures;
using UIKit;

namespace GiveAndTake.iOS.Views.Base
{
	public class BaseRequestApprovedView : BaseView
	{
		protected UIView _contentView;
		protected UIView _contentScrollView;
		protected UIScrollView _scrollView;
		protected NSLayoutConstraint _scrollViewHeightConstraint;
		protected NSLayoutConstraint _contentViewHeightConstraint;
		protected UIView _headerPopupView;
		protected CustomMvxCachedImageView _imagePost;

		private UIView _overlayView;
		private UIView _popupView;
		private UILabel _lbMyRequestPopupTitle;
		private UIButton _deletePhotoButton;
		
		private CustomMvxCachedImageView _imgTakerAvatar;
		private UILabel _lbTakerFullName;
		private UILabel _lbRequestCreatedDate;
		private UILabel _lbRequestMessage;

		private CustomMvxCachedImageView _imgGiverAvatar;
		private UILabel _lbGiverFullName;
		private UILabel _lbPostCreatedDate;
		private UILabel _lbApprovedMessage;

		protected override void InitView()
		{
			View.BackgroundColor = UIColor.Clear;
			InitOverlayView();
			InitRequestContentView();
			InitApprovedContentView();
		}
		protected override void CreateBinding()
		{
			base.CreateBinding();

			var bindingSet = this.CreateBindingSet<BaseRequestApprovedView, BaseRequestApprovedViewModel>();

			bindingSet.Bind(_lbMyRequestPopupTitle)
				.For(v => v.Text)
				.To(vm => vm.MyRequestPopupTitle);

			bindingSet.Bind(_deletePhotoButton.Tap())
				.For(v => v.Command)
				.To(vm => vm.CloseCommand);

			bindingSet.Bind(_imagePost)
				.For(v => v.ImageUrl)
				.To(vm => vm.ImgPostUrl);

			bindingSet.Bind(_imagePost.Tap())
				.For(v => v.Command)
				.To(vm => vm.ImgPostClickedCommand);

			bindingSet.Bind(_imgTakerAvatar)
				.For(v => v.ImageUrl)
				.To(vm => vm.TakerAvatarUrl);

			bindingSet.Bind(_imgTakerAvatar.Tap())
				.For(v => v.Command)
				.To(vm => vm.ShowTakerProfileCommand);

			bindingSet.Bind(_lbTakerFullName)
				.For(v => v.Text)
				.To(vm => vm.TakerFullName);

			bindingSet.Bind(_lbTakerFullName.Tap())
				.For(v => v.Command)
				.To(vm => vm.ShowTakerProfileCommand);

			bindingSet.Bind(_lbRequestCreatedDate)
				.For(v => v.Text)
				.To(vm => vm.RequestCreatedDate);

			bindingSet.Bind(_lbRequestMessage)
				.For(v => v.Text)
				.To(vm => vm.RequestMessage);

			bindingSet.Bind(_imgGiverAvatar)
				.For(v => v.ImageUrl)
				.To(vm => vm.GiverAvatarUrl);

			bindingSet.Bind(_imgGiverAvatar.Tap())
				.For(v => v.Command)
				.To(vm => vm.ShowGiverProfileCommand);

			bindingSet.Bind(_lbGiverFullName)
				.For(v => v.Text)
				.To(vm => vm.GiverFullName);

			bindingSet.Bind(_lbGiverFullName.Tap())
				.For(v => v.Command)
				.To(vm => vm.ShowGiverProfileCommand);

			bindingSet.Bind(_lbPostCreatedDate)
				.For(v => v.Text)
				.To(vm => vm.PostCreatedDate);

			bindingSet.Bind(_lbApprovedMessage)
				.For(v => v.Text)
				.To(vm => vm.ApprovedMessage);

			bindingSet.Apply();
		}

		private void InitOverlayView()
		{
			_overlayView = UIHelper.CreateView(0, 0, UIColor.Black.ColorWithAlpha(0.7f));

			View.Add(_overlayView);
			View.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_overlayView, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, View, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_overlayView, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, View, NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_overlayView, NSLayoutAttribute.Width, NSLayoutRelation.Equal, View, NSLayoutAttribute.Width, 1, 0),
				NSLayoutConstraint.Create(_overlayView, NSLayoutAttribute.Height, NSLayoutRelation.Equal, View, NSLayoutAttribute.Height, 1, 0),
			});
		}

		private void InitRequestContentView()
		{
			_popupView = UIHelper.CreateView(0, 0, UIColor.White, DimensionHelper.PopupContentRadius);
			_popupView.AddGestureRecognizer(new UITapGestureRecognizer { CancelsTouchesInView = true });

			_overlayView.Add(_popupView);
			_overlayView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_popupView, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _overlayView, NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_popupView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _overlayView, NSLayoutAttribute.Left, 1, DimensionHelper.BigMargin),
				NSLayoutConstraint.Create(_popupView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _overlayView, NSLayoutAttribute.Right, 1, -DimensionHelper.BigMargin),
			});

			_contentView = UIHelper.CreateView(0, 0, UIColor.White, 0);
			_contentViewHeightConstraint = NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, 0);

			_popupView.AddSubview(_contentView);
			_popupView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _popupView, NSLayoutAttribute.Left, 1, DimensionHelper.DefaultMargin),
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _popupView, NSLayoutAttribute.Right, 1, -DimensionHelper.DefaultMargin),
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _popupView, NSLayoutAttribute.Top, 1, DimensionHelper.MediumMargin),
				_contentViewHeightConstraint,
				NSLayoutConstraint.Create(_popupView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.Bottom, 1, DimensionHelper.MediumMargin),
			});

			_headerPopupView = UIHelper.CreateView(0, 0, UIColor.Clear);
			_contentView.AddSubview(_headerPopupView);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_headerPopupView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_headerPopupView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.Left, 1, DimensionHelper.DefaultMargin),
				NSLayoutConstraint.Create(_headerPopupView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.Right, 1, -DimensionHelper.DefaultMargin),
			});

			_lbMyRequestPopupTitle = UIHelper.CreateLabel(UIColor.Black, DimensionHelper.PopupRequestTitleTextSize);
			_headerPopupView.Add(_lbMyRequestPopupTitle);
			_headerPopupView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_lbMyRequestPopupTitle, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _headerPopupView, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_lbMyRequestPopupTitle, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _headerPopupView, NSLayoutAttribute.Top, 1, 0)
			});

			_deletePhotoButton = UIHelper.CreateImageButton(DimensionHelper.DeletePhotoButtonWidth,
				DimensionHelper.DeletePhotoButtonWidth, ImageHelper.DeleteRequestDetailButton);
			_headerPopupView.AddSubview(_deletePhotoButton);
			_headerPopupView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_deletePhotoButton, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _headerPopupView,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_deletePhotoButton, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _headerPopupView,
					NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_headerPopupView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _deletePhotoButton,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.MediumMargin)
			});

			_scrollView = UIHelper.CreateScrollView(0, 0);
			_scrollViewHeightConstraint = NSLayoutConstraint.Create(_scrollView, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null,
					NSLayoutAttribute.NoAttribute, 1, 0);
			_contentView.AddSubview(_scrollView);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_scrollView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _headerPopupView,
					NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_scrollView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Left, 1, 0),
					NSLayoutConstraint.Create(_scrollView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Right, 1, 0),
					_scrollViewHeightConstraint
			});

			_contentScrollView = UIHelper.CreateView(0, 0, UIColor.White, 0);
			_scrollView.AddSubview(_contentScrollView);
			_scrollView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_contentScrollView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _scrollView,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_contentScrollView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _scrollView,
					NSLayoutAttribute.Left, 1, DimensionHelper.DefaultMargin),
				NSLayoutConstraint.Create(_contentScrollView, NSLayoutAttribute.Width, NSLayoutRelation.Equal, _scrollView,
					NSLayoutAttribute.Width, 1, -2* DimensionHelper.DefaultMargin),
			});

			_imagePost = UIHelper.CreateCustomImageView(DimensionHelper.ImageRequestSmallSize, DimensionHelper.ImageRequestSmallSize, ImageHelper.DefaultPost, DimensionHelper.PostPhotoCornerRadius);
			_imagePost.SetPlaceHolder(ImageHelper.DefaultPost, ImageHelper.DefaultPost);

			_contentScrollView.AddSubview(_imagePost);
			_contentScrollView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_imagePost, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _contentScrollView,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_imagePost, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _contentScrollView,
					NSLayoutAttribute.Right, 1, 0)
			});

			_imgTakerAvatar = UIHelper.CreateCustomImageView(DimensionHelper.ImageAvatarSize, DimensionHelper.ImageAvatarSize, ImageHelper.DefaultAvatar, DimensionHelper.ImageAvatarSize / 2);
			_imgTakerAvatar.SetPlaceHolder(ImageHelper.DefaultAvatar, ImageHelper.DefaultAvatar);

			_contentScrollView.AddSubview(_imgTakerAvatar);
			_contentScrollView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_imgTakerAvatar, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _contentScrollView,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_imgTakerAvatar, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _contentScrollView,
					NSLayoutAttribute.Left, 1, 0)
			});

			_lbTakerFullName = UIHelper.CreateLabel(UIColor.Black, DimensionHelper.MediumTextSize);
			_contentScrollView.AddSubview(_lbTakerFullName);
			_contentScrollView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_lbTakerFullName, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _imgTakerAvatar,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_lbTakerFullName, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _imgTakerAvatar,
					NSLayoutAttribute.Right, 1, DimensionHelper.DefaultMargin)
			});

			_lbRequestCreatedDate = UIHelper.CreateLabel(UIColor.DarkGray, DimensionHelper.SmallTextSize);
			_contentScrollView.AddSubview(_lbRequestCreatedDate);
			_contentScrollView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_lbRequestCreatedDate, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _lbTakerFullName,
					NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_lbRequestCreatedDate, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _lbTakerFullName,
					NSLayoutAttribute.Left, 1, 0)
			});

			_lbRequestMessage = UIHelper.CreateLabel(UIColor.DarkGray, DimensionHelper.PostDescriptionTextSize, FontType.Regular, UITextAlignment.Justified);
			_contentScrollView.Add(_lbRequestMessage);
			_contentScrollView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_lbRequestMessage, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _lbRequestCreatedDate,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.LargeMargin),
				NSLayoutConstraint.Create(_lbRequestMessage, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _contentScrollView,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_lbRequestMessage, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _contentScrollView,
					NSLayoutAttribute.Right, 1, 0),
			});
		}
		private void InitApprovedContentView()
		{
			_imgGiverAvatar = UIHelper.CreateCustomImageView(DimensionHelper.ImageAvatarSize, DimensionHelper.ImageAvatarSize, ImageHelper.DefaultAvatar, DimensionHelper.ImageAvatarSize / 2);
			_imgGiverAvatar.SetPlaceHolder(ImageHelper.DefaultAvatar, ImageHelper.DefaultAvatar);
			_contentScrollView.AddSubview(_imgGiverAvatar);
			_contentScrollView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_imgGiverAvatar, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _lbRequestMessage,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.LargeMargin),
				NSLayoutConstraint.Create(_imgGiverAvatar, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _lbRequestMessage,
					NSLayoutAttribute.Left, 1, 0)
			});

			_lbGiverFullName = UIHelper.CreateLabel(UIColor.Black, DimensionHelper.MediumTextSize);
			_contentScrollView.AddSubview(_lbGiverFullName);
			_contentScrollView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_lbGiverFullName, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _imgGiverAvatar,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_lbGiverFullName, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _imgGiverAvatar,
					NSLayoutAttribute.Right, 1, DimensionHelper.DefaultMargin)
			});

			_lbPostCreatedDate = UIHelper.CreateLabel(UIColor.DarkGray, DimensionHelper.SmallTextSize);
			_contentScrollView.AddSubview(_lbPostCreatedDate);
			_contentScrollView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_lbPostCreatedDate, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _lbGiverFullName,
					NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_lbPostCreatedDate, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _lbGiverFullName,
					NSLayoutAttribute.Left, 1, 0)
			});

			_lbApprovedMessage = UIHelper.CreateLabel(UIColor.DarkGray, DimensionHelper.PostDescriptionTextSize, FontType.Regular, UITextAlignment.Justified);
			_contentScrollView.Add(_lbApprovedMessage);
			_contentScrollView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_lbApprovedMessage, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _lbPostCreatedDate,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.LargeMargin),
				NSLayoutConstraint.Create(_lbApprovedMessage, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _contentScrollView,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_lbApprovedMessage, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _contentScrollView,
					NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_contentScrollView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _lbApprovedMessage,
					NSLayoutAttribute.Bottom, 1, 0),
			});
		}
	}
}