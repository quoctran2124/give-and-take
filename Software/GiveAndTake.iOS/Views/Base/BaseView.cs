using GiveAndTake.iOS.CustomControls;
using Foundation;
using GiveAndTake.Core.ViewModels.Base;
using GiveAndTake.iOS.Helpers;
using MvvmCross.Platforms.Ios.Views;
using UIKit;

namespace GiveAndTake.iOS.Views.Base
{
	public abstract class BaseView : MvxViewController
	{
		protected HeaderBar HeaderBar;
		private NSObject _didBecomeActiveNotificationObserver;
		private NSObject _willEnterForegroundNotificationObserver;
		protected bool IsViewMoved;

		public override void ViewDidLoad()
		{
			View = new UIView
			{
				BackgroundColor = UIColor.White,
				MultipleTouchEnabled = false
			};

			KeyboardHelper.ViewToCenterOnKeyboardShown = null;

			base.ViewDidLoad();

			NavigationController?.SetNavigationBarHidden(true, true);

			CreateHeaderBar();
			InitView();
			DismissKeyboard();
			CreateBinding();
		}

		protected void RegisterViewToCenterOnKeyboardShown(UIView view)
		{
			KeyboardHelper.ViewToCenterOnKeyboardShown = view;
		}

		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);
			_didBecomeActiveNotificationObserver = NSNotificationCenter.DefaultCenter.AddObserver(UIApplication.DidBecomeActiveNotification, OnDidBecomeActive);
			_willEnterForegroundNotificationObserver = NSNotificationCenter.DefaultCenter.AddObserver(UIApplication.DidEnterBackgroundNotification, OnDidEnterBackground);
		}
		protected virtual void OnDidBecomeActive(NSNotification obj)
		{
			if (!View.Hidden) (ViewModel as BaseViewModel)?.OnActive();
		}

		protected virtual void OnDidEnterBackground(NSNotification obj)
		{
			if (!View.Hidden) (ViewModel as BaseViewModel)?.OnDeactive();
		}
		protected void CreateHeaderBar()
		{
			HeaderBar = UIHelper.CreateHeaderBar(ResolutionHelper.Width, DimensionHelper.HeaderBarHeight,
				UIColor.White);
			View.Add(HeaderBar);
			View.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(HeaderBar, NSLayoutAttribute.Top, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Top, 1, ResolutionHelper.StatusHeight),
				NSLayoutConstraint.Create(HeaderBar, NSLayoutAttribute.Left, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Left, 1, 0),
			});
		}

		protected virtual void ConfigNavigationBar()
		{
			//NavigationItem
		}

		protected abstract void InitView();

		protected virtual void CreateBinding()
		{
		}

		private void DismissKeyboard()
		{
			var g = new UITapGestureRecognizer(() => View.EndEditing(true));
			g.CancelsTouchesInView = false; //for iOS5

			View.AddGestureRecognizer(g);
		}


		protected virtual bool OnTextFieldEditting(UIView textField)
		{
			SetTextFieldPosition(textField);

			if (KeyboardHelper.IsKeyBoardVisible)
			{
				KeyboardHelper.OnKeyboardChanged(true);
			}
			return true;
		}

		protected virtual void SetTextFieldPosition(UIView textField)
		{
			if (textField == null) return;

			if (IsViewMoved)
			{
				var relativeFrameToScrollView =
					View.Superview.ConvertRectToView(View.Frame, KeyboardHelper.ViewToCenterOnKeyboardShown);
				KeyboardHelper.TextFieldPosition = new Position((float) relativeFrameToScrollView.X,
					(float) (relativeFrameToScrollView.Y + relativeFrameToScrollView.Height));
			}
			else
			{
				var relativeFrameWithContentView = textField.Superview.ConvertRectToView(textField.Frame,
					KeyboardHelper.ViewToCenterOnKeyboardShown);
				KeyboardHelper.TextFieldPosition = new Position((float) relativeFrameWithContentView.X,
					(float) (relativeFrameWithContentView.Y + textField.Frame.Height));
			}
		}
	}
}