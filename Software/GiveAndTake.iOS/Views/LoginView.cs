﻿using Facebook.CoreKit;
using Facebook.LoginKit;
using Foundation;
using GiveAndTake.Core.Helpers;
using GiveAndTake.Core.Models;
using GiveAndTake.Core.ViewModels;
using GiveAndTake.iOS.Controls;
using GiveAndTake.iOS.Helpers;
using GiveAndTake.iOS.Interfaces;
using GiveAndTake.iOS.Views.Base;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Commands;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using UIKit;
using ColorHelper = GiveAndTake.iOS.Helpers.ColorHelper;

namespace GiveAndTake.iOS.Views
{
	[MvxRootPresentation]
    public class LoginView : BaseView, ITextViewDelegate
	{
        private UIImageView _logoImage;
        private UIButton _customLoginFacebookButton;
        private DisabledInteractionTextView _tvTermAndCondition;
        private UIImageView _contentView;
        private PopupItemLabel _loginTitle;
		private bool _isFirstTime = true;
		private string _acceptTermAndConditionInHtml;

		public string AcceptTermAndConditionInHtml
		{
			get => _acceptTermAndConditionInHtml;
			set
			{
				_acceptTermAndConditionInHtml = value;
				_tvTermAndCondition.AttributedText = UIHelper.GetAttributedStringFromHtml(value, DimensionHelper.BigTextSize);
				_tvTermAndCondition.TextAlignment = UITextAlignment.Center;
			}
		}

		public IMvxCommand<User> LoginCommand { get; set; }
		public IMvxCommand ShowConditionCommand { get; set; }

		public bool ShouldInteractWithUrl(UITextView uiTextView, NSUrl nsUrl, NSRange arg3)
		{
			ShowConditionCommand?.Execute();
			return true;
		}

		protected override void InitView()
        {
            InitBackground();
            InitContent();
        }
        
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

			ResolutionHelper.InitStaticVariable();

	        Profile.Notifications.ObserveDidChange((sender, e) => {
		        if (e.NewProfile == null)
		        {
			        return;
		        }
		        if (_isFirstTime)
		        {
					_isFirstTime = false;
			        var facebookProfile = e.NewProfile;
					LoginCommand?.Execute(GetUserProfile(facebookProfile));
				}
			});
		}

        protected override void CreateBinding()
        {
            base.CreateBinding();

            var set = this.CreateBindingSet<LoginView, LoginViewModel>();

            set.Bind(this)
                .For(v => v.LoginCommand)
                .To(vm => vm.LoginCommand);

            set.Bind(this)
	            .For(v => v.ShowConditionCommand)
	            .To(vm => vm.ShowConditionCommand);

	        set.Bind(_loginTitle)
		        .To(vm => vm.LoginTitle);

	        set.Bind(this)
		        .For(v => v.AcceptTermAndConditionInHtml)
		        .To(vm => vm.AcceptTermAndConditionInHtml);

			set.Apply();
        }

        private void InitBackground()
        {
            _contentView = UIHelper.CreateImageView(ResolutionHelper.Width, ResolutionHelper.Height, UIColor.White, ImageHelper.LoginBackground);
            _contentView.UserInteractionEnabled = true;

            View.Add(_contentView);

            View.AddConstraints(new[]
            {
                NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, View,
                    NSLayoutAttribute.CenterY, 1, 0),
                NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, View,
                    NSLayoutAttribute.CenterX, 1, 0)
            });
        }

        private void InitContent()
        {
            _logoImage = UIHelper.CreateImageView(DimensionHelper.LoginLogoWidth, DimensionHelper.LoginLogoHeight, UIColor.White, ImageHelper.LoginLogo);
            _loginTitle = UIHelper.CreateLabel(ColorHelper.Blue, DimensionHelper.LoginTitleTextSize);
            _customLoginFacebookButton = UIHelper.CreateImageButton(DimensionHelper.LoginButtonHeight, DimensionHelper.LoginButtonWidth, ImageHelper.FacebookButton);

            _contentView.AddSubviews(_logoImage, _loginTitle, _customLoginFacebookButton);

            _contentView.AddConstraints(new[]
            {
                NSLayoutConstraint.Create(_loginTitle, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _contentView,
                    NSLayoutAttribute.CenterY, 1, - DimensionHelper.MarginNormal),
                NSLayoutConstraint.Create(_loginTitle, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _contentView,
                    NSLayoutAttribute.CenterX, 1, 0),

                NSLayoutConstraint.Create(_logoImage, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _loginTitle,
                    NSLayoutAttribute.Top, 1, - DimensionHelper.MarginNormal),
                NSLayoutConstraint.Create(_logoImage, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _contentView,
                    NSLayoutAttribute.CenterX, 1, 0),

                NSLayoutConstraint.Create(_customLoginFacebookButton, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _contentView,
                    NSLayoutAttribute.CenterY, 1, DimensionHelper.MarginShort),
                NSLayoutConstraint.Create(_customLoginFacebookButton, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _contentView,
                    NSLayoutAttribute.CenterX, 1, 0),
            });
            _customLoginFacebookButton.AddGestureRecognizer(new UITapGestureRecognizer(LoginToFacebook));

			_tvTermAndCondition = UIHelper.CreateDisabledInteractionTextView(ColorHelper.Black, DimensionHelper.BigTextSize);
			_tvTermAndCondition.Editable = false;
			_tvTermAndCondition.Delegate = new DisabledInteractionTextViewDelegate(this);
			_tvTermAndCondition.ScrollEnabled = false;
			_tvTermAndCondition.TextContainerInset = UIEdgeInsets.Zero;
			_tvTermAndCondition.ContentInset = UIEdgeInsets.Zero;
			_tvTermAndCondition.ClipsToBounds = false;
			_tvTermAndCondition.ScrollIndicatorInsets = UIEdgeInsets.Zero;
			_tvTermAndCondition.WeakLinkTextAttributes = new NSDictionary(UIStringAttributeKey.ForegroundColor, ColorHelper.ColorPrimary);
			_contentView.AddSubviews(_tvTermAndCondition);

			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_tvTermAndCondition, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _customLoginFacebookButton,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.BigMargin),
				NSLayoutConstraint.Create(_tvTermAndCondition, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_tvTermAndCondition, NSLayoutAttribute.Width, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Width, 1, - 2* DimensionHelper.BigMargin),
			});
		}

        private void LoginToFacebook()
        {
            new DebouncerHelper().Debouce((() =>
            {
	            var manager = new LoginManager();
	            manager.LogInWithReadPermissions(new[] { "public_profile", "email" }, this, (result, error) => {});
			}));
        }

		private User GetUserProfile(Profile facebookProfile)
        {
            var userProfile = new User
            {
                FirstName = facebookProfile.FirstName,
                LastName = facebookProfile.LastName,
	            DisplayName = facebookProfile.Name,
                UserName = facebookProfile.Name,
                AvatarUrl = GetProfilePicture(facebookProfile.UserID),
                SocialAccountId = facebookProfile.UserID
            };
            return userProfile;
        }

		private string GetProfilePicture(string profileId) => $"https://graph.facebook.com/{profileId}/picture?type=large";
    }
}
