﻿using GiveAndTake.Core.ViewModels.Popup;
using GiveAndTake.iOS.Helpers;
using GiveAndTake.iOS.Views.Base;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Binding.Views.Gestures;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using UIKit;

namespace GiveAndTake.iOS.Views.Popups
{
	[MvxModalPresentation(ModalPresentationStyle = UIModalPresentationStyle.OverCurrentContext, ModalTransitionStyle = UIModalTransitionStyle.CrossDissolve)]
	public class PopupAppUpdateView : BaseView
	{
		private UIView _contentView;
		private UIView _overlayView;
		private UILabel _titleLabel;
		private UILabel _messageLabel;
		private UIButton _submitButton;
		private UIImageView _logoImage;

		protected override void InitView()
		{
			HeaderBar.Hidden = true;
			View.BackgroundColor = UIColor.Clear;

			_overlayView = UIHelper.CreateView(0, 0, UIColor.Black.ColorWithAlpha(0.7f));

			View.Add(_overlayView);
			View.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_overlayView, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, View, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_overlayView, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, View, NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_overlayView, NSLayoutAttribute.Width, NSLayoutRelation.Equal, View, NSLayoutAttribute.Width, 1, 0),
				NSLayoutConstraint.Create(_overlayView, NSLayoutAttribute.Height, NSLayoutRelation.Equal, View, NSLayoutAttribute.Height, 1, 0),
			});

			_contentView = UIHelper.CreateView(0, DimensionHelper.PopupContentWidth, UIColor.White, DimensionHelper.PopupContentRadius);
			_contentView.AddGestureRecognizer(new UITapGestureRecognizer { CancelsTouchesInView = true });

			_overlayView.Add(_contentView);
			_overlayView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _overlayView, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _overlayView, NSLayoutAttribute.CenterY, 1, 0)
			});

			_logoImage = UIHelper.CreateImageView(DimensionHelper.LoginLogoWidth, DimensionHelper.LoginLogoHeight, UIColor.White, ImageHelper.LoginLogo);
			_contentView.Add(_logoImage);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_logoImage, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Top, 1, DimensionHelper.BigMargin),
				NSLayoutConstraint.Create(_logoImage, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.CenterX, 1, 0),
			});

			_titleLabel = UIHelper.CreateLabel(UIColor.Black, DimensionHelper.MediumTextSize);
			_titleLabel.TextAlignment = UITextAlignment.Center;
			_contentView.Add(_titleLabel);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_titleLabel, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _logoImage, NSLayoutAttribute.Bottom, 1, DimensionHelper.LargeMargin),
				NSLayoutConstraint.Create(_titleLabel, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.CenterX, 1, 0)
			});

			_messageLabel = UIHelper.CreateLabel(ColorHelper.DarkGray, DimensionHelper.MediumTextSize);
			_messageLabel.TextAlignment = UITextAlignment.Center;
			_contentView.Add(_messageLabel);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_messageLabel, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _titleLabel,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.SettingButtonMargin),
				NSLayoutConstraint.Create(_messageLabel, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.CenterX, 1, 0)
			});

			_submitButton = UIHelper.CreateButton(DimensionHelper.PopupButtonHeight,
				DimensionHelper.PopupMessageButtonWidth,
				ColorHelper.Blue,
				UIColor.White,
				DimensionHelper.PopupMediumTextSize,
				DimensionHelper.PopupButtonHeight / 2);
			_contentView.Add(_submitButton);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_submitButton, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _messageLabel,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.SettingButtonMargin),
				NSLayoutConstraint.Create(_submitButton, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _submitButton,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.BigMargin)
			});
		}

		protected override void CreateBinding()
		{
			base.CreateBinding();

			var bindingSet = this.CreateBindingSet<PopupAppUpdateView, PopupAppUpdateViewModel>();

			bindingSet.Bind(_titleLabel)
				.To(vm => vm.Title);

			bindingSet.Bind(_messageLabel)
				.To(vm => vm.Message);

			bindingSet.Bind(_submitButton)
				.For("Title")
				.To(vm => vm.UpdateButtonTitle);

			bindingSet.Bind(_submitButton.Tap())
				.For(v => v.Command)
				.To(vm => vm.UpdateCommand);

			bindingSet.Bind(_overlayView.Tap())
				.For(v => v.Command)
				.To(vm => vm.CloseCommand);

			bindingSet.Apply();
		}
	}
}