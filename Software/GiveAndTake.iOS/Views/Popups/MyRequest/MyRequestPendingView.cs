﻿using GiveAndTake.Core;
using GiveAndTake.Core.ViewModels.Popup.MyRequest;
using GiveAndTake.iOS.Helpers;
using GiveAndTake.iOS.Views.Base;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Binding.Views.Gestures;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using UIKit;

namespace GiveAndTake.iOS.Views.Popups.MyRequest
{
	[MvxModalPresentation(ModalPresentationStyle = UIModalPresentationStyle.OverCurrentContext, ModalTransitionStyle = UIModalTransitionStyle.CrossDissolve)]
	public class MyRequestPendingView : BaseView
	{
		private UIView _popupView;
		private UIView _overlayView;
		private UIView _contentView;
		private UIView _contentScrollView;
		private UIScrollView _scrollView;
		private UILabel _lbMyRequestPopupTitle;
		private UILabel _lbSentTo;
		private UILabel _lbGiverFullName;
		private UILabel _lbRequestDate;
		private UILabel _lbMyRequestMessage;
		private UIButton _deletePhotoButton;
		private UIButton _btnRemoveRequest;
		private UIView _headerPopupView;
		private UIView _bottomPopupView;

		private NSLayoutConstraint _scrollViewHeightConstraint;
		private NSLayoutConstraint _contentViewHeightConstraint;

		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);
			_scrollViewHeightConstraint.Constant = _contentScrollView.Frame.Height > DimensionHelper.PopupRequestScrollPendingViewMaxHeight ? DimensionHelper.PopupRequestScrollPendingViewMaxHeight : _contentScrollView.Frame.Height;
			_scrollView.ContentSize = _contentScrollView.Frame.Size;
			_contentViewHeightConstraint.Constant = _headerPopupView.Frame.Height + _scrollViewHeightConstraint.Constant + _bottomPopupView.Frame.Height;
		}
		protected override void InitView()
		{
			View.BackgroundColor = UIColor.Clear;
			InitOverlayView();
			InitRequestContentView();
		}

		protected override void CreateBinding()
		{
			var bindingSet = this.CreateBindingSet<MyRequestPendingView, MyRequestPendingViewModel>();

			bindingSet.Bind(_lbMyRequestPopupTitle)
				.For(v => v.Text)
				.To(vm => vm.MyRequestPopupTitle);

			bindingSet.Bind(_lbSentTo)
				.For(v => v.Text)
				.To(vm => vm.SentTo);

			bindingSet.Bind(_lbGiverFullName)
				.For(v => v.Text)
				.To(vm => vm.GiverFullName);

			bindingSet.Bind(_lbGiverFullName.Tap())
				.For(v => v.Command)
				.To(vm => vm.ShowGiverProfileCommand);

			bindingSet.Bind(_lbRequestDate)
				.For(v => v.Text)
				.To(vm => vm.RequestDate);

			bindingSet.Bind(_lbMyRequestMessage)
				.To(vm => vm.MyRequestMessage);

			bindingSet.Bind(_btnRemoveRequest)
				.For("Title")
				.To(vm => vm.BtnRemoveRequestTitle);

			bindingSet.Bind(_btnRemoveRequest.Tap())
				.For(v => v.Command)
				.To(vm => vm.RemoveRequestCommand);

			bindingSet.Bind(_deletePhotoButton.Tap())
				.For(v => v.Command)
				.To(vm => vm.CloseCommand);

			bindingSet.Apply();
		}

		private void InitOverlayView()
		{
			_overlayView = UIHelper.CreateView(0, 0, UIColor.Black.ColorWithAlpha(0.7f));

			View.Add(_overlayView);
			View.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_overlayView, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, View, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_overlayView, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, View, NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_overlayView, NSLayoutAttribute.Width, NSLayoutRelation.Equal, View, NSLayoutAttribute.Width, 1, 0),
				NSLayoutConstraint.Create(_overlayView, NSLayoutAttribute.Height, NSLayoutRelation.Equal, View, NSLayoutAttribute.Height, 1, 0),
			});
		}

		private void InitRequestContentView()
		{
			_popupView = UIHelper.CreateView(0, 0, UIColor.White, DimensionHelper.PopupContentRadius);
			_popupView.AddGestureRecognizer(new UITapGestureRecognizer { CancelsTouchesInView = true });

			_overlayView.Add(_popupView);
			_overlayView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_popupView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _overlayView, NSLayoutAttribute.Left, 1, DimensionHelper.BigMargin),
				NSLayoutConstraint.Create(_popupView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _overlayView, NSLayoutAttribute.Right, 1, -DimensionHelper.BigMargin),
				NSLayoutConstraint.Create(_popupView, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _overlayView, NSLayoutAttribute.CenterY, 1, 0),
			});

			_contentView = UIHelper.CreateView(0, 0, UIColor.White, 0);
			_contentViewHeightConstraint = NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, 0);

			_popupView.AddSubview(_contentView);
			_popupView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _popupView, NSLayoutAttribute.Left, 1, DimensionHelper.DefaultMargin),
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _popupView, NSLayoutAttribute.Right, 1, -DimensionHelper.DefaultMargin),
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _popupView, NSLayoutAttribute.Top, 1, DimensionHelper.MediumMargin),
				_contentViewHeightConstraint,
				NSLayoutConstraint.Create(_popupView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.Bottom, 1, DimensionHelper.MediumMargin),
			});

			_headerPopupView = UIHelper.CreateView(0, 0, UIColor.Clear);
			_contentView.AddSubview(_headerPopupView);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_headerPopupView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_headerPopupView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.Left, 1, DimensionHelper.DefaultMargin),
				NSLayoutConstraint.Create(_headerPopupView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.Right, 1, -DimensionHelper.DefaultMargin),
			});

			_lbMyRequestPopupTitle = UIHelper.CreateLabel(UIColor.Black, DimensionHelper.PopupRequestTitleTextSize);
			_headerPopupView.Add(_lbMyRequestPopupTitle);
			_headerPopupView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_lbMyRequestPopupTitle, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _headerPopupView, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_lbMyRequestPopupTitle, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _headerPopupView, NSLayoutAttribute.Top, 1, 0)
			});
			_deletePhotoButton = UIHelper.CreateImageButton(DimensionHelper.DeletePhotoButtonWidth,
				DimensionHelper.DeletePhotoButtonWidth, ImageHelper.DeleteRequestDetailButton);
			_headerPopupView.AddSubview(_deletePhotoButton);
			_headerPopupView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_deletePhotoButton, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _headerPopupView,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_deletePhotoButton, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _headerPopupView,
					NSLayoutAttribute.Right, 1, 0)
			});
			_lbSentTo = UIHelper.CreateLabel(UIColor.DarkGray, DimensionHelper.MediumTextSize);
			_headerPopupView.AddSubview(_lbSentTo);
			_headerPopupView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_lbSentTo, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _lbMyRequestPopupTitle,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.MarginNormal),
				NSLayoutConstraint.Create(_lbSentTo, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _headerPopupView,
					NSLayoutAttribute.Left, 1, 0)
			});

			_lbGiverFullName = UIHelper.CreateLabel(UIColor.Black, DimensionHelper.MediumTextSize);
			_headerPopupView.AddSubview(_lbGiverFullName);
			_headerPopupView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_lbGiverFullName, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _lbSentTo,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_lbGiverFullName, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _lbSentTo,
					NSLayoutAttribute.Right, 1, DimensionHelper.DefaultMargin)
			});

			_lbRequestDate = UIHelper.CreateLabel(UIColor.DarkGray, DimensionHelper.SmallTextSize);
			_headerPopupView.AddSubview(_lbRequestDate);
			_headerPopupView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_lbRequestDate, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _lbGiverFullName,
					NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_lbRequestDate, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _lbSentTo,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_headerPopupView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _lbRequestDate,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.MediumMargin)
			});


			_scrollView = UIHelper.CreateScrollView(0, 0);

			_scrollViewHeightConstraint = NSLayoutConstraint.Create(_scrollView, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null,
					NSLayoutAttribute.NoAttribute, 1, 0);

			_contentView.AddSubview(_scrollView);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_scrollView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _headerPopupView,
					NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_scrollView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_scrollView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Right, 1, 0),
				_scrollViewHeightConstraint

			});
			_contentScrollView = UIHelper.CreateView(0, 0, UIColor.White, 0);
			_scrollView.AddSubview(_contentScrollView);
			_scrollView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_contentScrollView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _scrollView,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_contentScrollView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _scrollView,
					NSLayoutAttribute.Left, 1, DimensionHelper.DefaultMargin),
				NSLayoutConstraint.Create(_contentScrollView, NSLayoutAttribute.Width, NSLayoutRelation.Equal, _scrollView,
					NSLayoutAttribute.Width, 1, -2* DimensionHelper.DefaultMargin),

			});
			_lbMyRequestMessage = UIHelper.CreateLabel(UIColor.DarkGray, DimensionHelper.PostDescriptionTextSize, FontType.Regular);
			_contentScrollView.Add(_lbMyRequestMessage);
			_contentScrollView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_lbMyRequestMessage, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _contentScrollView,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_lbMyRequestMessage, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _contentScrollView,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_lbMyRequestMessage, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _contentScrollView,
					NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_contentScrollView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _lbMyRequestMessage,
					NSLayoutAttribute.Bottom, 1, 0),

			});

			_bottomPopupView = UIHelper.CreateView(0, 0, UIColor.Clear);
			_contentView.AddSubview(_bottomPopupView);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_bottomPopupView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _scrollView, NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_bottomPopupView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.Left, 1, DimensionHelper.DefaultMargin),
				NSLayoutConstraint.Create(_bottomPopupView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.Right, 1, -DimensionHelper.DefaultMargin),
			});

			_btnRemoveRequest = UIHelper.CreateAlphaButton(0,
					DimensionHelper.PopupRequestButtonHeight,
					ColorHelper.LightBlue, ColorHelper.DarkBlue, DimensionHelper.MediumTextSize,
					UIColor.White, UIColor.White, ColorHelper.LightBlue, ColorHelper.DarkBlue,
					true, true, FontType.Light);
			_bottomPopupView.Add(_btnRemoveRequest);
			_bottomPopupView.AddConstraints(new[]
				{
				NSLayoutConstraint.Create(_btnRemoveRequest, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _bottomPopupView,
					NSLayoutAttribute.Top, 1, DimensionHelper.MediumMargin),
				NSLayoutConstraint.Create(_btnRemoveRequest, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _bottomPopupView,
					NSLayoutAttribute.CenterX, 1, -5*DimensionHelper.BigMargin/2),
				NSLayoutConstraint.Create(_btnRemoveRequest, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _bottomPopupView,
					NSLayoutAttribute.CenterX, 1, 5*DimensionHelper.BigMargin/2),
				NSLayoutConstraint.Create(_bottomPopupView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _btnRemoveRequest, 
					NSLayoutAttribute.Bottom, 1, 0),
				});
		}
	}
}