﻿using GiveAndTake.Core;
using GiveAndTake.Core.ViewModels.Popup.OtherRequest;
using GiveAndTake.iOS.Controls;
using GiveAndTake.iOS.Helpers;
using GiveAndTake.iOS.Views.Base;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Binding.Views.Gestures;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using UIKit;

namespace GiveAndTake.iOS.Views.Popups.OtherRequest
{
	[MvxModalPresentation(ModalPresentationStyle = UIModalPresentationStyle.OverCurrentContext, ModalTransitionStyle = UIModalTransitionStyle.CrossDissolve)]
	public class OtherRequestPendingView : BaseView
	{
		private UIView _popupView;
		private UIView _overlayView;
		private UIView _contentView;
		private UIView _contentScrollView;
		private UIScrollView _scrollView;
		private CustomMvxCachedImageView _imagePost;
		private CustomMvxCachedImageView _imgTakerAvatar;
		private UILabel _lbPopupTitle;
		private UILabel _lbTakerFullName;
		private UILabel _lbRequestCreatedDate;
		private UILabel _lbRequestMessage;
		private UIButton _deletePhotoButton;
		private UIButton _btnRejectRequest;
		private UIButton _btnApproveRequest;
		private UIView _headerPopupView;
		private UIView _bottomPopupView;

		private NSLayoutConstraint _scrollViewHeightConstraint;
		private NSLayoutConstraint _contentViewHeightConstraint;

		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);
			_scrollViewHeightConstraint.Constant = _contentScrollView.Frame.Height > DimensionHelper.PopupRequestScrollPendingViewMaxHeight ? DimensionHelper.PopupRequestScrollPendingViewMaxHeight : _contentScrollView.Frame.Height;
			_scrollView.ContentSize = _contentScrollView.Frame.Size;
			_contentViewHeightConstraint.Constant = _headerPopupView.Frame.Height + _scrollViewHeightConstraint.Constant + _bottomPopupView.Frame.Height;
		}
		protected override void InitView()
		{
			View.BackgroundColor = UIColor.Clear;
			InitOverlayView();
			InitRequestContentView();
		}

		protected override void CreateBinding()
		{
			var bindingSet = this.CreateBindingSet<OtherRequestPendingView, OtherRequestPendingViewModel>();

			bindingSet.Bind(_lbPopupTitle)
				.For(v => v.Text)
				.To(vm => vm.PopupTitle);

			bindingSet.Bind(_imagePost)
				.For(v => v.ImageUrl)
				.To(vm => vm.PostUrl);

			bindingSet.Bind(_imagePost.Tap())
				.For(v => v.Command)
				.To(vm => vm.ShowPostDetailCommand);

			bindingSet.Bind(_imgTakerAvatar)
				.For(v => v.ImageUrl)
				.To(vm => vm.TakerAvatarUrl);

			bindingSet.Bind(_imgTakerAvatar.Tap())
				.For(v => v.Command)
				.To(vm => vm.ShowTakerProfileCommand);

			bindingSet.Bind(_lbTakerFullName)
				.For(v => v.Text)
				.To(vm => vm.TakerFullName);

			bindingSet.Bind(_lbTakerFullName.Tap())
				.For(v => v.Command)
				.To(vm => vm.ShowTakerProfileCommand);

			bindingSet.Bind(_lbRequestCreatedDate)
				.For(v => v.Text)
				.To(vm => vm.RequestCreatedDate);

			bindingSet.Bind(_lbRequestMessage)
				.To(vm => vm.RequestMessage);

			bindingSet.Bind(_btnRejectRequest)
				.For("Title")
				.To(vm => vm.BtnRejectTitle);

			bindingSet.Bind(_btnRejectRequest.Tap())
				.For(v => v.Command)
				.To(vm => vm.RejectCommand);

			bindingSet.Bind(_btnApproveRequest)
				.For("Title")
				.To(vm => vm.BtnApproveTitle);

			bindingSet.Bind(_btnApproveRequest.Tap())
				.For(v => v.Command)
				.To(vm => vm.ApproveCommand);

			bindingSet.Bind(_deletePhotoButton.Tap())
				.For(v => v.Command)
				.To(vm => vm.CloseCommand);

			bindingSet.Apply();
		}

		private void InitOverlayView()
		{
			_overlayView = UIHelper.CreateView(0, 0, UIColor.Black.ColorWithAlpha(0.7f));

			View.Add(_overlayView);
			View.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_overlayView, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, View, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_overlayView, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, View, NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_overlayView, NSLayoutAttribute.Width, NSLayoutRelation.Equal, View, NSLayoutAttribute.Width, 1, 0),
				NSLayoutConstraint.Create(_overlayView, NSLayoutAttribute.Height, NSLayoutRelation.Equal, View, NSLayoutAttribute.Height, 1, 0),
			});
		}

		private void InitRequestContentView()
		{
			_popupView = UIHelper.CreateView(0, 0, UIColor.White, DimensionHelper.PopupContentRadius);
			_popupView.AddGestureRecognizer(new UITapGestureRecognizer { CancelsTouchesInView = true });

			_overlayView.Add(_popupView);
			_overlayView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_popupView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _overlayView, NSLayoutAttribute.Left, 1, DimensionHelper.BigMargin),
				NSLayoutConstraint.Create(_popupView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _overlayView, NSLayoutAttribute.Right, 1, -DimensionHelper.BigMargin),
				NSLayoutConstraint.Create(_popupView, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _overlayView, NSLayoutAttribute.CenterY, 1, 0),
			});

			_contentView = UIHelper.CreateView(0, 0, UIColor.White, 0);
			_contentViewHeightConstraint = NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, 0);

			_popupView.AddSubview(_contentView);
			_popupView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _popupView, NSLayoutAttribute.Left, 1, DimensionHelper.DefaultMargin),
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _popupView, NSLayoutAttribute.Right, 1, -DimensionHelper.DefaultMargin),
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _popupView, NSLayoutAttribute.Top, 1, DimensionHelper.MediumMargin),
				_contentViewHeightConstraint,
				NSLayoutConstraint.Create(_popupView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.Bottom, 1, DimensionHelper.MediumMargin),
			});

			_headerPopupView = UIHelper.CreateView(0, 0, UIColor.Clear);
			_contentView.AddSubview(_headerPopupView);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_headerPopupView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_headerPopupView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.Left, 1, DimensionHelper.DefaultMargin),
				NSLayoutConstraint.Create(_headerPopupView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.Right, 1, -DimensionHelper.DefaultMargin),
			});

			_lbPopupTitle = UIHelper.CreateLabel(UIColor.Black, DimensionHelper.PopupRequestTitleTextSize);
			_headerPopupView.Add(_lbPopupTitle);
			_headerPopupView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_lbPopupTitle, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _headerPopupView, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_lbPopupTitle, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _headerPopupView, NSLayoutAttribute.Top, 1, 0)
			});
			_deletePhotoButton = UIHelper.CreateImageButton(DimensionHelper.DeletePhotoButtonWidth,
				DimensionHelper.DeletePhotoButtonWidth, ImageHelper.DeleteRequestDetailButton);
			_headerPopupView.AddSubview(_deletePhotoButton);
			_headerPopupView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_deletePhotoButton, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _headerPopupView,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_deletePhotoButton, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _headerPopupView,
					NSLayoutAttribute.Right, 1, 0)
			});
			_imagePost = UIHelper.CreateCustomImageView(DimensionHelper.ImageRequestSmallSize, DimensionHelper.ImageRequestSmallSize, ImageHelper.DefaultPost, DimensionHelper.PostPhotoCornerRadius);
			_imagePost.SetPlaceHolder(ImageHelper.DefaultPost, ImageHelper.DefaultPost);
			_headerPopupView.AddSubview(_imagePost);
			_headerPopupView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_imagePost, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _lbPopupTitle,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.MarginNormal),
				NSLayoutConstraint.Create(_imagePost, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _headerPopupView,
					NSLayoutAttribute.Right, 1, 0)
			});

			_imgTakerAvatar = UIHelper.CreateCustomImageView(DimensionHelper.ImageAvatarSize, DimensionHelper.ImageAvatarSize, ImageHelper.DefaultAvatar, DimensionHelper.ImageAvatarSize / 2);
			_imgTakerAvatar.SetPlaceHolder(ImageHelper.DefaultAvatar, ImageHelper.DefaultAvatar);
			_headerPopupView.AddSubview(_imgTakerAvatar);
			_headerPopupView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_imgTakerAvatar, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _lbPopupTitle,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.MarginNormal),
				NSLayoutConstraint.Create(_imgTakerAvatar, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _headerPopupView,
					NSLayoutAttribute.Left, 1, 0)
			});

			_lbTakerFullName = UIHelper.CreateLabel(UIColor.Black, DimensionHelper.MediumTextSize);
			_headerPopupView.AddSubview(_lbTakerFullName);
			_headerPopupView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_lbTakerFullName, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _imgTakerAvatar,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_lbTakerFullName, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _imgTakerAvatar,
					NSLayoutAttribute.Right, 1, DimensionHelper.DefaultMargin)
			});

			_lbRequestCreatedDate = UIHelper.CreateLabel(UIColor.DarkGray, DimensionHelper.SmallTextSize);
			_headerPopupView.AddSubview(_lbRequestCreatedDate);
			_headerPopupView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_lbRequestCreatedDate, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _lbTakerFullName,
					NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_lbRequestCreatedDate, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _lbTakerFullName,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_headerPopupView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _lbRequestCreatedDate,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.MediumMargin)
			});


			_scrollView = UIHelper.CreateScrollView(0, 0);

			_scrollViewHeightConstraint = NSLayoutConstraint.Create(_scrollView, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null,
					NSLayoutAttribute.NoAttribute, 1, 0);

			_contentView.AddSubview(_scrollView);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_scrollView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _headerPopupView,
					NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_scrollView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_scrollView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Right, 1, 0),
				_scrollViewHeightConstraint

			});
			_contentScrollView = UIHelper.CreateView(0, 0, UIColor.White, 0);
			_scrollView.AddSubview(_contentScrollView);
			_scrollView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_contentScrollView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _scrollView,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_contentScrollView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _scrollView,
					NSLayoutAttribute.Left, 1, DimensionHelper.DefaultMargin),
				NSLayoutConstraint.Create(_contentScrollView, NSLayoutAttribute.Width, NSLayoutRelation.Equal, _scrollView,
					NSLayoutAttribute.Width, 1, -2* DimensionHelper.DefaultMargin),

			});
			_lbRequestMessage = UIHelper.CreateLabel(UIColor.DarkGray, DimensionHelper.PostDescriptionTextSize);
			_contentScrollView.Add(_lbRequestMessage);
			_contentScrollView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_lbRequestMessage, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _contentScrollView,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_lbRequestMessage, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _contentScrollView,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_lbRequestMessage, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _contentScrollView,
					NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_contentScrollView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _lbRequestMessage,
					NSLayoutAttribute.Bottom, 1, 0),

			});

			_bottomPopupView = UIHelper.CreateView(0, 0, UIColor.Clear);
			_contentView.AddSubview(_bottomPopupView);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_bottomPopupView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _scrollView, NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_bottomPopupView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.Left, 1, DimensionHelper.DefaultMargin),
				NSLayoutConstraint.Create(_bottomPopupView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.Right, 1, -DimensionHelper.DefaultMargin),
			});

			_btnRejectRequest = UIHelper.CreateAlphaButton(0,
					DimensionHelper.PopupRequestButtonHeight,
					ColorHelper.LightBlue, ColorHelper.DarkBlue, DimensionHelper.MediumTextSize,
					UIColor.White, UIColor.White, ColorHelper.LightBlue, ColorHelper.DarkBlue,
					true, true, FontType.Light);
			_bottomPopupView.Add(_btnRejectRequest);
			_bottomPopupView.AddConstraints(new[]
				{
				NSLayoutConstraint.Create(_btnRejectRequest, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _bottomPopupView,
					NSLayoutAttribute.Top, 1, DimensionHelper.MediumMargin),
				NSLayoutConstraint.Create(_btnRejectRequest, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _bottomPopupView,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_btnRejectRequest, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _bottomPopupView,
					NSLayoutAttribute.CenterX, 1, -DimensionHelper.DefaultMargin/2)
				});

			_btnApproveRequest = UIHelper.CreateAlphaButton(0,
					DimensionHelper.PopupRequestButtonHeight,
					UIColor.White, UIColor.White, DimensionHelper.MediumTextSize,
					ColorHelper.LightBlue, ColorHelper.DarkBlue, ColorHelper.LightBlue, ColorHelper.DarkBlue, true, false, FontType.Light);
			_bottomPopupView.Add(_btnApproveRequest);
			_bottomPopupView.AddConstraints(new[]
				{
				NSLayoutConstraint.Create(_btnApproveRequest, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _btnRejectRequest,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_btnApproveRequest, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _bottomPopupView,
					NSLayoutAttribute.CenterX, 1, DimensionHelper.DefaultMargin/2),
				NSLayoutConstraint.Create(_btnApproveRequest, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _bottomPopupView,
					NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_bottomPopupView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _btnRejectRequest,
					NSLayoutAttribute.Bottom, 1, 0),
				});
		}
	}
}