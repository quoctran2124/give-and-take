﻿using GiveAndTake.iOS.Helpers;
using GiveAndTake.iOS.Views.Base;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using UIKit;

namespace GiveAndTake.iOS.Views.Popups.OtherRequest
{
	[MvxModalPresentation(ModalPresentationStyle = UIModalPresentationStyle.OverCurrentContext, ModalTransitionStyle = UIModalTransitionStyle.CrossDissolve)]
	public class OtherRequestApprovedView : BaseRequestApprovedView
	{
		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);
			_scrollViewHeightConstraint.Constant = _contentScrollView.Frame.Height > DimensionHelper.PopupRequestScrollApprovedViewMaxHeight ? DimensionHelper.PopupRequestScrollApprovedViewMaxHeight : _contentScrollView.Frame.Height;
			_scrollView.ContentSize = _contentScrollView.Frame.Size;
			_contentViewHeightConstraint.Constant = _headerPopupView.Frame.Height + _scrollViewHeightConstraint.Constant;
		}
	}
}