﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using GiveAndTake.Core;
using GiveAndTake.Core.ViewModels.Popup;
using GiveAndTake.iOS.Controls;
using GiveAndTake.iOS.Helpers;
using GiveAndTake.iOS.Views.Base;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Binding.Views.Gestures;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using UIKit;

namespace GiveAndTake.iOS.Views.Popups
{
	[MvxModalPresentation(ModalPresentationStyle = UIModalPresentationStyle.OverCurrentContext, ModalTransitionStyle = UIModalTransitionStyle.CrossDissolve)]
	public class PopupChangeLanguageView : BaseView
	{
		private UIView _overlayView;
		private UIView _contentView;
		private UILabel _lbPopupTitle;
		private RadioButton _radBtnVietnamese;
		private RadioButton _radBtnEnglish;
		private UIButton _btnCancel;
		private UIButton _btnSubmit;

		protected override void InitView()
		{
			View.BackgroundColor = UIColor.Clear;
			InitOverlayView();
			InitContentView();
		}

		protected override void CreateBinding()
		{
			var bindingSet = this.CreateBindingSet<PopupChangeLanguageView, PopupChangeLanguageViewModel>();

			bindingSet.Bind(_lbPopupTitle)
				.For(v => v.Text)
				.To(vm => vm.PopupTitle);

			bindingSet.Bind(_radBtnVietnamese)
				.For(v => v.Title)
				.To(vm => vm.VnTextTitle);

			bindingSet.Bind(_radBtnVietnamese)
				.For(v => v.Checked)
				.To(vm => vm.IsVietnameseChecked);

			bindingSet.Bind(_radBtnVietnamese)
				.For(v => v.ClickCommand)
				.To(vm => vm.IsVietnameseClickCommand)
				.CommandParameter(LanguageType.vi);

			bindingSet.Bind(_radBtnEnglish)
				.For(v => v.Title)
				.To(vm => vm.EnTextTitle);

			bindingSet.Bind(_radBtnEnglish)
				.For(v => v.Checked)
				.To(vm => vm.IsEnglishChecked);

			bindingSet.Bind(_radBtnEnglish)
				.For(v => v.ClickCommand)
				.To(vm => vm.IsEnglishClickCommand)
				.CommandParameter(LanguageType.en);

			bindingSet.Bind(_btnCancel)
				.For("Title")
				.To(vm => vm.BtnCancelTitle);

			bindingSet.Bind(_btnSubmit)
				.For("Title")
				.To(vm => vm.BtnSubmitTitle);

			bindingSet.Bind(_btnSubmit.Tap())
				.For(v => v.Command)
				.To(vm => vm.SubmitCommand);

			bindingSet.Bind(_btnSubmit)
				.For("Enabled")
				.To(vm => vm.IsSubmitBtnEnabled);

			bindingSet.Bind(_btnCancel.Tap())
				.For(v => v.Command)
				.To(vm => vm.CloseCommand);

			bindingSet.Apply();
		}

		private void InitOverlayView()
		{
			_overlayView = UIHelper.CreateView(0, 0, UIColor.Black.ColorWithAlpha(0.7f));

			View.Add(_overlayView);
			View.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_overlayView, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, View, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_overlayView, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, View, NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_overlayView, NSLayoutAttribute.Width, NSLayoutRelation.Equal, View, NSLayoutAttribute.Width, 1, 0),
				NSLayoutConstraint.Create(_overlayView, NSLayoutAttribute.Height, NSLayoutRelation.Equal, View, NSLayoutAttribute.Height, 1, 0),
			});
		}
		private void InitContentView()
		{
			_contentView = UIHelper.CreateView(DimensionHelper.PopupChangeLanguageHeight, DimensionHelper.PopupContentWidth, UIColor.White, DimensionHelper.PopupContentRadius);
			_contentView.AddGestureRecognizer(new UITapGestureRecognizer { CancelsTouchesInView = true });
			_overlayView.Add(_contentView);
			_overlayView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _overlayView, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _overlayView, NSLayoutAttribute.CenterY, 1, 0)
			});

			_lbPopupTitle = UIHelper.CreateLabel(UIColor.Black, DimensionHelper.PopupRequestTitleTextSize);
			_contentView.Add(_lbPopupTitle);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_lbPopupTitle, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_lbPopupTitle, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.Top, 1, DimensionHelper.DefaultMargin)
			});

			_radBtnVietnamese = UIHelper.CreateRadioButton(0);
			_contentView.Add(_radBtnVietnamese);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_radBtnVietnamese, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _lbPopupTitle,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.MediumMargin),
				NSLayoutConstraint.Create(_radBtnVietnamese, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Left, 1, DimensionHelper.BigMargin)
			});

			_radBtnEnglish = UIHelper.CreateRadioButton(0);
			_contentView.Add(_radBtnEnglish);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_radBtnEnglish, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _radBtnVietnamese,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.MediumMargin),
				NSLayoutConstraint.Create(_radBtnEnglish, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _radBtnVietnamese,
					NSLayoutAttribute.Left, 1, 0)
			});

			_btnCancel = UIHelper.CreateAlphaButton(0,
				DimensionHelper.PopupRequestButtonHeight,
				ColorHelper.LightBlue, ColorHelper.DarkBlue, DimensionHelper.MediumTextSize,
				UIColor.White, UIColor.White, ColorHelper.LightBlue, ColorHelper.DarkBlue,
				true, true, FontType.Light);
			_contentView.Add(_btnCancel);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_btnCancel, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Bottom, 1, -DimensionHelper.MediumMargin),
				NSLayoutConstraint.Create(_btnCancel, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Left, 1, DimensionHelper.DefaultMargin),
				NSLayoutConstraint.Create(_btnCancel, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.CenterX, 1, -DimensionHelper.DefaultMargin/2)
			});

			_btnSubmit = UIHelper.CreateAlphaButton(0,
				DimensionHelper.PopupRequestButtonHeight,
				UIColor.White, UIColor.White, DimensionHelper.MediumTextSize,
				ColorHelper.LightBlue, ColorHelper.DarkBlue, ColorHelper.LightBlue, ColorHelper.DarkBlue, true, false, FontType.Light);
			_contentView.Add(_btnSubmit);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_btnSubmit, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _btnCancel,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_btnSubmit, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.CenterX, 1, DimensionHelper.DefaultMargin/2),
				NSLayoutConstraint.Create(_btnSubmit, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Right, 1, -DimensionHelper.DefaultMargin),
			});
		}
	}
}