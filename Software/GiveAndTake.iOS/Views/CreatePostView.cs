﻿using CoreGraphics;
using GiveAndTake.Core;
using GiveAndTake.Core.ViewModels;
using GiveAndTake.iOS.Controls;
using GiveAndTake.iOS.Helpers;
using GiveAndTake.iOS.Views.Base;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Commands;
using MvvmCross.Platforms.Ios.Binding.Views.Gestures;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using System;
using System.Collections.Generic;
using I18NPortable;
using UIKit;

namespace GiveAndTake.iOS.Views
{
	[MvxModalPresentation(ModalPresentationStyle = UIModalPresentationStyle.OverFullScreen,
		ModalTransitionStyle = UIModalTransitionStyle.CrossDissolve)]
	public class CreatePostView : BaseView
	{
		private UIButton _chooseProvinceCityButton;
		private UIButton _chooseCategoryButton;
		private UITextField _postTitleTextField;
        private PlaceholderTextView _postDescriptionTextView;
		private UIButton _btnTakePicture;
		private UIButton _btnChoosePicture;
		private UIButton _btnCancel;
		private UIButton _btnSubmit;
		private UILabel _selectedImageTextView;
		private UIScrollView _scrollView;
	    private UIView _contentView;
		private string _selectedImage;
		private nfloat _postDescriptionTextViewHeight;

		private static II18N Strings => I18N.Current;

		public string SelectedImage
		{
			get => _selectedImage;
			set
			{
				_selectedImage = value;
				UpdateSelectedImageTextView();
			}
		}

		public IMvxCommand<List<byte[]>> ImageCommand { get; set; }
		public IMvxAsyncCommand CloseCommand { get; set; }

		protected override void InitView()
		{
			HeaderBar.BackButtonIsShown = true;

			_scrollView = UIHelper.CreateScrollView(0, 0, UIColor.Clear);
			View.AddSubview(_scrollView);
			View.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_scrollView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, HeaderBar,
					NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_scrollView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_scrollView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_scrollView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Bottom, 1, -UIApplication.SharedApplication.KeyWindow.SafeAreaInsets.Bottom),
			});

			_contentView = UIHelper.CreateView(0, 0, UIColor.Clear);
			_scrollView.AddSubview(_contentView);
			_scrollView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _scrollView,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _scrollView,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Width, NSLayoutRelation.Equal, _scrollView,
					NSLayoutAttribute.Width, 1, 0),
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Height, NSLayoutRelation.Equal, _scrollView,
					NSLayoutAttribute.Height, 1, 0),
			});

			_btnCancel = UIHelper.CreateAlphaButton(0, DimensionHelper.CreatePostButtonHeight,
				ColorHelper.LightBlue, ColorHelper.DarkBlue, DimensionHelper.MediumTextSize,
				UIColor.White, UIColor.White, ColorHelper.LightBlue, ColorHelper.DarkBlue,
				true, true, FontType.Light);

			_contentView.Add(_btnCancel);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_btnCancel, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Bottom, 1, -DimensionHelper.BigMargin),
				NSLayoutConstraint.Create(_btnCancel, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Left, 1, DimensionHelper.DefaultMargin),
				NSLayoutConstraint.Create(_btnCancel, NSLayoutAttribute.Width, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Width, 0.5f, -(3 * DimensionHelper.DefaultMargin / 2)),
			});

			_btnSubmit = UIHelper.CreateAlphaButton(0, DimensionHelper.CreatePostButtonHeight,
				UIColor.White, UIColor.White, DimensionHelper.MediumTextSize,
				ColorHelper.LightBlue, ColorHelper.DarkBlue, ColorHelper.LightBlue, ColorHelper.DarkBlue, true, false, FontType.Light);

			_contentView.Add(_btnSubmit);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_btnSubmit, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Bottom, 1, -DimensionHelper.BigMargin),
				NSLayoutConstraint.Create(_btnSubmit, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Right, 1, -DimensionHelper.DefaultMargin),
				NSLayoutConstraint.Create(_btnSubmit, NSLayoutAttribute.Width, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Width, 0.5f, -(3 * DimensionHelper.DefaultMargin / 2)),
			});

			_selectedImageTextView = UIHelper.CreateLabel(ColorHelper.Gray, DimensionHelper.BigTextSize, FontType.Light);

			_contentView.Add(_selectedImageTextView);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_selectedImageTextView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _btnSubmit,
					NSLayoutAttribute.Top, 1, -DimensionHelper.MarginNormal),
				NSLayoutConstraint.Create(_selectedImageTextView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Left, 1, DimensionHelper.DefaultMargin)
			});

			_btnChoosePicture = UIHelper.CreateImageButton(DimensionHelper.PictureButtonHeight, DimensionHelper.PictureButtonWidth, ImageHelper.ChoosePictureButton);
			_btnChoosePicture.UserInteractionEnabled = true;
			_btnChoosePicture.TouchUpInside += HandleSelectImage;

			_contentView.Add(_btnChoosePicture);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_btnChoosePicture, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _selectedImageTextView,
					NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_btnChoosePicture, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Right, 1, -DimensionHelper.DefaultMargin)
			});

			_btnTakePicture = UIHelper.CreateImageButton(DimensionHelper.PictureButtonHeight, DimensionHelper.PictureButtonWidth, ImageHelper.TakePictureButton);

			_contentView.Add(_btnTakePicture);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_btnTakePicture, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _selectedImageTextView,
					NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_btnTakePicture, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _btnChoosePicture,
					NSLayoutAttribute.Left, 1, -DimensionHelper.BigMargin)
			});

			_chooseProvinceCityButton = UIHelper.CreateButton(DimensionHelper.DropDownButtonHeight, 0,
				ColorHelper.DefaultEditTextFieldColor, UIColor.Black, DimensionHelper.MediumTextSize, DimensionHelper.DropDownButtonHeight / 2, FontType.Light);

			_chooseProvinceCityButton.Layer.BorderColor = ColorHelper.Gray.CGColor;
			_chooseProvinceCityButton.Layer.BorderWidth = 1;
			_chooseProvinceCityButton.HorizontalAlignment = UIControlContentHorizontalAlignment.Left;
			_chooseProvinceCityButton.ContentEdgeInsets = new UIEdgeInsets(10, 15, 10, 10);

			_contentView.Add(_chooseProvinceCityButton);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_chooseProvinceCityButton, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Top, 1, DimensionHelper.DefaultMargin),
				NSLayoutConstraint.Create(_chooseProvinceCityButton, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Left, 1, DimensionHelper.DefaultMargin),
				NSLayoutConstraint.Create(_chooseProvinceCityButton, NSLayoutAttribute.Width, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Width, 0.5f, -(3 * DimensionHelper.DefaultMargin / 2)),
			});

			_chooseCategoryButton = UIHelper.CreateButton(DimensionHelper.DropDownButtonHeight, 0,
				ColorHelper.DefaultEditTextFieldColor, UIColor.Black, DimensionHelper.MediumTextSize, DimensionHelper.DropDownButtonHeight / 2, FontType.Light);

			_chooseCategoryButton.Layer.BorderColor = ColorHelper.Gray.CGColor;
			_chooseCategoryButton.Layer.BorderWidth = 1;
			_chooseCategoryButton.HorizontalAlignment = UIControlContentHorizontalAlignment.Left;
			_chooseCategoryButton.ContentEdgeInsets = new UIEdgeInsets(10, 15, 10, 10);

			_contentView.Add(_chooseCategoryButton);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_chooseCategoryButton, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Top, 1, DimensionHelper.DefaultMargin),
				NSLayoutConstraint.Create(_chooseCategoryButton, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Right, 1, -DimensionHelper.DefaultMargin),
				NSLayoutConstraint.Create(_chooseCategoryButton, NSLayoutAttribute.Width, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Width, 0.5f, -(3 * DimensionHelper.DefaultMargin / 2)),
			});

			_postTitleTextField = UIHelper.CreateTextField(DimensionHelper.DropDownButtonHeight, 0,
				ColorHelper.LightGray, ColorHelper.Gray, DimensionHelper.RoundCorner, DimensionHelper.MediumTextSize, FontType.Light);

			UIView paddingView = new UIView(new CGRect(0, 0, 15, _postTitleTextField.Frame.Height));
			_postTitleTextField.LeftView = paddingView;
			_postTitleTextField.LeftViewMode = UITextFieldViewMode.Always;
			_postTitleTextField.ShouldReturn = (textField) =>
			{
				textField.ResignFirstResponder();
				return true;
			};

			_contentView.Add(_postTitleTextField);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_postTitleTextField, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _chooseProvinceCityButton,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.DefaultMargin),
				NSLayoutConstraint.Create(_postTitleTextField, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Left, 1, DimensionHelper.DefaultMargin),
				NSLayoutConstraint.Create(_postTitleTextField, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Right, 1, -DimensionHelper.DefaultMargin)
			});

			_postDescriptionTextView = UIHelper.CreateTextView(DimensionHelper.PostDescriptionTextViewHeight, DimensionHelper.CreatePostEditTextWidth,
				ColorHelper.LightGray, ColorHelper.Gray, DimensionHelper.RoundCorner, ColorHelper.Gray, DimensionHelper.MediumTextSize, FontType.Light);


			_contentView.Add(_postDescriptionTextView);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_postDescriptionTextView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _btnTakePicture,
					NSLayoutAttribute.Top, 1, -DimensionHelper.DefaultMargin),
				NSLayoutConstraint.Create(_postDescriptionTextView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Left, 1, DimensionHelper.DefaultMargin),
				NSLayoutConstraint.Create(_postDescriptionTextView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Right, 1, -DimensionHelper.DefaultMargin),
				NSLayoutConstraint.Create(_postDescriptionTextView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _postTitleTextField,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.DefaultMargin)
			});

            RegisterViewToCenterOnKeyboardShown(_contentView);
		    IsViewMoved = false;

			_postTitleTextField.ShouldBeginEditing += OnTextFieldEditting;
            _postDescriptionTextView.ShouldBeginEditing += OnTextFieldEditting;
			_postDescriptionTextView.ShouldEndEditing += OnTextFieldEndEditting;
		}

		private bool OnTextFieldEndEditting(UITextView textview)
		{
			var spaceAboveKeyboard = _contentView.Frame.Height - KeyboardHelper.KeyboardFrameHeight;
			if (textview is PlaceholderTextView)
			{
				_postDescriptionTextView.Frame = new CGRect(_postDescriptionTextView.Frame.X,
					_postDescriptionTextView.Frame.Y - (_postDescriptionTextViewHeight - spaceAboveKeyboard), _postDescriptionTextView.Frame.Width, _postDescriptionTextViewHeight);
			}

			return true;
		}

		protected override void SetTextFieldPosition(UIView textField)
		{
			// Re-register after cancel back action
			RegisterViewToCenterOnKeyboardShown(_contentView);

			_postDescriptionTextViewHeight = _postDescriptionTextView.Frame.Height;
			if (textField is PlaceholderTextView)
			{
				var spaceAboveKeyboard = _contentView.Frame.Height - KeyboardHelper.KeyboardFrameHeight;
				_postDescriptionTextView.Frame = new CGRect(_postDescriptionTextView.Frame.X,
					_postDescriptionTextView.Frame.Y + (_postDescriptionTextViewHeight - spaceAboveKeyboard), _postDescriptionTextView.Frame.Width, spaceAboveKeyboard);

			}

			base.SetTextFieldPosition(textField);
		}

		protected override void CreateBinding()
		{
			var bindingSet = this.CreateBindingSet<CreatePostView, CreatePostViewModel>();

			bindingSet.Bind(_btnTakePicture)
				.To(vm => vm.TakePictureCommand);

			bindingSet.Bind(_btnSubmit.Tap())
				.For(v => v.Command)
				.To(vm => vm.SubmitCommand);

			bindingSet.Bind(_postTitleTextField)
				.For("Text")
				.To(vm => vm.PostTitle);

			bindingSet.Bind(_chooseProvinceCityButton.Tap())
				.For(v => v.Command)
				.To(vm => vm.ShowProvinceCityCommand);

			bindingSet.Bind(_chooseProvinceCityButton)
				.For("Title")
				.To(vm => vm.ProvinceCity);

			bindingSet.Bind(_chooseCategoryButton.Tap())
				.For(v => v.Command)
				.To(vm => vm.ShowCategoriesCommand);

			bindingSet.Bind(_chooseCategoryButton)
				.For("Title")
				.To(vm => vm.Category);

			bindingSet.Bind(_btnCancel.Tap())
				.For(v => v.Command)
				.To(vm => vm.BackPressedCommand);

			bindingSet.Bind(this)
				.For(v => v.ImageCommand)
				.To(vm => vm.ImageCommand);

			bindingSet.Bind(_postTitleTextField)
				.For(v => v.Placeholder)
				.To(vm => vm.PostTitlePlaceHolder);

			bindingSet.Bind(_postDescriptionTextView)
				.For(v => v.Text)
				.To(vm => vm.PostDescription);

			bindingSet.Bind(_postDescriptionTextView)
				.For(v => v.Placeholder)
				.To(vm => vm.PostDescriptionPlaceHolder);

			bindingSet.Bind(this)
				.For(v => v.SelectedImage)
				.To(vm => vm.SelectedImage);

			bindingSet.Bind(_selectedImageTextView)
				.For("Visibility")
				.To(vm => vm.VisibleSelectedImage)
				.WithConversion("InvertBool");

			bindingSet.Bind(_selectedImageTextView.Tap())
				.For(v => v.Command)
				.To(vm => vm.ShowPhotoCollectionCommand);

			bindingSet.Bind(_btnCancel)
				.For("Title")
				.To(vm => vm.BtnCancelTitle);

			bindingSet.Bind(_btnSubmit)
				.For("Title")
				.To(vm => vm.BtnSubmitTitle);

			bindingSet.Bind(HeaderBar)
				.For(v => v.BackPressedCommand)
				.To(vm => vm.BackPressedCommand);

			bindingSet.Bind(_btnSubmit)
				.For("Enabled")
				.To(vm => vm.IsSubmitBtnEnabled);

			bindingSet.Apply();
		}

		private async void HandleSelectImage(object sender, EventArgs e)
		{
			await MediaHelper.OpenGallery();
			var image = MediaHelper.Images;
			if (image != null)
			{
				ImageCommand.Execute(image);
			}

			image?.Clear();
		}

		private void UpdateSelectedImageTextView()
		{
			if (_selectedImage == null || _selectedImage == Strings["SelectedImage"])
			{
				_selectedImage = Strings["SelectedImage"];
				_selectedImageTextView.AttributedText =
					UIHelper.CreateAttributedString(_selectedImage, ColorHelper.Gray, true);
			}
			else
			{
				_selectedImageTextView.AttributedText =
					UIHelper.CreateAttributedString(_selectedImage, ColorHelper.DarkBlue, true);
			}
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			_postTitleTextField.BecomeFirstResponder();
		}
	}
}