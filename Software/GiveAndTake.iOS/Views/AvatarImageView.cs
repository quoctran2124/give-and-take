﻿using GiveAndTake.Core.ViewModels;
using GiveAndTake.iOS.Controls;
using GiveAndTake.iOS.Helpers;
using GiveAndTake.iOS.Views.Base;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Binding.Views.Gestures;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using UIKit;

namespace GiveAndTake.iOS.Views
{
	[MvxModalPresentation(ModalPresentationStyle = UIModalPresentationStyle.OverFullScreen,
		ModalTransitionStyle = UIModalTransitionStyle.CrossDissolve)]
	public class AvatarImageView : BaseView
	{
		private CustomMvxCachedImageView _ivAvatarImage;
		private UIButton _closeButton;

		public override bool PrefersStatusBarHidden() => true;

		protected override void InitView()
		{
			HeaderBar.Hidden = true;
			View.BackgroundColor = UIColor.Black;
			InitContentView();
		}

		protected override void CreateBinding()
		{
			var bindingSet = this.CreateBindingSet<AvatarImageView, AvatarImageViewModel>();

			bindingSet.Bind(_closeButton.Tap())
				.For(v => v.Command)
				.To(vm => vm.CloseCommand);

			bindingSet.Bind(_ivAvatarImage)
				.For(v => v.ImageUrl)
				.To(vm => vm.ImageUrl);

			bindingSet.Bind(_ivAvatarImage)
				.For(v => v.Base64String)
				.To(vm => vm.Base64String);

			bindingSet.Apply();
		}

		private void InitContentView()
		{
			_ivAvatarImage = UIHelper.CreateCustomImageView(0, 0, ImageHelper.DefaultAvatar);
			_ivAvatarImage.ContentMode = UIViewContentMode.ScaleAspectFit;

			View.AddSubview(_ivAvatarImage);

			View.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_ivAvatarImage, NSLayoutAttribute.Top, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_ivAvatarImage, NSLayoutAttribute.Left, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_ivAvatarImage, NSLayoutAttribute.Right, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_ivAvatarImage, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Bottom, 1, 0)
			});

			_closeButton = UIHelper.CreateImageButton(DimensionHelper.NavigationWidth,
				DimensionHelper.NavigationWidth, ImageHelper.DeletePhotoButton);

			View.AddSubview(_closeButton);

			View.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_closeButton, NSLayoutAttribute.Top, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Top, 1, DimensionHelper.MarginNormal + UIApplication.SharedApplication.KeyWindow.SafeAreaInsets.Top),
				NSLayoutConstraint.Create(_closeButton, NSLayoutAttribute.Right, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Right, 1, - DimensionHelper.MarginNormal)
			});
		}
	}
}