﻿using System;
using System.Windows.Input;
using CoreAnimation;
using GiveAndTake.iOS.Helpers;
using UIKit;

namespace GiveAndTake.iOS.Controls
{
	public class RadioButton : UIView
	{
		private UIView _pointView;

		private CAShapeLayer _strokeSharp;
		private UILabel _titleButtonLabel;
		private UIView _strokeCircle;
		private UIView _fillCircle;

		public ICommand ClickCommand { get; set; }
		public EventHandler StateChanged;

		public string Title
		{
			get => _titleButtonLabel.Text;
			set => _titleButtonLabel.Text = value;
		}

		private bool _checked;

		public bool Checked
		{
			get => _checked;
			set
			{
				_checked = value;
				_fillCircle.Hidden = !value;

				_strokeSharp.StrokeColor = value ? ColorHelper.Black.CGColor : ColorHelper.DarkGray.CGColor;
				StateChanged?.Invoke(this, EventArgs.Empty);
			}
		}

		public RadioButton(nfloat width)
		{
			InitControl(width);
		}

		private void InitControl(nfloat width)
		{
			TranslatesAutoresizingMaskIntoConstraints = false;
			AddGestureRecognizer(new UITapGestureRecognizer(OnClicked));

			if (width > 0)
			{
				AddConstraint(NSLayoutConstraint.Create(this, NSLayoutAttribute.Width, NSLayoutRelation.Equal, null,
					NSLayoutAttribute.NoAttribute, 1, width));
			}

			_pointView = UIHelper.CreateView(DimensionHelper.RadioButtonHeight, DimensionHelper.RadioButtonHeight,
				ColorHelper.Transparent);
			AddPointView();

			_titleButtonLabel = UIHelper.CreateLabel(ColorHelper.DarkGray, DimensionHelper.MediumTextSize);

			AddSubview(_pointView);
			AddSubview(_titleButtonLabel);
			AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_pointView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, this, NSLayoutAttribute.Top, 1,
					0),
				NSLayoutConstraint.Create(_pointView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, this, NSLayoutAttribute.Left,
					1, 0),

				NSLayoutConstraint.Create(_titleButtonLabel, NSLayoutAttribute.Top, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_titleButtonLabel, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _pointView,
					NSLayoutAttribute.Right, 1,  DimensionHelper.DefaultMargin),
				NSLayoutConstraint.Create(_titleButtonLabel, NSLayoutAttribute.Right, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.Right, 1, -DimensionHelper.DefaultMargin),

				NSLayoutConstraint.Create(this, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _titleButtonLabel,
					NSLayoutAttribute.Bottom, 1, 0)
			});
		}

		private void AddPointView()
		{
			_strokeCircle = UIHelper.CreateView(DimensionHelper.RadioSize, DimensionHelper.RadioSize, ColorHelper.Transparent);
			_strokeSharp = UIHelper.CreateCircleSharpLayer(ColorHelper.Black, DimensionHelper.RadioSize, 1);
			_strokeCircle.Layer.AddSublayer(_strokeSharp);

			_fillCircle = UIHelper.CreateView(DimensionHelper.SmallRadioSize, DimensionHelper.SmallRadioSize, ColorHelper.LightBlue);
			_fillCircle.Layer.CornerRadius = DimensionHelper.SmallRadioSize / 2;

			_pointView.AddSubview(_strokeCircle);
			_pointView.AddSubview(_fillCircle);
			_pointView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_strokeCircle, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _pointView,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_strokeCircle, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _pointView,
					NSLayoutAttribute.CenterX, 1, 0),

				NSLayoutConstraint.Create(_fillCircle, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _strokeCircle,
					NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_fillCircle, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _strokeCircle,
					NSLayoutAttribute.CenterY, 1, 0),
			});
		}

		private void OnClicked(UITapGestureRecognizer obj)
		{
			ClickCommand?.Execute(null);
		}
	}
}