﻿using GiveAndTake.iOS.Helpers;
using UIKit;

namespace GiveAndTake.iOS.Controls
{
	public class PopupItemLabel : UILabel
	{
		private bool _isSelected;

		public bool IsSelected
		{
			get => _isSelected;
			set
			{
				_isSelected = value;
				TextColor = value ? UIColor.Black : ColorHelper.Black.ColorWithAlpha(0.5f);
				Font = value ? UIFont.FromName("SanFranciscoDisplay-Medium", DimensionHelper.PopupMediumTextSize) : UIFont.FromName("SanFranciscoDisplay-Regular", DimensionHelper.PopupMediumTextSize);
			}
		}
	}
}