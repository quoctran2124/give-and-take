﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Facebook.CoreKit;
using Firebase.CloudMessaging;
using Foundation;
using GiveAndTake.Core;
using GiveAndTake.Core.Extensions;
using GiveAndTake.Core.Helpers;
using GiveAndTake.Core.Models;
using GiveAndTake.iOS.Helpers;
using HockeyApp.iOS;
using MvvmCross;
using MvvmCross.Platforms.Ios.Core;
using Newtonsoft.Json;
using UIKit;
using UserNotifications;
namespace GiveAndTake.iOS
{
	[Register("AppDelegate")]
	public class AppDelegate : MvxApplicationDelegate<Setup, App>, IUNUserNotificationCenterDelegate, IMessagingDelegate
	{
#if DEVELOPMENT
		const string HockeyAppid = "e3d67fc2cd7e4c988886fdcc4dc1c486";
#else
		const string HockeyAppid = "6a9a4f2bf4154af386c07da063fcc458";
#endif

		private IDataModel _dataModel;
		public override UIWindow Window { get; set; }

		private const int PostIdLength = 36;
		private const int ApplinksCutLength = 31;

		public override bool FinishedLaunching(UIApplication application, NSDictionary launchOptions)
		{
			base.FinishedLaunching(application, launchOptions);
			Profile.EnableUpdatesOnAccessTokenChange(true);
			Settings.AppID = Keys.FacebookAppId;
			Settings.DisplayName = Keys.FacebookDisplayName;

			var manager = BITHockeyManager.SharedHockeyManager;
			manager.Configure(HockeyAppid);
			manager.CrashManager.CrashManagerStatus = BITCrashManagerStatus.AutoSend;
			manager.DisableMetricsManager = true;
			manager.StartManager();
			manager.Authenticator.AuthenticateInstallation();

			if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
			{
				var pushSettings = UIUserNotificationSettings.GetSettingsForTypes(
					UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound,
					new NSSet());

				UIApplication.SharedApplication.RegisterUserNotificationSettings(pushSettings);
				UIApplication.SharedApplication.RegisterForRemoteNotifications();
			}
			else
			{
				UIRemoteNotificationType notificationTypes =
					UIRemoteNotificationType.Alert | UIRemoteNotificationType.Badge | UIRemoteNotificationType.Sound;
				UIApplication.SharedApplication.RegisterForRemoteNotificationTypes(notificationTypes);
			}

			return ApplicationDelegate.SharedInstance.FinishedLaunching(application, launchOptions);
        }
		public override void DidEnterBackground(UIApplication uiApplication)
		{
			
		}

		public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
		{
			var currentDeviceToken = deviceToken.Description;
			if (!string.IsNullOrWhiteSpace(currentDeviceToken))
			{
				currentDeviceToken = currentDeviceToken.Trim('<').Trim('>').Replace(" ", "");
				Mvx.Resolve<IDeviceInfo>().DeviceToken = currentDeviceToken;
			}
		}

		public override void FailedToRegisterForRemoteNotifications(UIApplication application, NSError error)
		{
			
		}

		// when app in foreground
		public override void DidReceiveRemoteNotification(UIApplication application, NSDictionary userInfo,
			Action<UIBackgroundFetchResult> completionHandler)
		{
			if (userInfo != null)
			{
				SendNotification(userInfo);
			}

		}


		public override void ReceivedLocalNotification(UIApplication application, UILocalNotification notification)
		{

		}

		// iOS 10, fire when recieve notification foreground
		[Export("userNotificationCenter:willPresentNotification:withCompletionHandler:")]

		public void WillPresentNotification(UNUserNotificationCenter center, UNNotification notification, Action<UNNotificationPresentationOptions> completionHandler)
		{
			completionHandler(UNNotificationPresentationOptions.None);
		}

		private void SendNotification(NSDictionary data)
		{
			var notification = new Notification();
			var notificationData = (NSDictionary) data.ObjectForKey(new NSString("notification"));
			var notificationSystemData = (NSDictionary) data.ObjectForKey(new NSString("aps"));
			var badgeKey = new NSString("badge");

			var badgeValue = int.Parse(notificationSystemData[badgeKey].ToString());

			UpdateBadgeIcon(badgeValue);

			var dictionary = new Dictionary<string, string>();

			foreach (var dataMember in GetModelDataMembers(notification.GetType()))
			{
				var key = new NSString(dataMember);

				if (notificationData.Keys.Contains(key))
				{
					var value = notificationData[key];
					if (value.ToString() == "0")
					{
						dictionary[dataMember] = "false";
					}
					else if (value.ToString() == "1")
					{
						dictionary[dataMember] = "true";
					}
					else
					{
						dictionary[dataMember] = value.ToString();
					}
				}
			}

			notification = dictionary.ToObject<Notification>();
			bool isDataModelInitialized = Mvx.CanResolve<IDataModel>();
			if (isDataModelInitialized)
			{
				_dataModel = Mvx.Resolve<IDataModel>();
				_dataModel.SelectedNotification = notification;
			}

			if (UIApplication.SharedApplication.ApplicationState == UIApplicationState.Active)
			{
				if (isDataModelInitialized)
				{
					_dataModel.RaiseBadgeUpdated(badgeValue);
				}
			}
			else
			{
				if (isDataModelInitialized)
				{
					_dataModel.RaiseNotificationReceived(notification);
					_dataModel.RaiseBadgeUpdated(badgeValue);
				}
			}
		}

		private void UpdateBadgeIcon(int badgeValue)
		{
			UIUserNotificationSettings settings =
				UIUserNotificationSettings.GetSettingsForTypes(UIUserNotificationType.Badge, null);
			UIApplication.SharedApplication.RegisterUserNotificationSettings(settings);
			UIApplication.SharedApplication.ApplicationIconBadgeNumber = badgeValue;
		}

		private List<string> GetModelDataMembers(Type type)
		{
			var names = new List<string>();
			foreach (var property in type.GetProperties())
			{
				var jsonIgnoreAttributes = property.GetCustomAttributes(typeof(JsonIgnoreAttribute), false)
					.OfType<JsonIgnoreAttribute>().ToList();

				if (jsonIgnoreAttributes.Any())
				{
					continue;
				}

				var dataMemberAttributes = property.GetCustomAttributes(typeof(DataMemberAttribute), false)
					.OfType<DataMemberAttribute>().ToList();
				var dataMemberName = dataMemberAttributes.Any() ? dataMemberAttributes[0].Name : property.Name;
				names.Add(dataMemberName);
			}

			return names;
		}

		public override bool OpenUrl(UIApplication application, NSUrl url, string sourceApplication,
			NSObject annotation)
		{
			var dataModel = Mvx.Resolve<IDataModel>();
			var str = url.AbsoluteString.Length - ApplinksCutLength - PostIdLength;
			dataModel.ApplinkUrl = url.AbsoluteString.Substring(str, ApplinksCutLength + PostIdLength); //Get the link to be synchronized with the link from the Android

			if (dataModel.IsHomeViewBeforeApplink)
			{
				dataModel.OnApplinkReceived();
			}
			else
			{
				dataModel.IsHomeViewBeforeApplink = false;
			}			
			return ApplicationDelegate.SharedInstance.OpenUrl(application, url, sourceApplication, annotation);
		}
	}
}