﻿using Foundation;
using GiveAndTake.Core;
using GiveAndTake.Core.Helpers.Interface;

namespace GiveAndTake.iOS.Helpers
{
	public class StorageHelper : IStorageHelper
	{
		public void SaveLanguage(LanguageType language)
		{
			var nSUserDefaults = NSUserDefaults.StandardUserDefaults;
			nSUserDefaults.SetString(language.ToString(), "chovanhan_language");
			nSUserDefaults.Synchronize();
		}
	}
}