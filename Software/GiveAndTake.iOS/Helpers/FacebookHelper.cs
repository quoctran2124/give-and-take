﻿using System;
using Facebook.CoreKit;
using Facebook.LoginKit;
using Facebook.ShareKit;
using Foundation;
using GiveAndTake.Core.Helpers.Interface;
using GiveAndTake.iOS.Views;
using UIKit;

namespace GiveAndTake.iOS.Helpers
{
	public class FacebookHelper : IFacebookHelper
	{
		public void ShareFacebookContent(string content, string contentUrl)
		{
			if (string.IsNullOrEmpty(contentUrl))
			{
				return;
			}

			var linkContent = new ShareLinkContent()
			{
				Quote = content
			};


			linkContent.SetContentUrl(NSUrl.FromString(contentUrl));
			var shareDialog = new ShareDialog();
			var window = UIApplication.SharedApplication.KeyWindow;
			var rootViewController = window.RootViewController;
			shareDialog.FromViewController = rootViewController;

			shareDialog.Mode = ShareDialogMode.FeedWeb;

			shareDialog.SetShareContent(linkContent);
			shareDialog.Show();
		}
	}
}