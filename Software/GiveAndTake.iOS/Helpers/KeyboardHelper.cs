﻿using System;
using CoreGraphics;
using Foundation;
using MvvmCross.Platforms.Ios.Views;
using UIKit;

namespace GiveAndTake.iOS.Helpers
{
	public static class KeyboardHelper
	{
		public static bool IsKeyBoardVisible { get; private set; }

		public static Position TextFieldPosition { get; set; }

		/// <summary>
		/// Set this field to any view inside the scroll view to center this view instead of the current responder
		/// </summary>
		public static UIView ViewToCenterOnKeyboardShown { get; set; }

		public static UIView TopView { get; set; }

		public static bool IsKeyBoardDidHidden { get; set; }

		public static nfloat KeyboardFrameHeight { get; private set; }

		static KeyboardHelper()
		{
			ObserverKeyboard();
		}

		private static void ObserverKeyboard()
		{
			NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.WillHideNotification, OnWillHideKeyboard);
			NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.WillShowNotification, OnWillShowKeyboard);
			NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.DidHideNotification, OnDidHideKeyboard);
		}
		public static void HideKeyboard()
		{
			if (IsKeyBoardDidHidden) return;

			if (IsKeyBoardVisible)
			{
				UIApplication.SharedApplication.KeyWindow.EndEditing(true);
			}
		}

		private static void OnWillShowKeyboard(NSNotification notification)
		{
			IsKeyBoardVisible = true;
			IsKeyBoardDidHidden = false;
			OnKeyboardNotification(notification);
		}

		private static void OnWillHideKeyboard(NSNotification notification)
		{
			IsKeyBoardVisible = false;
			OnKeyboardNotification(notification);
		}

		private static void OnDidHideKeyboard(NSNotification notification)
		{
			IsKeyBoardDidHidden = true;
		}

		private static void OnKeyboardNotification(NSNotification notification)
		{
			//Check if the keyboard is becoming visible
			var visible = notification.Name == UIKeyboard.WillShowNotification;

			//Start an animation, using values from the keyboard
			UIView.BeginAnimations("AnimateForKeyboard");
			UIView.SetAnimationBeginsFromCurrentState(true);
			UIView.SetAnimationDuration(UIKeyboard.AnimationDurationFromNotification(notification));
			UIView.SetAnimationCurve((UIViewAnimationCurve)UIKeyboard.AnimationCurveFromNotification(notification));

			KeyboardFrameHeight = visible
				? UIKeyboard.FrameEndFromNotification(notification).Height
				: UIKeyboard.FrameBeginFromNotification(notification).Height;

			OnKeyboardChanged(visible);

			//Commit the animation
			UIView.CommitAnimations();
		}

		/// <summary>
		/// Override this method to apply custom logic when the keyboard is shown/hidden
		/// </summary>
		/// If the keyboard is visible
		public static void OnKeyboardChanged(bool visible)
		{
			var activeView = ViewToCenterOnKeyboardShown;
			var scrollView = activeView?.FindSuperviewOfType(TopView, typeof(UIScrollView)) as UIScrollView;
			if (scrollView == null)
			{
				return;
			}

			if (!visible)
			{
				RestoreScrollPosition(scrollView);
			}
			else
			{
				CenterViewInScroll(activeView, scrollView);
			}
		}

		private static void CenterViewInScroll(UIView viewToCenter, UIScrollView scrollView)
		{
			var spaceAboveKeyboard = viewToCenter.Frame.Height - KeyboardFrameHeight;

			if (TextFieldPosition != null && TextFieldPosition.Y > spaceAboveKeyboard)
			{
				var contentInsets = new UIEdgeInsets(0.0f, 0.0f, KeyboardFrameHeight, 0.0f);
				scrollView.ContentInset = contentInsets;
				scrollView.ScrollIndicatorInsets = contentInsets;

				// Position of the active field relative isnside the scroll view
				var relativeFrame = viewToCenter.Superview.ConvertRectToView(viewToCenter.Frame, scrollView);

				// Move the active field to the center of the available space
				var offset = relativeFrame.Y + TextFieldPosition.Y - spaceAboveKeyboard;
				scrollView.ContentOffset = new CGPoint(0, offset);
			}
		}

		private static void RestoreScrollPosition(UIScrollView scrollView)
		{
			scrollView.ContentOffset = new CGPoint(0, 0);
			scrollView.ContentInset = UIEdgeInsets.Zero;
			scrollView.ScrollIndicatorInsets = UIEdgeInsets.Zero;
		}
	}
}