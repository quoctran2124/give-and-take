﻿using System;
using System.Globalization;
using UIKit;

namespace GiveAndTake.iOS.Helpers
{
	public static class ColorHelper

	{
        public static UIColor Black => ToUIColor("191919");
        public static UIColor Blue => ToUIColor("0FBCF9");
		public static UIColor Default => ToUIColor("E4E4E4");
		public static UIColor LightGray => ToUIColor("F3F3F3");
		public static UIColor DarkBlue => ToUIColor("0D70B2");
		public static UIColor Gray => ToUIColor("CCCCCC");
		public static UIColor LightBlue => ToUIColor("3FB8EA");
		public static UIColor PhotoCollectionViewBackground => ToUIColor("F4F5F4");
		public static UIColor PopupSeparator => ToUIColor("E0EFF7");
		public static UIColor DefaultEditTextFieldColor => ToUIColor("F3F3F3");
	    public static UIColor GreyLineColor => ToUIColor("F2F2F2");
		public static UIColor Green => ToUIColor("2CB273");
		public static UIColor DarkRed => ToUIColor("8B0000");
		public static UIColor Line => ToUIColor("7DBEf4");
		public static UIColor TextNormalColor => ToUIColor("EEF2F5");
		public static UIColor ColorPrimary => ToUIColor("3FB8EA");
		public static UIColor ButtonOff => ToUIColor("F2F2F2");
		public static UIColor DarkGray => ToUIColor("666666");
		public static UIColor LoadingIndicatorLightBlue => ToUIColor("3AB9EB");
		public static UIColor Transparent => ToUIColor("00000000");
		public static UIColor BlueFacebook => ToUIColor("3B5998");

		public static UIColor ToUIColor(string hexString)
		{
			if (string.IsNullOrWhiteSpace(hexString))
			{
				return UIColor.White;
			}

			hexString = hexString.Replace("#", string.Empty);
			hexString = hexString.Replace("argb: ", string.Empty);

			if (hexString.Length != 6 && hexString.Length != 8)
			{
				throw new Exception("Invalid hex string");
			}

			var index = -2;
			int alpha;

			if (hexString.Length == 6)
			{
				alpha = 255;
			}
			else
			{
				index += 2;
				alpha = int.Parse(hexString.Substring(index, 2), NumberStyles.AllowHexSpecifier);
			}

			var red = int.Parse(hexString.Substring(index + 2, 2), NumberStyles.AllowHexSpecifier);
			var green = int.Parse(hexString.Substring(index + 4, 2), NumberStyles.AllowHexSpecifier);
			var blue = int.Parse(hexString.Substring(index + 6, 2), NumberStyles.AllowHexSpecifier);

			return UIColor.FromRGBA(red, green, blue, alpha);
		}
    }
}