﻿using GiveAndTake.Core;
using GiveAndTake.Core.Helpers.Interface;
using GiveAndTake.iOS.Helpers;
using MvvmCross;
using MvvmCross.Platforms.Ios.Core;
using MvvmCross.ViewModels;
using GiveAndTake.Core.Helpers;
using MvvmCross.Base;
using MvvmCross.Plugin.ResourceLoader.Platforms.Ios;
using SystemHelper = GiveAndTake.iOS.Helpers.SystemHelper;

namespace GiveAndTake.iOS
{
	public class Setup : MvxIosSetup<App>
	{
		protected override void InitializeFirstChance()
		{
			base.InitializeFirstChance();
			Mvx.LazyConstructAndRegisterSingleton<IUrlHelper, UrlHelper>();
			Mvx.LazyConstructAndRegisterSingleton<ISystemHelper, SystemHelper>();
			Mvx.LazyConstructAndRegisterSingleton<IFacebookHelper, FacebookHelper>();
			Mvx.LazyConstructAndRegisterSingleton<IDeviceInfo, DeviceInfo>();
			Mvx.LazyConstructAndRegisterSingleton<IStorageHelper, StorageHelper>();
			Mvx.RegisterType<IMvxResourceLoader, MvxIosResourceLoader>();
		}

		protected override IMvxApplication CreateApp()
		{
			return new App();
		}
	}
}