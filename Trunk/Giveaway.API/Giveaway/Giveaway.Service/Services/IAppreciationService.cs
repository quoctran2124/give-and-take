﻿using Giveaway.Data.Models.Database;

namespace Giveaway.Service.Services
{
	public interface IAppreciationService : IEntityService<Appreciation>
	{
	}

	public class AppreciationService : EntityService<Appreciation>, IAppreciationService
	{
	}
}