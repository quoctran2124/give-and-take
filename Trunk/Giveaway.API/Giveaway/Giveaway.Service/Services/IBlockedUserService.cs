﻿using Giveaway.Data.Models.Database;

namespace Giveaway.Service.Services
{
	public interface IBlockedUserService : IEntityService<BlockedUser>
	{
	}

	public class BlockedUserService : EntityService<BlockedUser>, IBlockedUserService
	{
	}
}
