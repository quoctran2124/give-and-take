﻿namespace Giveaway.Data.Enums
{
    public enum PostStatus
    {
        Giving,
        Gave
    }
}