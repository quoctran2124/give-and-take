﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Giveaway.Data.Models.Database
{
	[Table("BlackList")]
	public class BlockedUser : BaseEntity
	{
		public Guid UserId { get; set; }
		public Guid BanId { get; set; }

		[ForeignKey("UserId")]
		public virtual User User { get; set; }

		[ForeignKey("BanId")]
		public virtual User BannedUser { get; set; }
	}
}