﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Giveaway.Data.Models.Database
{
	[Table("Appreciation")]
	public class Appreciation : BaseEntity
	{
		public Guid PostId { get; set; }

		public Guid UserId { get; set; }

		public virtual User User { get; set; }

		public virtual Post Post { get; set; }
	}
}