using System.Linq;
using AutoMapper;
using AutoMapper.Configuration;
using Giveaway.API.Shared.Models.DTO;
using Giveaway.API.Shared.Requests.DeviceIdentity;
using Giveaway.API.Shared.Requests.Image;
using Giveaway.API.Shared.Requests.Notification;
using Giveaway.API.Shared.Requests.Post;
using Giveaway.API.Shared.Requests.Report;
using Giveaway.API.Shared.Requests.Request;
using Giveaway.API.Shared.Requests.Response;
using Giveaway.API.Shared.Requests.Warning;
using Giveaway.API.Shared.Responses.Category;
using Giveaway.API.Shared.Responses.Image;
using Giveaway.API.Shared.Responses.Notification;
using Giveaway.API.Shared.Responses.Post;
using Giveaway.API.Shared.Responses.ProviceCity;
using Giveaway.API.Shared.Responses.Report;
using Giveaway.API.Shared.Responses.Request;
using Giveaway.API.Shared.Responses.Response;
using Giveaway.API.Shared.Responses.User;
using Giveaway.API.Shared.Responses.Warning;
using Giveaway.Data.Models.Database;

namespace Giveaway.API
{
    public static class AutoMapperConfig
    {
        public static void RegisterModel()
        {
            var cfg = new MapperConfigurationExpression();

			#region User

			cfg.CreateMap<Data.Models.Database.User, Giveaway.API.Shared.Models.DTO.User>();
            cfg.CreateMap<Data.Models.Database.User, UserPostResponse>()
				.ForMember(
					destination => destination.MemberType,
					map => map.MapFrom(source => source.MemberType.ToString())
				); 
            cfg.CreateMap<Data.Models.Database.User, UserRequestResponse>();
            cfg.CreateMap<Data.Models.Database.User, UserReportResponse>()
                .ForMember(
                    destination => destination.Status,
                    map => map.MapFrom(source => source.EntityStatus.ToString()
                ));

            #endregion

            #region Category

            cfg.CreateMap<Data.Models.Database.Category, Shared.Models.DTO.Category>();
            cfg.CreateMap<Data.Models.Database.Category, CategoryAppResponse>();
            cfg.CreateMap<CategoryAppResponse, Data.Models.Database.Category>();
            cfg.CreateMap<Data.Models.Database.Category, CategoryCmsResponse>();
            cfg.CreateMap<CategoryCmsResponse, Data.Models.Database.Category>();

            #endregion

            #region Post

            cfg.CreateMap<Post, PostAppResponse>()
                .ForMember(
                    destination => destination.Status,
                    map => map.MapFrom(source => source.PostStatus.ToString())
                )
                .ForMember(
                    destination => destination.CommentCount,
                    map => map.MapFrom(source => source.Comments.Count)
                )
                .ForMember(
                    destination => destination.RequestCount,
                    map => map.MapFrom(source => source.Requests.Count)
                )
				.ForMember(
		            destination => destination.AppreciationCount,
		            map => map.MapFrom(source => source.Appreciations.Count)
	            );
			cfg.CreateMap<PostAppResponse, Post>();

            cfg.CreateMap<Post, PostCmsResponse>()
                .ForMember(
                    destination => destination.EntityStatus,
                    map => map.MapFrom(source => source.EntityStatus.ToString())
                )
                .ForMember(
                    destination => destination.Status,
                    map => map.MapFrom(source => source.PostStatus.ToString())
                );
            cfg.CreateMap<PostCmsResponse, Post>();

            cfg.CreateMap<Post, PostReportResponse>();
            cfg.CreateMap<PostRequest, Post>();
	        cfg.CreateMap<Post, PostRequestResponse>();

			#endregion

			#region Image

			cfg.CreateMap<Image, ImageResponse>();
            cfg.CreateMap<ImageRequest, Image>();
            cfg.CreateMap<ImageDTO, Image>();

            #endregion

            #region ProvinceCity

            cfg.CreateMap<ProvinceCity, ProvinceCityResponse>();

            #endregion

            #region Report

            cfg.CreateMap<Report, ReportCmsResponse>()
                .ForMember(
                    destination => destination.WarningNumber,
                    map => map.MapFrom(source => source.User.WarningMessages.Count)
                );
	        cfg.CreateMap<CreateReportRequest, Report>();
	        cfg.CreateMap<Report, ReportAppResponse>();

			#endregion

			#region Warning

			cfg.CreateMap<WarningMessage, WarningResponse>();
            cfg.CreateMap<WarningRequest, WarningMessage>();

            #endregion

            #region Request

	        cfg.CreateMap<Request, RequestPostResponse>()
		        .ForMember(
			        destination => destination.RequestStatus,
			        map => map.MapFrom(source => source.RequestStatus.ToString())
		        );
            cfg.CreateMap<RequestPostRequest, Request>();

			#endregion

			#region Response

			cfg.CreateMap<ResponseRequest, Response>();
			cfg.CreateMap<Response, ResponseRequestResponse>();

			#endregion

	        #region Notification

	        cfg.CreateMap<Notification, NotificationResponse>()
				.ForMember(
			        destination => destination.Type,
			        map => map.MapFrom(source => source.Type.ToString()))
				.ForMember(
			        destination => destination.NotificationType,
			        map => map.MapFrom(source => source.TemptType.ToString())
		        );
	        cfg.CreateMap<NotificationRequest, Notification>();

			#endregion

			#region DeviceIdentity

	        cfg.CreateMap<DeviceIdentityRequest, DeviceIdentity>();

			#endregion

			Mapper.Initialize(cfg);
        }
    }
}
