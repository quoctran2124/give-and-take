﻿using System;

namespace Giveaway.API.Shared.Exceptions
{
	public class ServiceUnavailableException : Exception
	{
		public ServiceUnavailableException(string message) : base(message)
		{
		}
	}
}