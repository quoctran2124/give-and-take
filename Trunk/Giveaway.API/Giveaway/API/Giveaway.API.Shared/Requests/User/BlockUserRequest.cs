﻿using System;
using System.Runtime.Serialization;

namespace Giveaway.API.Shared.Requests.User
{
	[DataContract]
	public class BlockUserRequest
	{
		[DataMember(Name = "userId")]
		public Guid UserId { get; set; }

		[DataMember(Name = "banId")]
		public Guid BanId { get; set; }
	}
}