﻿using System.Runtime.Serialization;

namespace Giveaway.API.Shared.Requests.User
{
	[DataContract]
	public class UserProfileRequest
	{
		[DataMember(Name = "displayName")]
		public string DisplayName { get; set; }

		[DataMember(Name = "avatarImageData")]
	    public string AvatarImageData { get; set; }
    }
}
