using System;
using Giveaway.Data.Enums;
using System.Collections.Generic;
using Giveaway.Data.EF.Exceptions;
using Giveaway.Util.Constants;

namespace Giveaway.API.Shared.Helpers
{
	public static class NotificationHelper
	{
		public static Dictionary<TemptNotificationType, string> MessagesVN = new Dictionary<TemptNotificationType, string>
		{
			{TemptNotificationType.Appreciation,  "đã yêu thích bài đăng của bạn!" },
			{TemptNotificationType.RequestPending,  "đã gửi yêu cầu cho bài đăng của bạn!" },
			{TemptNotificationType.RequestApproved,  "đã chấp nhận yêu cầu của bạn!" },
			{TemptNotificationType.RequestRejected,  "đã từ chối yêu cầu của bạn!" },
			{TemptNotificationType.ItemReceived,  "đã nhận vật phẩm của bạn!" },
			{TemptNotificationType.ItemCancelled,  "đã hủy nhận vật phẩm của bạn!" },
			{TemptNotificationType.Comment,  "đã bình luận về bài đăng của bạn!" },
			{TemptNotificationType.Closed,  "đã đóng bài đăng này!" }
		};

		public static Dictionary<TemptNotificationType, string> MessagesEN = new Dictionary<TemptNotificationType, string>
		{
			{TemptNotificationType.Appreciation,  "appreciated on your post!" },
			{TemptNotificationType.RequestPending,  "sent you a new request!" },
			{TemptNotificationType.RequestApproved,  "approved your request!" },
			{TemptNotificationType.RequestRejected,  "rejected your request!" },
			{TemptNotificationType.ItemReceived,  "received your item!" },
			{TemptNotificationType.ItemCancelled,  "cancelled your approval!" },
			{TemptNotificationType.Comment,  "commented on your post!" },
			{TemptNotificationType.Closed,  "closed this post!" }
		};

		public static Dictionary<string, Dictionary<TemptNotificationType, string>> NotificationMessage =
			new Dictionary<string, Dictionary<TemptNotificationType, string>>()
			{
				{"vi", MessagesVN },
				{"en", MessagesEN }
			};

		public static string GenerateNotificationMessage(string language, string userName, TemptNotificationType action, int sameActionCount = 1)
		{
			try
			{
				return sameActionCount > 1
					? string.Format(ZippedNotificationMessageTemplate[language], userName, sameActionCount - 1, action.GetMessage(language))
					: string.Format(NotificationMessageTemplate[language], userName, action.GetMessage(language));
			}
			catch
			{
				throw new BadRequestException(CommonConstant.Error.InvalidInput);
			}
		}

		public static Dictionary<string, string> NotificationMessageTemplate = new Dictionary<string, string>
		{
			{"vi", "{0} {1}" },
			{"en", "{0} {1}" }
		};

		public static Dictionary<string, string> ZippedNotificationMessageTemplate = new Dictionary<string, string>
		{
			{"vi", "{0} và {1} người khác {2}" },
			{"en", "{0} and {1} other people {2}" }
		};

		public static string GetMessage(this TemptNotificationType action, string language)
		{
			return NotificationMessage[language][action];
		}
	}
}