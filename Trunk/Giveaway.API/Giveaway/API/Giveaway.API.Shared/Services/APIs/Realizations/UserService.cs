using Giveaway.API.Shared.Extensions;
using Giveaway.API.Shared.Helpers;
using Giveaway.API.Shared.Models;
using Giveaway.API.Shared.Requests;
using Giveaway.API.Shared.Requests.User;
using Giveaway.API.Shared.Responses;
using Giveaway.API.Shared.Responses.User;
using Giveaway.Data.EF;
using Giveaway.Data.EF.DTOs.Requests;
using Giveaway.Data.EF.Exceptions;
using Giveaway.Data.Enums;
using Giveaway.Data.Models.Database;
using Giveaway.Util.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Giveaway.API.Shared.Constants;
using Giveaway.API.Shared.Exceptions;
using BadRequestException = Giveaway.API.Shared.Exceptions.BadRequestException;
using DbService = Giveaway.Service.Services;
using User = Giveaway.Data.Models.Database.User;

namespace Giveaway.API.Shared.Services.APIs.Realizations
{
	/// <inheritdoc />
	public class UserService : IUserService
	{
		#region Properties

		private readonly DbService.IUserService _userService;
		private readonly DbService.IUserRoleService _userRoleService;
		private readonly DbService.IRoleService _roleService;
		private readonly DbService.IBlockedUserService _blockService;
		private readonly DbService.INotificationService _notificationService;
		private readonly DbService.IRequestService _requestService;

		#endregion

		#region Constructor

		public UserService(DbService.IUserService userService, DbService.IUserRoleService userRoleService, DbService.IRoleService roleService, DbService.IBlockedUserService blockService, DbService.INotificationService notificationService, DbService.IRequestService requestService)
		{
			_userService = userService;
			_userRoleService = userRoleService;
			_roleService = roleService;
			_blockService = blockService;
			_notificationService = notificationService;
			_requestService = requestService;
		}

		#endregion

		#region public methods

		public User GetUser(Guid userId)
		{
			var user = _userService.Find(userId);
			return user;
		}

		public UserProfileResponse GetUserProfile(Guid userId)
		{
			var currentUser = GetUser(userId);
			return GenerateUserProfileResponse(currentUser);
		}

		public PagingQueryResponse<UserProfileResponse> All(IDictionary<string, string> @params)
		{
			var request = @params.ToObject<PagingQueryUserRequest>();
			var users = GetPagedUsers(request);
			var pageInformation = GetPageInformation(request);
			return GeneratePagingQueryResponse(users, pageInformation);
		}

		public bool Update(User user)
		{
			var isUpdated = _userService.Update(user);
			if (!isUpdated)
			{
				throw new InternalServerErrorException(CommonConstant.Error.InternalServerError);
			}
			return true;
		}

		public UserProfileResponse Update(Guid userId, UserProfileRequest request)
		{
			var currentUser = GetUser(userId);
			var updatedUser = UpdateUserProfile(currentUser, request);
			return GenerateUserProfileResponse(updatedUser);
		}

		public UserProfileResponse SetRole(Guid userId, RoleRequest request)
		{
			var roleId = _roleService.GetRoleId(request.Role);
			_userRoleService.CreateUserRole(userId, roleId);
			return GetUserProfile(userId);
		}

		public UserProfileResponse ChangeUserStatus(Guid userId, StatusRequest request)
		{
			var updatedUser = _userService.UpdateStatus(userId, request.UserStatus);
			return GenerateUserProfileResponse(updatedUser);
		}

		public UserProfileResponse Create(CreateUserProfileRequest request)
		{
			var createdUser = _userService.Create(CreateUserFromRequest(request), out _);
			CreateUserRole(createdUser.Id, Const.Roles.User);
			return GetUserProfile(createdUser.Id);
		}

		private void CreateUserRole(Guid userId, string role)
		{
			var normalUserRoleId = _roleService.FirstOrDefault(r => r.RoleName == role).Id;
			_userRoleService.CreateUserRole(userId, normalUserRoleId);
		}

		public LoginResponse LoginWithFacebook(IDictionary<string, string> @params, FacebookConnectRequest request)
		{
			var versionRequest = @params.ToObject<VersionRequest>();
			if (versionRequest.Version < AppConstant.Version)
			{
				throw new ServiceUnavailableException(CommonConstant.Error.ServiceUnavailable);
			}

			var user = _userService.FirstOrDefault(u => string.Equals(u.SocialAccountId, request.SocialAccountId));
			if (user == null)
			{
				user = new User();
				UpdateBasicUserInformation(request, user);
				UpdateUserPassword(Const.DefaultUserPassword, user);
				user = _userService.Create(user, out _);
				CreateUserRole(user.Id, Const.Roles.User);
			}
			if (user.EntityStatus != EntityStatus.Activated)
			{
				throw new BadRequestException(CommonConstant.Error.BlockedUser);
			}
			return GenerateLoginResponse(user);
		}

		private static void UpdateBasicUserInformation(FacebookConnectRequest request, User user)
		{
			user.FirstName = request.FirstName;
			user.LastName = request.LastName;
			user.UserName = request.SocialAccountId;
			user.SocialAccountId = request.SocialAccountId;
			user.AvatarUrl = request.AvatarUrl;
			user.DisplayName = request.DisplayName;
		}


		public LoginResponse Login(LoginRequest request)
		{
			var validateResult = _userService.ValidateLogin(request);

			if (validateResult.StatusCode != HttpStatusCode.OK)
			{
				throw validateResult.ToException();
			}

			return GenerateLoginResponse(validateResult.Data as User);
		}

		public UserProfileResponse BlockUser(BlockedUser blockRequest)
		{

			var response = _blockService.Create(blockRequest, out var isSaved);

			if (isSaved == false) throw new InternalServerErrorException(CommonConstant.Error.InternalServerError);

			var notifications = _notificationService.Where(x =>
					x.DestinationUserId.Equals(blockRequest.UserId) && x.SourceUserId.Equals(blockRequest.BanId) ||
					x.SourceUserId.Equals(blockRequest.UserId) && x.DestinationUserId.Equals(blockRequest.BanId));
			_notificationService.Block(notifications, out _);

			var requests = _requestService.Include(r => r.Post).Where(x =>
				x.UserId.Equals(blockRequest.UserId) && x.Post.UserId.Equals(blockRequest.BanId) ||
				x.UserId.Equals(blockRequest.BanId) && x.Post.UserId.Equals(blockRequest.UserId));
			_requestService.Block(requests, out _);

			return GenerateUserProfileResponse(GetUser(response.BanId));
		}

		#endregion

		#region private methods

		private static PagingQueryResponse<UserProfileResponse> GeneratePagingQueryResponse(List<UserProfileResponse> users, PageInformation pageInformation)
			=> new PagingQueryResponse<UserProfileResponse>
			{
				Data = users,
				PageInformation = pageInformation
			};

		private PageInformation GetPageInformation(PagingQueryUserRequest request) => new PageInformation
		{
			Total = _userService.Where(u => u.EntityStatus != EntityStatus.Deleted).Count(),
			Limit = request.Limit,
			Page = request.Page
		};

		private List<UserProfileResponse> GetPagedUsers(PagingQueryUserRequest request)
		{
			var users = _userService.Where(u => u.EntityStatus != EntityStatus.Deleted && u.UserName != Const.DefaultSuperAdminUserName);
			if (request.Name != null)
			{
				users = users.Where(u => u.DisplayName.Contains(request.Name));
			}
			if (request.Email != null)
			{
				users = users.Where(u => u.Email.Contains(request.Email));
			}
			if (request.PhoneNumber != null)
			{
				users = users.Where(u => u.PhoneNumber.Contains(request.PhoneNumber));
			}
			if (request.UserName != null)
			{
				users = users.Where(u => u.UserName.Contains(request.UserName));
			}
			return users
				.Skip(request.Limit * (request.Page - 1))
				.Take(request.Limit)
				.Select(u => GenerateUserProfileResponse(u))
				.ToList();
		}

		private UserProfileResponse GenerateUserProfileResponse(User user) => new UserProfileResponse
		{
			Id = user.Id,
			DisplayName = user.DisplayName,
			Username = user.UserName,
			BirthDate = user.BirthDate,
			Gender = user.Gender.ToString(),
			PhoneNumber = user.PhoneNumber,
			Address = user.Address,
			Email = user.Email,
			Roles = _userRoleService.GetUserRoles(user.Id),
			AvatarUrl = user.AvatarUrl,
			Status = user.EntityStatus.ToString(),
			MemberType = user.MemberType.ToString(),
			AppreciationNumber = user.AppreciationNumber
		};

		private LoginResponse GenerateLoginResponse(User user)
		{
			var token = JwtHelper.CreateToken(user.UserName, user.Id, user.DisplayName, _userRoleService.GetUserRoles(user.Id));

			var response = new LoginResponse
			{
				Profile = GenerateUserProfileResponse(user),
				RefreshToken = token.RefreshToke,
				TokenType = token.Type,
				Token = token.AccessToken,
				ExpiresIn = token.Expires,
			};

			return response;
		}

		private User CreateUserFromRequest(CreateUserProfileRequest request)
		{
			var user = new User();
			UpdateUserFields(user, request);
			UpdateUserPassword(request.Password, user);
			return user;
		}

		private User UpdateUserProfile(User user, UserProfileRequest request)
		{
			UpdateUserFields(user, request);
			Update(user);
			return user;
		}

		private void UpdateUserPassword(string password, User user)
		{
			var securedPassword = _userService.GenerateSecurePassword(password);
			user.PasswordHash = securedPassword.Hash;
			user.PasswordSalt = securedPassword.Salt;
		}

		private static void UpdateUserFields(User user, UserProfileRequest request)
		{
			user.DisplayName = request.DisplayName;
			if (!string.IsNullOrEmpty(request.AvatarImageData))
			{
				user.AvatarUrl = SaveNewImage(user.Id.ToString(), request.AvatarImageData);
			}
		}

		private static string SaveNewImage(string id, string imageData)
		{
			var image = new ImageBase64Request
			{
				Id = id,
				Type = "avatar",
				File = imageData
			};

			return UploadImageHelper.PostBase64Image(image).ElementAt(1);
		}

		#endregion
	}
}
