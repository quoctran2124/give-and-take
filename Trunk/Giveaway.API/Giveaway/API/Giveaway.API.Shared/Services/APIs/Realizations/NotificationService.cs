using AutoMapper;
using Giveaway.API.Shared.Extensions;
using Giveaway.API.Shared.Requests.Notification;
using Giveaway.API.Shared.Responses;
using Giveaway.API.Shared.Responses.Notification;
using Giveaway.Data.EF.Exceptions;
using Giveaway.Data.Enums;
using Giveaway.Data.Models.Database;
using Giveaway.Util.Constants;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PushSharp.Core;
using PushSharp.Google;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Transactions;
using Giveaway.API.Shared.Constants;
using Giveaway.API.Shared.Helpers;
using Giveaway.Data.EF;
using Giveaway.Util.Utils;
using Microsoft.AspNetCore.Hosting;
using PushSharp.Apple;
using DbService = Giveaway.Service.Services;

namespace Giveaway.API.Shared.Services.APIs.Realizations
{
	public class NotificationService : INotificationService
	{
		private readonly string FcmProjectNumber = "179460394067";
		private readonly string FcmApiKey = "AIzaSyCfzK4m0TR78drfat67wl_K5WuSRpVrDN4";
		private FcmServiceBroker _fcmBroker;
		private ApnsServiceBroker _apnsBroker;
		private readonly string cerFilePwd = "sioux@123";

		private readonly DbService.INotificationService _notificationService;
		private readonly DbService.IPostService _postService;
		private readonly DbService.IUserService _userService;
		private readonly DbService.IDeviceIdentityService _deviceIdentityService;
		private readonly DbService.IRequestService _requestService;

		public NotificationService(DbService.INotificationService notificationService, DbService.IPostService postService,
			DbService.IUserService userService, DbService.IDeviceIdentityService deviceIdentityService, DbService.IRequestService requestService)
		{
			_notificationService = notificationService;
			_postService = postService;
			_userService = userService;
			_deviceIdentityService = deviceIdentityService;
			_requestService = requestService;

			InitForAndroid();
			InitForiOs();
		}

		public PagingQueryResponseForNotification GetNotificationForPaging(Guid userId, IDictionary<string, string> @params)
		{
			var request = @params.ToObject<PagingQueryNotificationRequest>();

			var notifications = GetPagedNotifications(userId, request, out var total, out var numberOfNotiNotSeen);

			return new PagingQueryResponseForNotification
			{
				Data = notifications,
				NumberOfNotiNotSeen = numberOfNotiNotSeen,
				PageInformation = new PageInformation
				{
					Total = total,
					Page = request.Page,
					Limit = request.Limit
				}
			};
		}

		public int GetUnSeenNotificationNumber(Guid userId)
		{
			var number = _notificationService.Where(x =>
				x.DestinationUserId == userId && x.IsSeen == false && x.EntityStatus == EntityStatus.Activated).Count();

			return number;
		}

		public NotificationResponse Create(Notification notification)
		{
			if (notification.SourceUserId == notification.DestinationUserId)
			{
				return new NotificationResponse();
			}

			var noti = _notificationService.Create(notification, out var isSaved);
			if (isSaved)
			{
				var androidRegistrationIds = _deviceIdentityService.Where(x =>
					x.EntityStatus == EntityStatus.Activated && x.UserId == notification.DestinationUserId && x.MobilePlatform == MobilePlatform.Android)
					.OrderByDescending(x => x.UpdatedTime).ToList();

				var iosRegistrationIds = _deviceIdentityService.Where(x =>
						x.EntityStatus == EntityStatus.Activated && x.UserId == notification.DestinationUserId && x.MobilePlatform == MobilePlatform.Ios)
					.OrderByDescending(x => x.UpdatedTime).ToList();

				if (androidRegistrationIds.Any())
				{
					PushAndroidNotification(noti, androidRegistrationIds);
				}

				if (iosRegistrationIds.Any())
				{
					PushIosNotification(noti, iosRegistrationIds);
				}

				return Mapper.Map<Notification, NotificationResponse>(noti);
			}

			throw new InternalServerErrorException(CommonConstant.Error.InternalServerError);
		}

		public NotificationResponse UpdateReadStatus(Guid notiId, NotificationIsReadRequest request)
		{
			var noti = _notificationService.FirstOrDefault(
				x => x.EntityStatus == EntityStatus.Activated && x.Id == notiId);
			if (noti == null)
			{
				throw new BadRequestException(CommonConstant.Error.NotFound);
			}

			noti.IsRead = request.IsRead;
			var isSaved = _notificationService.Update(noti);
			if (isSaved)
			{
				return Mapper.Map<Notification, NotificationResponse>(noti);
			}

			throw new InternalServerErrorException(CommonConstant.Error.InternalServerError);
		}

		public bool UpdateSeenStatus(Guid userId, NotificationIsSeenRequest request)
		{
			var notis = _notificationService.Where(
				x => x.EntityStatus == EntityStatus.Activated && x.IsSeen == false && x.DestinationUserId == userId).ToList();
			if (!notis.Any())
			{
				throw new BadRequestException(CommonConstant.Error.NotFound);
			}

			_notificationService.UpdateMany(notis.Select(x =>
			{
				x.IsSeen = true;
				return x;
			}).ToList(), out var isSaved);
			if (isSaved)
			{
				return true;
			}

			throw new InternalServerErrorException(CommonConstant.Error.InternalServerError);
		}

		public bool Delete(Guid notiId)
		{
			bool updated = _notificationService.UpdateStatus(notiId, EntityStatus.Deleted.ToString()) != null;
			if (updated == false)
				throw new InternalServerErrorException(CommonConstant.Error.InternalServerError);

			return true;
		}

		public void PushAndroidNotification(Notification noti, List<DeviceIdentity> myRegistrationIds)
		{
			_fcmBroker.Start();

			//List<string> myRegistrationIds = new List<string>() { "fgiLiXDnTro:APA91bHoRKcCAZl0yDP2x2Gb5gHTUsk6s3ks7uWNCF_gcBLrYbqXNpxbJieL821WIJzKGcmddFKp2KuwIaVro17I4Zj3zvjS6B3M34AZDIrHs2rTFU-VgjOLRPU7cRhT0EtK5a-qGHJS" };
			var dataNotification = PreparePushNotification(noti, myRegistrationIds.First().Language);

			_fcmBroker.QueueNotification(new FcmNotification
			{
				RegistrationIds = myRegistrationIds.Select(x => x.DeviceToken).ToList(),
				Notification = JObject.Parse(dataNotification)
			});

			if (_fcmBroker.IsCompleted)
			{
				_fcmBroker.Stop();
			}
		}

		public void PushIosNotification(Notification noti, List<DeviceIdentity> myRegistrationIds)
		{
			_apnsBroker.Start();

			var dataNotification = PreparePushNotification(noti, myRegistrationIds.First().Language);

			//myRegistrationIds = new List<string>() { "143112ce10ce7d909311685f6f64949c2c5ba75fb2a72a1241a89b53ee6a2bc5" };
			foreach (var id in myRegistrationIds)
			{
				_apnsBroker.QueueNotification(new ApnsNotification
				{
					DeviceToken = id.DeviceToken,
					Payload = JObject.Parse(dataNotification)
				});
			}

			if (_apnsBroker.IsCompleted)
			{
				_apnsBroker.Stop();
			}
		}

		#region Utils

		private string PreparePushNotification(Notification noti, string language)
		{
			noti.Message = NotificationHelper.GenerateNotificationMessage(language, noti.UserDisplayName, noti.TemptType);
			var notification = Mapper.Map<NotificationResponse>(noti);
			var numberOfNotiNotSeen = _notificationService
				.Where(x => x.EntityStatus == EntityStatus.Activated && x.DestinationUserId == noti.DestinationUserId)
				.GroupBy(x => x.RelevantId)
				.Select(g => g.GroupBy(n => n.TemptType).Select(gg => gg.OrderByDescending(x => x.CreatedTime).First()).Count(x => !x.IsSeen))
				.Sum();

			var data = new
			{
				aps = new
				{
					alert = notification.Message,
					badge = numberOfNotiNotSeen
				},
				notification
			};

			return JsonConvert.SerializeObject(data);
		}

		private List<NotificationResponse> GetPagedNotifications(Guid userId, PagingQueryNotificationRequest request, out int total, out int numberOfNotiNotSeen)
		{
			var notifications = new List<NotificationResponse>();

			_notificationService
				.Where(x => x.EntityStatus == EntityStatus.Activated && x.DestinationUserId == userId)
				.GroupBy(x => x.RelevantId)
				.Select(g => new List<NotificationResponse>(g.GroupBy(n => n.TemptType).Select(gg => GenerateNotificationResponse(gg, request.Language))))
				.ToList()
				.ForEach(notifications.AddRange);

			numberOfNotiNotSeen = notifications.Count(x => x.IsSeen == false);
			total = notifications.Count;
			
			return notifications
				.OrderByDescending(x => x.CreatedTime)
				.Skip(request.Limit * (request.Page - 1))
				.Take(request.Limit)
				.ToList();
		}

		private NotificationResponse GenerateNotificationResponse(IGrouping<TemptNotificationType, Notification> group, string language)
		{
			var notification = group.OrderByDescending(x => x.CreatedTime).First();
			notification.Message = NotificationHelper.GenerateNotificationMessage(language, notification.UserDisplayName, notification.TemptType, group.GroupBy(x => x.SourceUserId).Count());
			var notificationResponse = Mapper.Map<NotificationResponse>(notification);

			Post post = null;
			if (notification.Type == NotificationType.Comment || notification.Type == NotificationType.Like)
			{
				post = _postService.Include(x => x.Images).FirstOrDefault(x => x.Id == notification.RelevantId);
			}
			else if (notification.Type == NotificationType.Request)
			{
				var request = _requestService.Include(x => x.Post.Images).FirstOrDefault(x => x.Id == notification.RelevantId);
				if (request == null)
				{
					throw new DataNotFoundException("Request not found");
				}
				post = request.Post;
			}
			//var post = _postService.Include(x => x.Images).FirstOrDefault(x => x.Id == notification.RelevantId);
			if (post != null)
			{
				notificationResponse.PostUrl = post.Images.Count > 0 ? post.Images.ElementAt(0).ResizedImage : null;
			}

			var user = _userService.FirstOrDefault(x => x.Id == notification.SourceUserId);
			if (user != null)
			{
				notificationResponse.AvatarUrl = user.AvatarUrl;
				notificationResponse.UserDisplayName = user.DisplayName;
			}

			return notificationResponse;
		}

		private void InitForAndroid()
		{
			var config = new FcmConfiguration(FcmProjectNumber, FcmApiKey, null);
			_fcmBroker = new FcmServiceBroker(config);

			_fcmBroker.OnNotificationFailed += (notification, aggregateEx) =>
			{
				aggregateEx.Handle(ex =>
				{
					if (ex is DeviceSubscriptionExpiredException expiredException)
					{
						var oldId = expiredException.OldSubscriptionId;
						ClearInvalidDeviceToken(oldId, MobilePlatform.Android);
					}

					return true;
				});
			};

			_fcmBroker.OnNotificationSucceeded += notification =>
			{

			};
		}

		private void InitForiOs()
		{
			var environment = ServiceProviderHelper.Current.GetService<IHostingEnvironment>();
			var webRoot = environment.WebRootPath;
			var cerFileFullPath = Path.Combine(webRoot, Const.StaticFilesFolder, "giventake.dev.p12");

			// use this for development environment
			var apnsConfig = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Sandbox, cerFileFullPath, cerFilePwd);

			// use this for production environment: testflight, appstore
			//var apnsConfig = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Production, cerFileFullPath, cerFilePwd);

			_apnsBroker = new ApnsServiceBroker(apnsConfig);
			_apnsBroker.OnNotificationFailed += (notification, aggregateEx) => {
				aggregateEx.Handle(ex =>
				{
					if (ex is DeviceSubscriptionExpiredException expiredException)
					{
						var oldId = expiredException.OldSubscriptionId;
						ClearInvalidDeviceToken(oldId, MobilePlatform.Ios);
					}

					return true;
				});
			};

			_apnsBroker.OnNotificationSucceeded += _apnsBroker_OnNotificationSucceeded;
		}

		private static void _apnsBroker_OnNotificationSucceeded(ApnsNotification notification)
		{
		}

		private void ClearInvalidDeviceToken(string token, MobilePlatform platform)
		{
			var item = _deviceIdentityService.FirstOrDefault(k => k.DeviceToken == token && k.MobilePlatform == platform);

			if (item != null)
			{
				_deviceIdentityService.Delete(item);
			}
		}
		#endregion
	}
}
