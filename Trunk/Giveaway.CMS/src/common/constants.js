export const DEFAULT_ERROR_MESSAGE = 'Có lỗi xảy ra';
export const DEFAULT_AVATAR_URL = '/images/default-avatar.png';
export const STATUS_NOT_ACTIVATED = 'not_activated';
export const STATUS_ACTION_ACTIVATE = 'Kích hoạt';
export const STATUS_ACTION_BLOCK = 'Khóa';
export const STATUS_NOT_VERIFIED = 'not_verified';
export const STATUS_NOT_AUTHORIZED = 'not_authorized';
export const MAX_RETRIES_COUNT = 3;
export const RETRIES_SECONDS_COUNTDOWN = 30;
export const TABLE_PAGESIZE = 10;
export const DateFormatDisplay = 'DD/MM/YYYY';
export const DateTimeFormatDisplay = 'HH:mm DD/MM/YYYY';

export const API_KEY = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6ImFkbWluIiwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvc2lkIjoiMjMwMzc5MmItZGI1MC00YzlhLTk3MjAtN2JkMWVjN2QzM2U4IiwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvbmFtZSI6IkFkbWluIEFkbWluIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9yb2xlIjpbIkFkbWluIiwiVXNlciJdLCJuYmYiOjE1MzM4ODY3MzMsImV4cCI6MTUzNjQ3ODczMywiaXNzIjoiR2l2ZWF3YXkiLCJhdWQiOiJFdmVyeW9uZSJ9.2A8md3WFGYT-w2Kz0OMAUAj7e20L-wTxIfKL4KyEOko';

export const COLOR = {
  Warning: 'orange',
};

export const STATUSES = {
  Activated: 'Activated',
  Blocked: 'Blocked',
  Deleted: 'Deleted',
};

export const ENG_VN_DICTIONARY = {
  Activated: 'Đang kích hoạt',
  Blocked: 'Đã khóa',
  Gived: 'Đã cho',
  Giving: 'Đang cho',
  User: 'Người dùng',
  Admin: 'Admin',
  Male: 'Nam',
  Female: 'Nữ',
  SuperAdmin: 'SuperAdmin',
};
